Using the Plugins
=================

This file describes how to use any-annotate, any-deannotate and batch-annotate plugins in detail.


any-annotate and any-deannotate
-------------------------------

#### Command-line Options

The plugins are enabled by adding `-Xany-annotate`, `-Xany-deannotate` command-line options to XJC, respectively.

In addition to that you can specify the default property annotation target via `-Xany-annotate-defaultFieldTarget=TARGET`
(or `-Xany-deannotate-defaultFieldTarget=TARGET`), where `TARGET` is one of `getter` (the default), `setter`,
`setter-parameter` and `field` (see **Customization Element Options** below for more details).

#### Customizing Code Generation
Like with all other customizable XJC plugins the customizations can be specified either via an external binding file:
```xml
<jaxb:bindings xmlns:jaxb="https://jakarta.ee/xml/ns/jaxb"
               xmlns:xs="http://www.w3.org/2001/XMLSchema"
               xmlns:aa="https://gitlab.com/virtual-machinist/any-annotate"
               extensionBindingPrefixes="aa"
               version="3.0">
...
    <jaxb:bindings node="xs:complexType[@name='Car']">
        <aa:any-annotate>@foo.bar.ClassMarker</aa:any-annotate>
        <jaxb:bindings node="xs:sequence/xs:element[@name='door']">
            <aa:any-annotate target="field">@foo.bar.FieldMarker</aa:any-annotate>
        </jaxb:bindings>
        <jaxb:bindings node="xs:attribute[@name='make']">
            <aa:any-annotate target="setter">@foo.bar.SetterMarker</aa:any-annotate>
        </jaxb:bindings>
        <jaxb:bindings node="xs:attribute[@name='model']">
            <aa:any-annotate target="getter">@foo.bar.GetterMarker</aa:any-annotate>
        </jaxb:bindings>
    </jaxb:bindings>
...
    <jaxb:bindings node="xs:element[@name='Bar']/xs:complexType">
        <aa:any-deannotate>javax.xml.bind.annotation.XmlRootElement</aa:any-deannotate>
    </jaxb:bindings>
...
</jaxb:bindings>               
```
... or by embedding the customization inside the schema itself:
```xml
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns:jaxb="https://jakarta.ee/xml/ns/jaxb"
           xmlns:aa="https://gitlab.com/virtual-machinist/any-annotate"
           jaxb:extensionBindingPrefixes="aa"
           jaxb:version="3.0">
...           
   <xs:complexType name="Car">
       <xs:annotation>
           <xs:appinfo>
               <aa:any-annotate>@foo.bar.ClassMarker</aa:any-annotate>
           </xs:appinfo>
       </xs:annotation>
       <xs:sequence>
           <xs:element name="door" type="Door" maxOccurs="5">
               <xs:annotation>
                   <xs:appinfo>
                       <aa:any-annotate target="field">@foo.bar.FieldMarker</aa:any-annotate>
                   </xs:appinfo>
               </xs:annotation>
           </xs:element>
       </xs:sequence>
...
   </xs:complexType>
...   
</xs:schema>           
```
For more information please have a look at `simple-schema.xjb` and `simple-schema-embedded.xsd` used in code generation
tests in `src/test/resources/any-annotate` or `src/test/resources/any-deannotate` respectively.

#### Customization Element Options
The customization element name is `any-annotate` for annotation adding and `any-deannotate` for annotation removal
(namespace `https://gitlab.com/virtual-machinist/any-annotate`). A schema file is available at
[schema/any-annotate.xsd](https://gitlab.com/virtual-machinist/any-annotate/raw/master/schema/any-annotate.xsd) for
autocomplete and basic validation support in IDEs (although XJC does not validate elements by itself).

Customization elements are specified where appropriate as:

`<any-annotate target="TARGET" replace="REPLACE">EXPRESSION</any-annotate>` (adding)
or `<any-deannotate target="TARGET">CLASS_NAME</any-deannotate>` (removal)

* `TARGET` - annotation target, optional. May be one of:
    * `package` - package. Can be referenced from a declaration resolvable to a class (`complexType`),
      type property (`element` or `attribute`), enum, enum constant, root `element` declaration or directly from the
      schema root at the same level as `schemaBindings`[^1].
    * `class` - class. Can be referenced from a type declaration resolvable to a class or a type property.
    * `field` - property field. Can be referenced from a type property.
    * `getter` - property getter method. Can be referenced from a type property.
    * `setter` - property setter method. Can be referenced from a type property.
    * `setter-parameter` - property setter method argument. Can be referenced from a type property.
    * `enum` - enum. Can be referenced from a type declaration resolvable to an enum
      (`simpleType/restriction/enumeration`) or an enum constant.
    * `enum-constant` - enum constant. Can be referenced from an enum constant declaration (`enumeration`).
    * `enum-value-method` - `value()` method of an enum. Can be referenced from a type declaration resolvable to an enum.
    * `enum-fromValue-method` - static `fromValue(String)` method of an enum. Can be referenced from a type declaration
      resolvable to an enum.
    * `element` - root element class. Can be referenced from a root element declaration
      (i.e. from standalone `element`). Isn't always generated by XJC.

* `REPLACE` - replace any existing annotation of the same class, optional. May be `true` or `false` (the default).
* `EXPRESSION` - annotation expression, mandatory. An error is reported if the annotation is blank or cannot be parsed.
* `CLASS_NAME` - fully qualified annotation class name, mandatory. An error is reported if the class name is blank or
  invalid.


batch-annotate
--------------

#### Command-line Options

The plugin is enabled by adding `-Xbatch-annotate` command-line option to XJC. As of now the plugin does not have any
additional command-line options.

#### Customizing Code Generation
Like with `any-annotate` and `any-deannotate` the plugin is configured in an external binding file:
```xml
<jaxb:bindings xmlns:jaxb="https://jakarta.ee/xml/ns/jaxb"
               xmlns:xs="http://www.w3.org/2001/XMLSchema"
               xmlns:ba="https://gitlab.com/virtual-machinist/batch-annotate"
               extensionBindingPrefixes="ba"
               version="3.0">
...
    <jaxb:bindings schemaLocation="../simple-schema.xsd" node="/xs:schema">
        <jaxb:schemaBindings>
            <jaxb:package name="foo.bar.something"/>
        </jaxb:schemaBindings>
        <ba:batch-annotate pattern-type="wildcard">
            <ba:annotate>
                <ba:package/>
                <ba:expression>@com.foo.PackageAnnotation</ba:expression>
            </ba:annotate>
            <ba:annotate>
                <ba:field name="make" class-name="*Car"/>
                <ba:expression>@com.foo.FieldAnnotation</ba:expression>
            </ba:annotate>
            <ba:annotate>
                <ba:enum-constant name="*FRONT"/>
                <ba:expression>@com.foo.EnumConstantAnnotation</ba:expression>
            </ba:annotate>
            <ba:annotate>
                <ba:method-param method-name="set*" name="value" method-modifiers="public" type="java.lang.String"/>
                <ba:expression>@com.foo.StringParamAnnotation</ba:expression>
            </ba:annotate>
            <ba:annotate>
                <ba:method name="getDoor" modifiers="public"/>
                <ba:expression>@com.foo.MethodAnnotation</ba:expression>
            </ba:annotate>
        </ba:batch-annotate>
    </jaxb:bindings>
...
</jaxb:bindings>               
```

It is highly recommended to use `jaxb:schemaBindings` to define a package name for the schema file even if the
conventional naming strategy works for you.

For more information please have a look at `simple-schema.xjb` and `simple-schema-embedded.xsd` used in code generation
tests in `src/test/resources/batch-annotate` subdirectory.

#### Customization Element Options
A schema file is available at
[schema/batch-annotate.xsd](https://gitlab.com/virtual-machinist/any-annotate/raw/master/schema/batch-annotate.xsd) for
autocomplete and basic validation support in IDEs (although XJC does not validate elements by itself). The XSD is
documented, so it is encouraged to have a look at it regardless of IDE autocomplete support.

##### batch-annotate element
The `batch-annotate` customization element must be placed in the root of the schema binding customization element and
thus does batch annotation processing per package. Placing the element somewhere else may lead to unexpected results.

`batch-annotate` element has an option to specify the pattern type used to match type, field or parameter names via the
`pattern-type` attribute. There are two options: `wildcard` (the default) or `regex`:
* `regex` patterns are standard Java regular expressions compiled using `java.util.Pattern`.  
* `wildcard` patterns are somewhat simpler expressions that obey the following rules:
    * An asterisk (`*`) matches zero or more characters.
    * A question mark (`?`) matches exactly one character.
    * If the first character is an exclamation mark (`!`) the rest of the pattern is negated.
    * Any other character is matched verbatim case-sensitively.
    * The pattern is matched against the whole input from start to end.

Omitting any non-mandatory expression attribute throughout the model regardless of pattern type assumes a positive match
for any input.

`batch-annotate` element may have 1..N `annotate` children.

##### annotate element
The `annotate` child element defines a filter type, and a list of annotation expressions to add or replace in all
targets matched by the filter. Only one filter type can be defined for every `annotate` element.

`expression` child element value must contain a valid annotation expression (see **Annotation Expressions** below).
It may have an optional boolean `replace` parameter (`false` by default) that controls whether to add or replace any
existing annotations in matched targets. One `annotate` element may contain 1..N `expression` children.

Valid filter child elements follow. Any attribute not explicitly marked as optional is mandatory.
* `package` - match the current package. Has no attributes.
* `class` - match a class or enum. Has the following attributes:
    * `name` - pattern for the fully qualified class or enum name.
    * `type` - class type, optional. May be `class`, `enum`, `interface` or `annotation`. Matches any class type if
      omitted. 
* `enum-constant` - match an enum constant. Has the following attributes:
    * `class-name` - pattern for the fully qualified class enum name, optional.
    * `name` - pattern for enum constant name.
* `field` - match a field. Has the following attributes:
    * `class-name` - pattern for the fully qualified class or enum name, optional.
    * `class-type` - class type, optional. May be `class`, `enum`, `interface` or `annotation`. Matches any class type
      if omitted.
    * `name` - pattern for field name.
    * `type` - pattern for fully qualified field type, including generics, optional.
    * `modifiers` - field modifiers separated by a space, in any order, or special modifier `none` for no (package
      private) modifier match. Optional.
* `method` - match a method or constructor. Has the following attributes:
    * `class-name` - pattern for the fully qualified class or enum name, optional.
    * `class-type` - class type, optional. May be `class`, `enum`, `interface` or `annotation`. Matches any class type
      if omitted.
    * `name` - pattern for method name. Constructors are matched using the special name `<init>`.
    * `modifiers` - method modifiers separated by a space, in any order, or special modifier `none` for no (package
      private) modifier match. Optional.
    * `param-types` - pattern(s) for fully qualified method parameter types (including generics), separated by a
      semicolon (`;`) character. Optional. See below for more details.
* `method-param` - match a method or constructor parameter.  Has the following attributes:
    * `class-name` - pattern for the fully qualified class or enum name, optional.
    * `class-type` - class type, optional. May be `class`, `enum`, `interface` or `annotation`. Matches any class type
      if omitted.
    * `method-name` - pattern for method name. Constructors are matched using the special name `<init>`.
    * `method-modifiers` - method modifiers separated by a space, in any order, or special modifier `none` for no
      (package private) modifier match. Optional.
    * `method-param-types` - pattern(s) for fully qualified method parameter types (including generics), separated by a
      semicolon (`;`) character. Optional. See below for more details.
    * `name` - pattern for method parameter name, optional. 
    * `type` - pattern for fully qualified parameter type, including generics, optional.
    * `index` - zero-based parameter index, optional. Any index is matched if omitted.
    
For method and constructor parameter types values the following rules apply:
* No value matches against any parameter type combination.
* Empty value matches against no parameters.
* Non-empty value matches parameter types and their count after splitting.
* **Non-empty values are supported only when using wildcard pattern matching.**


Annotation Expressions
----------------------

All plugins in this project use the excellent **JavaParser** library ([homepage](https://javaparser.org/)) to parse the
expressions and map the model to the one used in XJC.

Since the expression parser doesn't know anything about what type is the annotation or its parameters, it is necessary
to use fully-qualified names when specifying annotation classes, class parameters and constants
(i.e. `@java.lang.Deprecated`, `java.math.BigDecimal.class`, `java.lang.Integer.MAX_VALUE`).
All fully-qualified classes will be added to imports, with the exception of constant field references.

Depending on your mileage you may also want to add `d`, `f`, `l` numeric literals for floating-point and long values.

Expression parser will do its best to use your expression, but may occasionally fail to produce compilable code.
For more info please check out `AnnotatableFacadeTests.java`.

----
[^1]: experimental feature, works best if `schemaBindings` define a package name.