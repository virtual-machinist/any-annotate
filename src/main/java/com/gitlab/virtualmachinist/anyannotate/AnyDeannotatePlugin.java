package com.gitlab.virtualmachinist.anyannotate;

import com.gitlab.virtualmachinist.anyannotate.outline.visitor.AnnotationRemovingOutlineVisitor;
import com.gitlab.virtualmachinist.anyannotate.outline.visitor.OutlineVisitor;
import com.sun.codemodel.JCodeModel;
import org.xml.sax.ErrorHandler;

import javax.xml.namespace.QName;
import java.util.stream.Collectors;

public class AnyDeannotatePlugin extends OutlineVisitingPlugin {

  public AnyDeannotatePlugin() {
    super("any-deannotate", "Xany-deannotate");
  }

  @Override
  OutlineVisitor getOutlineVisitor(JCodeModel codeModel, ErrorHandler errorHandler, QName customizationElementName,
                                   AnnotationTarget defaultFieldTarget) {
    return new AnnotationRemovingOutlineVisitor(codeModel, errorHandler, customizationElementName, defaultFieldTarget);
  }

  @Override
  public String getUsage() {
    String targets = VALID_FIELD_TARGETS.stream().map(AnnotationTarget::getTarget).collect(Collectors.joining(","));
    String optionName = getOptionName();
    return "  -" + optionName + ":  enables plugin for removing arbitrary annotations\n" +
        "  -" + optionName + "-defaultFieldTarget=[" + targets + "]:  sets the default target for properties" +
        " (if unspecified: " + getDefaultFieldTarget() + ")";
  }

}
