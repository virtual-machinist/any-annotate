package com.gitlab.virtualmachinist.anyannotate.expression;

import com.sun.codemodel.JAnnotationUse;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JExpressionImpl;
import com.sun.codemodel.JFormatter;

/**
 * Wrapper class for {@link JAnnotationUse} that enables {@link JAnnotationUse} instances to be used as parameters for
 * {@link JAnnotationUse#param(String, JExpression)}.
 */
public class JAnnotationExpression extends JExpressionImpl {

  private final JAnnotationUse annotation;

  private JAnnotationExpression(JAnnotationUse annotation) {
    this.annotation = annotation;
  }

  @Override
  public void generate(JFormatter f) {
    annotation.generate(f);
  }

  public static JAnnotationExpression of(JAnnotationUse annotation) {
    return new JAnnotationExpression(annotation);
  }

}
