package com.gitlab.virtualmachinist.anyannotate.expression;

import com.sun.codemodel.JExpressionImpl;
import com.sun.codemodel.JFormatter;

/**
 * Wrapper for raw expressions directly printed in the code model.
 */
public class JRawExpression extends JExpressionImpl {

  private final String expression;

  private JRawExpression(String expression) {
    this.expression = expression;
  }

  @Override
  public void generate(JFormatter f) {
    f.p(expression);
  }

  public static JRawExpression of(String expression) {
    return new JRawExpression(expression);
  }

}
