package com.gitlab.virtualmachinist.anyannotate;

import com.sun.codemodel.JAnnotatable;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JType;
import com.sun.tools.xjc.model.CEnumLeafInfo;
import com.sun.tools.xjc.outline.*;
import org.jvnet.basicjaxb.util.FieldAccessorUtils;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This class describes all supported annotation targets and ways to get the relevant target.
 * For non-relevant targets an {@link UnsupportedOperationException} is thrown, otherwise an empty optional is returned.
 */
public enum AnnotationTarget {

  PACKAGE("package") {
    @Override
    public Optional<JAnnotatable> getAnnotatable(PackageOutline packageOutline) {
      return Optional.of(packageOutline._package());
    }

    @Override
    public Optional<JAnnotatable> getAnnotatable(FieldOutline fieldOutline) {
      return Optional.of(fieldOutline.parent().ref._package());
    }

    @Override
    public Optional<JAnnotatable> getAnnotatable(ClassOutline classOutline) {
      return Optional.of(classOutline.ref._package());
    }

    @Override
    public Optional<JAnnotatable> getAnnotatable(ElementOutline elementOutline) {
      return Optional.of(elementOutline.implClass._package());
    }

    @Override
    public Optional<JAnnotatable> getAnnotatable(EnumConstantOutline enumConstantOutline) {
      // for some strange reason the annotation constant constRef cannot know its enclosing enum class, but we improvise
      CEnumLeafInfo enclosingClass = enumConstantOutline.target.getEnclosingClass();
      return Optional.of(enclosingClass.parent.getOwnerPackage());
    }

    @Override
    public Optional<JAnnotatable> getAnnotatable(EnumOutline enumOutline) {
      return Optional.of(enumOutline.clazz._package());
    }
  },

  CLASS("class") {
    @Override
    public Optional<JAnnotatable> getAnnotatable(FieldOutline fieldOutline) {
      return Optional.of(fieldOutline.parent().ref);
    }

    @Override
    public Optional<JAnnotatable> getAnnotatable(ClassOutline classOutline) {
      return Optional.of(classOutline.ref);
    }
  },

  PROPERTY_GETTER("getter") {
    @Override
    public Optional<JAnnotatable> getAnnotatable(FieldOutline fieldOutline) {
      return Optional.ofNullable(FieldAccessorUtils.getter(fieldOutline));
    }
  },

  PROPERTY_SETTER("setter") {
    @Override
    public Optional<JAnnotatable> getAnnotatable(FieldOutline fieldOutline) {
      return Optional.ofNullable(FieldAccessorUtils.setter(fieldOutline));
    }
  },

  PROPERTY_FIELD("field") {
    @Override
    public Optional<JAnnotatable> getAnnotatable(FieldOutline fieldOutline) {
      return Optional.ofNullable(FieldAccessorUtils.field(fieldOutline));
    }
  },

  PROPERTY_SETTER_PARAMETER("setter-parameter") {
    @Override
    public Optional<JAnnotatable> getAnnotatable(FieldOutline fieldOutline) {
      return Optional.ofNullable(FieldAccessorUtils.setter(fieldOutline))
          .map(JMethod::listParams)
          .filter(params -> params.length == 1)
          .map(params -> params[0]);
    }

    @Override
    public String getDescription() {
      return "setter parameter";
    }
  },

  ENUM("enum") {
    @Override
    public Optional<JAnnotatable> getAnnotatable(EnumConstantOutline enumConstantOutline) {
      CEnumLeafInfo enclosingClass = enumConstantOutline.target.getEnclosingClass();
      // this yields the same JDirectClass instance as in EnumOutline.clazz, at least during debugging
      return Optional.of(enclosingClass.model.codeModel._getClass(enclosingClass.fullName()));
    }

    @Override
    public Optional<JAnnotatable> getAnnotatable(EnumOutline enumOutline) {
      return Optional.of(enumOutline.clazz);
    }
  },

  ENUM_CONSTANT("enum-constant") {
    @Override
    public Optional<JAnnotatable> getAnnotatable(EnumConstantOutline enumConstantOutline) {
      return Optional.of(enumConstantOutline.constRef);
    }

    @Override
    public String getDescription() {
      return "enum constant";
    }
  },

  ENUM_VALUE_METHOD("enum-value-method") {
    @Override
    public Optional<JAnnotatable> getAnnotatable(EnumOutline enumOutline) {
      return Optional.ofNullable(enumOutline.clazz.getMethod("value", EMPTY_TYPE_ARRAY));
    }

    @Override
    public String getDescription() {
      return "enum value() method";
    }
  },

  ENUM_FROM_VALUE_METHOD("enum-fromValue-method") {
    @Override
    public Optional<JAnnotatable> getAnnotatable(EnumOutline enumOutline) {
      JDefinedClass enumClass = enumOutline.clazz;
      // we could expect some sort of constant references for standard types, alas...
      JType stringType = enumClass.owner()._ref(String.class);
      return Optional.ofNullable(enumClass.getMethod("fromValue", new JType[]{stringType}));
    }

    @Override
    public String getDescription() {
      return "enum fromValue(String) method";
    }
  },

  ELEMENT("element") {
    @Override
    public Optional<JAnnotatable> getAnnotatable(ElementOutline elementOutline) {
      return Optional.of(elementOutline.implClass);
    }
  };

  private static final JType[] EMPTY_TYPE_ARRAY = new JType[0];

  private final String target;

  AnnotationTarget(String target) {
    this.target = target;
  }

  public String getTarget() {
    return target;
  }

  public String getDescription() {
    return target;
  }

  public Optional<JAnnotatable> getAnnotatable(EnumOutline enumOutline) {
    throw new UnsupportedOperationException("Annotation target '" + target + "' cannot be applied to an enum");
  }

  public Optional<JAnnotatable> getAnnotatable(EnumConstantOutline enumConstantOutline) {
    throw new UnsupportedOperationException("Annotation target '" + target + "' cannot be applied to an enum constant");
  }

  public Optional<JAnnotatable> getAnnotatable(ClassOutline classOutline) {
    throw new UnsupportedOperationException("Annotation target '" + target + "' cannot be applied to a class");
  }

  public Optional<JAnnotatable> getAnnotatable(FieldOutline fieldOutline) {
    throw new UnsupportedOperationException("Annotation target '" + target + "' cannot be applied to a field");
  }

  public Optional<JAnnotatable> getAnnotatable(ElementOutline elementOutline) {
    throw new UnsupportedOperationException("Annotation target '" + target + "' cannot be applied to an element");
  }

  public Optional<JAnnotatable> getAnnotatable(PackageOutline packageOutline) {
    throw new UnsupportedOperationException("Annotation target '" + target + "' cannot be applied to a package");
  }

  private static final Map<String, AnnotationTarget> TARGET_MAP = Stream.of(values())
      .collect(Collectors.toMap(AnnotationTarget::getTarget, Function.identity()));

  public static Optional<AnnotationTarget> fromTarget(String target) {
    return Optional.ofNullable(TARGET_MAP.get(target));
  }

}
