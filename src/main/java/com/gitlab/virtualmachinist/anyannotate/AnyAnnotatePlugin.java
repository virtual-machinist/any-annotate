package com.gitlab.virtualmachinist.anyannotate;

import com.gitlab.virtualmachinist.anyannotate.outline.visitor.AnnotationAddingOutlineVisitor;
import com.gitlab.virtualmachinist.anyannotate.outline.visitor.OutlineVisitor;
import com.sun.codemodel.JCodeModel;
import org.xml.sax.ErrorHandler;

import javax.xml.namespace.QName;
import java.util.stream.Collectors;

public class AnyAnnotatePlugin extends OutlineVisitingPlugin {

  public AnyAnnotatePlugin() {
    super("any-annotate", "Xany-annotate");
  }

  @Override
  OutlineVisitor getOutlineVisitor(JCodeModel codeModel, ErrorHandler errorHandler, QName customizationElementName,
                                   AnnotationTarget defaultFieldTarget) {
    return new AnnotationAddingOutlineVisitor(codeModel, errorHandler, customizationElementName, defaultFieldTarget);
  }

  @Override
  public String getUsage() {
    String targets = VALID_FIELD_TARGETS.stream().map(AnnotationTarget::getTarget).collect(Collectors.joining(","));
    String optionName = getOptionName();
    return "  -" + optionName + ":  enables plugin for adding arbitrary annotations\n" +
        "  -" + optionName + "-defaultFieldTarget=[" + targets + "]:  sets the default target for properties" +
        " (if unspecified: " + getDefaultFieldTarget() + ")";
  }

}
