package com.gitlab.virtualmachinist.anyannotate;

import com.gitlab.virtualmachinist.anyannotate.outline.util.PackageCustomizationFinder;
import com.gitlab.virtualmachinist.anyannotate.outline.visitable.ElementVisitableOutline;
import com.gitlab.virtualmachinist.anyannotate.outline.visitable.EnumVisitableOutline;
import com.gitlab.virtualmachinist.anyannotate.outline.visitable.PackageVisitableOutline;
import com.gitlab.virtualmachinist.anyannotate.outline.visitor.OutlineVisitor;
import com.sun.codemodel.JCodeModel;
import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.model.CElementInfo;
import com.sun.tools.xjc.model.CPluginCustomization;
import com.sun.tools.xjc.outline.ElementOutline;
import com.sun.tools.xjc.outline.EnumOutline;
import com.sun.tools.xjc.outline.Outline;
import com.sun.tools.xjc.outline.PackageOutline;
import org.jvnet.basicjaxb.plugin.AbstractParameterizablePlugin;
import org.xml.sax.ErrorHandler;

import javax.xml.namespace.QName;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.gitlab.virtualmachinist.anyannotate.AnnotationTarget.*;

abstract class OutlineVisitingPlugin extends AbstractParameterizablePlugin {

  static final Set<AnnotationTarget> VALID_FIELD_TARGETS = Set.of(PROPERTY_GETTER, PROPERTY_SETTER,
      PROPERTY_SETTER_PARAMETER, PROPERTY_FIELD);

  private final String optionName;
  private final List<QName> customizationElementNames;
  private AnnotationTarget defaultFieldTarget = PROPERTY_GETTER;

  OutlineVisitingPlugin(String elementName, String optionName) {
    this.customizationElementNames = List.of(new QName(Constants.NAMESPACE_URI, elementName));
    this.optionName = optionName;
  }

  @Override
  public boolean run(Outline outline, Options options, ErrorHandler errorHandler) {
    JCodeModel codeModel = outline.getCodeModel();
    QName customizationElementName = customizationElementNames.get(0);
    OutlineVisitor visitor = getOutlineVisitor(codeModel, errorHandler, customizationElementName,
        defaultFieldTarget);
    for (CElementInfo elementInfo : outline.getModel().getAllElements()) {
      ElementOutline elementOutline = outline.getElement(elementInfo);
      if (elementOutline != null) {
        new ElementVisitableOutline(elementOutline).accept(visitor);
      }
    }

    Map<PackageOutline, List<CPluginCustomization>> packageCustomizations
        = new PackageCustomizationFinder(outline).findUnacknowledged(customizationElementNames.get(0));
    for (PackageOutline packageOutline : outline.getAllPackageContexts()) {
      List<CPluginCustomization> customizations = packageCustomizations.getOrDefault(packageOutline, List.of());
      new PackageVisitableOutline(packageOutline, customizations).accept(visitor);
    }

    for (EnumOutline enumOutline : outline.getEnums()) {
      new EnumVisitableOutline(enumOutline).accept(visitor);
    }

    return true;
  }

  abstract OutlineVisitor getOutlineVisitor(JCodeModel codeModel, ErrorHandler errorHandler,
                                            QName customizationElementName, AnnotationTarget defaultFieldTarget);

  @Override
  public String getOptionName() {
    return optionName;
  }

  @Override
  public Collection<QName> getCustomizationElementNames() {
    return customizationElementNames;
  }

  public String getDefaultFieldTarget() {
    return defaultFieldTarget.getTarget();
  }

  public void setDefaultFieldTarget(String target) {
    this.defaultFieldTarget = AnnotationTarget.fromTarget(target)
        .filter(VALID_FIELD_TARGETS::contains)
        .orElseThrow(() -> new IllegalArgumentException("Invalid default field annotation target: " + target));
  }

}
