package com.gitlab.virtualmachinist.anyannotate;

public final class Constants {

  public static final String NAMESPACE_URI = "https://gitlab.com/virtual-machinist/any-annotate";

  private Constants() {
    // utility class
  }

}
