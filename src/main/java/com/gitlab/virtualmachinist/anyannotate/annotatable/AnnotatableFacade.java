package com.gitlab.virtualmachinist.anyannotate.annotatable;

import com.github.javaparser.ParseProblemException;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.sun.codemodel.JAnnotatable;
import com.sun.codemodel.JAnnotationUse;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JCodeModel;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Facade for {@link JAnnotatable} for providing simpler annotation operations.
 */
public class AnnotatableFacade {

  private final JCodeModel owner;
  private final JAnnotatable target;

  /**
   * @param owner  owner JCodeModel for class references.
   * @param target target element to receive the annotation.
   */
  public AnnotatableFacade(JCodeModel owner, JAnnotatable target) {
    this.owner = owner;
    this.target = target;
  }

  /**
   * Parses the annotation expression and adds it to the target.
   *
   * @param annotationExpression single annotation expression with all class identifiers fully qualified.
   * @return The added annotation.
   */
  public JAnnotationUse add(String annotationExpression) {
    AnnotationExpr parsedExpression = JavaParserUtils.parseAnnotation(annotationExpression);
    return add(parsedExpression);
  }

  /**
   * Adds the parsed annotation expression to the target.
   *
   * @param parsedExpression single annotation expression with all class identifiers fully qualified.
   * @return The added annotation.
   */
  public JAnnotationUse add(AnnotationExpr parsedExpression) {
    JAnnotationUse annotation = target.annotate(owner.ref(parsedExpression.getNameAsString()));
    new ParsingAnnotator(owner, annotation, parsedExpression).copyParameters();
    return annotation;
  }

  /**
   * Removes all annotations of the specified class from the target.
   *
   * @param annotationClassName fully qualified annotation class name.
   * @return The list of removed annotations.
   */
  public List<JAnnotationUse> removeAll(String annotationClassName) {
    JClass annotationClass = owner.ref(validateClassName(annotationClassName));
    return removeAll(annotationClass);
  }

  private List<JAnnotationUse> removeAll(JClass annotationClass) {
    List<JAnnotationUse> annotations = target.annotations().stream()
        .filter(annotation -> areEqual(annotation.getAnnotationClass(), annotationClass))
        .collect(Collectors.toList());
    annotations.forEach(target::removeAnnotation);
    return annotations;
  }

  /**
   * Parses the annotation expression and replaces any other annotation of the same class in the target.
   *
   * @param annotationExpression single annotation expression with all class identifiers fully qualified.
   * @return The added annotation.
   */
  public JAnnotationUse replaceAll(String annotationExpression) {
    AnnotationExpr parsedExpression = JavaParserUtils.parseAnnotation(annotationExpression);
    return replaceAll(parsedExpression);
  }

  /**
   * Replaces any other annotation of the same class as of the expression in the target.
   *
   * @param parsedExpression single annotation expression with all class identifiers fully qualified.
   * @return The added annotation.
   */
  public JAnnotationUse replaceAll(AnnotationExpr parsedExpression) {
    JClass annotationClass = owner.ref(parsedExpression.getNameAsString());
    removeAll(annotationClass);
    return add(parsedExpression);
  }

  private static String validateClassName(String className) {
    try {
      StaticJavaParser.parseClassOrInterfaceType(className);
      return className;
    } catch (ParseProblemException e) {
      throw new IllegalArgumentException("Cannot parse '" + className + "' as a class type", e);
    }
  }

  private static boolean areEqual(JClass left, JClass right) {
    // JClass or its descendants do not define a proper equals()/hashCode() implementation, so we improvise
    return left == right || left.fullName().equals(right.fullName());
  }

}
