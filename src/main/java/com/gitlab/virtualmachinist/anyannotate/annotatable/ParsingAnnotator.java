package com.gitlab.virtualmachinist.anyannotate.annotatable;

import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.nodeTypes.NodeWithName;
import com.github.javaparser.ast.nodeTypes.NodeWithType;
import com.github.javaparser.ast.type.Type;
import com.gitlab.virtualmachinist.anyannotate.expression.JAnnotationExpression;
import com.gitlab.virtualmachinist.anyannotate.expression.JRawExpression;
import com.sun.codemodel.*;

class ParsingAnnotator {

  private final JCodeModel codeModel;
  private final JAnnotationUse annotation;
  private final AnnotationExpr expression;

  ParsingAnnotator(JCodeModel codeModel, JAnnotationUse annotation, AnnotationExpr expression) {
    this.codeModel = codeModel;
    this.annotation = annotation;
    this.expression = expression;
  }

  void copyParameters() {
    //noinspection StatementWithEmptyBody
    if (expression.isMarkerAnnotationExpr()) {
      // do nothing
    } else if (expression.isSingleMemberAnnotationExpr()) {
      copyParameter("value", expression.asSingleMemberAnnotationExpr().getMemberValue());
    } else if (expression.isNormalAnnotationExpr()) {
      for (MemberValuePair pair : expression.asNormalAnnotationExpr().getPairs()) {
        copyParameter(pair.getNameAsString(), pair.getValue());
      }
    }
  }

  private void copyParameter(String name, Expression parameterExpression) {
    if (parameterExpression.isAnnotationExpr()) {
      copyAnnotationParameter(name, parameterExpression.asAnnotationExpr());
    } else if (parameterExpression.isArrayInitializerExpr()) {
      copyArrayParameter(name, parameterExpression.asArrayInitializerExpr());
    } else {
      addParameter(name, convertExpression(parameterExpression));
    }
  }

  // this class is uglier than would be necessary because there is no direct way to construct a
  // JAnnotationUse using a JClass as input and no way to add it directly as a param afterwards
  // ... hence this method
  private void copyAnnotationParameter(String name, AnnotationExpr annotationExpression) {
    // create the instance indirectly using a temporary array parameter
    JAnnotationUse annotationParam = annotation.paramArray(name).annotate(referenceClass(annotationExpression));
    new ParsingAnnotator(this.codeModel, annotationParam, annotationExpression).copyParameters();
    addParameter(name, JAnnotationExpression.of(annotationParam)); // overwrite the parameter
  }

  private void copyArrayParameter(String name, ArrayInitializerExpr arrayInitializer) {
    JAnnotationArrayMember arrayMember = annotation.paramArray(name);
    for (Expression elementExpression : arrayInitializer.getValues()) {
      if (elementExpression.isAnnotationExpr()) {
        AnnotationExpr annotationExpression = elementExpression.asAnnotationExpr();
        JAnnotationUse annotationElement = arrayMember.annotate(referenceClass(annotationExpression));
        new ParsingAnnotator(this.codeModel, annotationElement, annotationExpression).copyParameters();
      } else {
        arrayMember.param(convertExpression(elementExpression));
      }
    }
  }

  private JExpression convertExpression(Expression scalarExpression) {
    if (scalarExpression.isBooleanLiteralExpr()) {
      return JExpr.lit(scalarExpression.asBooleanLiteralExpr().getValue());
    } else if (scalarExpression.isIntegerLiteralExpr()) {
      return JExpr.lit(scalarExpression.asIntegerLiteralExpr().asNumber().intValue());
    } else if (scalarExpression.isLongLiteralExpr()) {
      return JExpr.lit(scalarExpression.asLongLiteralExpr().asNumber().longValue());
    } else if (scalarExpression.isDoubleLiteralExpr()) {
      // workaround for https://github.com/javaparser/javaparser/issues/664
      return JRawExpression.of(scalarExpression.asDoubleLiteralExpr().getValue());
    } else if (scalarExpression.isCharLiteralExpr()) {
      return JExpr.lit(scalarExpression.asCharLiteralExpr().asChar());
    } else if (scalarExpression.isStringLiteralExpr()) {
      return JExpr.lit(scalarExpression.asStringLiteralExpr().asString());
    } else if (scalarExpression.isClassExpr()) {
      ClassExpr classExpr = scalarExpression.asClassExpr();
      if (!isPrimitiveOrPrimitiveArray(classExpr.getType())) {
        return JExpr.dotclass(referenceClass(classExpr));
      }
    }
    // hail mary attempt, this can yield non-compilable code, but no other apparent way to support
    // constant expressions and enums
    return JRawExpression.of(scalarExpression.toString());
  }

  private void addParameter(String name, JExpression expression) {
    annotation.param(name, expression);
  }

  private JClass referenceClass(NodeWithName<?> nodeWithName) {
    return referenceClass(nodeWithName.getNameAsString());
  }

  private JClass referenceClass(NodeWithType<?, ?> nodeWithType) {
    return referenceClass(nodeWithType.getTypeAsString());
  }

  private JClass referenceClass(String fullyQualifiedClassName) {
    return codeModel.ref(fullyQualifiedClassName);
  }

  private static boolean isPrimitiveOrPrimitiveArray(Type type) {
    if (type.isPrimitiveType()) {
      return true;
    } else if (type.isArrayType()) {
      return isPrimitiveOrPrimitiveArray(type.asArrayType().getComponentType());
    } else {
      return false;
    }
  }

}
