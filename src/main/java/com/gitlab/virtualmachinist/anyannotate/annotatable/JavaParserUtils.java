package com.gitlab.virtualmachinist.anyannotate.annotatable;

import com.github.javaparser.ParseProblemException;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.expr.AnnotationExpr;

public final class JavaParserUtils {

  private JavaParserUtils() {
    // utility class
  }

  /**
   * @param annotationExpression a well-formed annotation expression.
   * @return Parsed annotation expression.
   * @throws IllegalArgumentException if the expression cannot be parsed as an annotation.
   */
  public static AnnotationExpr parseAnnotation(String annotationExpression) {
    try {
      return StaticJavaParser.parseAnnotation(annotationExpression);
    } catch (ParseProblemException e) {
      throw new IllegalArgumentException("Cannot parse '" + annotationExpression + "' as an annotation", e);
    }
  }

}
