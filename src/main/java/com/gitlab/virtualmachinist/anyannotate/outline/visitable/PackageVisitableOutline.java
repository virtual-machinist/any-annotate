package com.gitlab.virtualmachinist.anyannotate.outline.visitable;

import com.gitlab.virtualmachinist.anyannotate.AnnotationTarget;
import com.gitlab.virtualmachinist.anyannotate.outline.visitor.OutlineVisitor;
import com.sun.codemodel.JAnnotatable;
import com.sun.tools.xjc.model.CPluginCustomization;
import com.sun.tools.xjc.outline.ClassOutline;
import com.sun.tools.xjc.outline.PackageOutline;

import java.util.Optional;

public class PackageVisitableOutline extends AbstractVisitableOutline {

  private final PackageOutline outline;
  private final Iterable<CPluginCustomization> customizations;

  public PackageVisitableOutline(PackageOutline outline, Iterable<CPluginCustomization> customizations) {
    this.outline = outline;
    this.customizations = customizations;
  }

  @Override
  public PackageOutline getOutline() {
    return outline;
  }

  @Override
  public Optional<JAnnotatable> findAnnotatable(AnnotationTarget target) {
    return target.getAnnotatable(outline);
  }

  @Override
  public Iterable<CPluginCustomization> getAllCustomizations() {
    return customizations;
  }

  @Override
  public String getDescription() {
    return "package " + outline._package().name();
  }

  @Override
  public void accept(OutlineVisitor visitor) {
    visitor.visit(this);
    for (ClassOutline classOutline : outline.getClasses()) {
      new ClassVisitableOutline(classOutline).accept(visitor);
    }
  }

}
