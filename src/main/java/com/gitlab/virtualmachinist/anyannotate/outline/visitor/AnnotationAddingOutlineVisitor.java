package com.gitlab.virtualmachinist.anyannotate.outline.visitor;

import com.gitlab.virtualmachinist.anyannotate.AnnotationTarget;
import com.gitlab.virtualmachinist.anyannotate.annotatable.AnnotatableFacade;
import com.sun.codemodel.JAnnotatable;
import com.sun.codemodel.JCodeModel;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;
import org.xml.sax.Locator;

import javax.xml.namespace.QName;
import java.util.Optional;

public class AnnotationAddingOutlineVisitor extends AnnotationProcessingOutlineVisitor {

  private static final String REPLACE_ATTRIBUTE_NAME = "replace";

  public AnnotationAddingOutlineVisitor(JCodeModel codeModel, ErrorHandler errorHandler,
                                        QName customizationElementName, AnnotationTarget defaultFieldTarget) {
    super(codeModel, errorHandler, customizationElementName, defaultFieldTarget);
  }

  @Override
  void doProcess(Element customizationElement, Locator locator, JAnnotatable annotatable) {
    String expressionValue = requireElementContent(customizationElement,
        () -> customizationElement.getTagName() + " must contain an annotation expression");
    boolean replace = Optional.of(customizationElement.getAttribute(REPLACE_ATTRIBUTE_NAME))
        .filter(Boolean::parseBoolean)
        .isPresent();
    AnnotatableFacade facade = new AnnotatableFacade(codeModel, annotatable);
    if (replace) {
      facade.replaceAll(expressionValue);
    } else {
      facade.add(expressionValue);
    }
  }

}
