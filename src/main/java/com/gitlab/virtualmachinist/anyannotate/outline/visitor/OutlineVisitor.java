package com.gitlab.virtualmachinist.anyannotate.outline.visitor;

import com.gitlab.virtualmachinist.anyannotate.outline.visitable.*;

/**
 * Visitor interface for all known {@link VisitableOutline} classes. Implementations should contain the processing logic
 * appropriate for each such class and shouldn't make assumptions about their internal structure beyond what's available
 * via the interface.
 */
public interface OutlineVisitor {

  void visit(PackageVisitableOutline outline);

  void visit(ClassVisitableOutline outline);

  void visit(FieldVisitableOutline outline);

  void visit(ElementVisitableOutline outline);

  void visit(EnumVisitableOutline outline);

  void visit(EnumConstantVisitableOutline outline);

}
