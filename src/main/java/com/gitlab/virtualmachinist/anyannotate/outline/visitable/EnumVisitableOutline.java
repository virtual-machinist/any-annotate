package com.gitlab.virtualmachinist.anyannotate.outline.visitable;

import com.gitlab.virtualmachinist.anyannotate.AnnotationTarget;
import com.gitlab.virtualmachinist.anyannotate.outline.visitor.OutlineVisitor;
import com.sun.codemodel.JAnnotatable;
import com.sun.tools.xjc.model.CPluginCustomization;
import com.sun.tools.xjc.outline.EnumConstantOutline;
import com.sun.tools.xjc.outline.EnumOutline;
import org.jvnet.basicjaxb.util.CustomizationUtils;

import java.util.Optional;

public class EnumVisitableOutline extends AbstractVisitableOutline {

  private final EnumOutline outline;

  public EnumVisitableOutline(EnumOutline outline) {
    this.outline = outline;
  }

  @Override
  public EnumOutline getOutline() {
    return outline;
  }

  @Override
  public Optional<JAnnotatable> findAnnotatable(AnnotationTarget target) {
    return target.getAnnotatable(outline);
  }

  @Override
  public Iterable<CPluginCustomization> getAllCustomizations() {
    return CustomizationUtils.getCustomizations(outline);
  }

  @Override
  public String getDescription() {
    return "enum " + outline.clazz.fullName();
  }

  @Override
  public void accept(OutlineVisitor visitor) {
    visitor.visit(this);
    for (EnumConstantOutline constantOutline : outline.constants) {
      new EnumConstantVisitableOutline(constantOutline).accept(visitor);
    }
  }

}
