package com.gitlab.virtualmachinist.anyannotate.outline.visitable;

public abstract class AbstractVisitableOutline implements VisitableOutline {

  @Override
  public String toString() {
    return getDescription();
  }

}
