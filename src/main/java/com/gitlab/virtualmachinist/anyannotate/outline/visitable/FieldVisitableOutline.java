package com.gitlab.virtualmachinist.anyannotate.outline.visitable;

import com.gitlab.virtualmachinist.anyannotate.AnnotationTarget;
import com.gitlab.virtualmachinist.anyannotate.outline.visitor.OutlineVisitor;
import com.sun.codemodel.JAnnotatable;
import com.sun.tools.xjc.model.CPluginCustomization;
import com.sun.tools.xjc.outline.FieldOutline;
import org.jvnet.basicjaxb.util.CustomizationUtils;
import org.jvnet.basicjaxb.util.OutlineUtils;

import java.util.Optional;

public class FieldVisitableOutline extends AbstractVisitableOutline {

  private final FieldOutline outline;

  FieldVisitableOutline(FieldOutline outline) {
    this.outline = outline;
  }

  @Override
  public FieldOutline getOutline() {
    return outline;
  }

  @Override
  public Optional<JAnnotatable> findAnnotatable(AnnotationTarget target) {
    return target.getAnnotatable(outline);
  }

  @Override
  public Iterable<CPluginCustomization> getAllCustomizations() {
    return CustomizationUtils.getCustomizations(outline);
  }

  @Override
  public String getDescription() {
    return "field " + OutlineUtils.getFieldName(outline);
  }

  @Override
  public void accept(OutlineVisitor visitor) {
    visitor.visit(this);
  }

}
