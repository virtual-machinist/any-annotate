package com.gitlab.virtualmachinist.anyannotate.outline.visitable;

import com.gitlab.virtualmachinist.anyannotate.AnnotationTarget;
import com.gitlab.virtualmachinist.anyannotate.outline.visitor.OutlineVisitor;
import com.sun.codemodel.JAnnotatable;
import com.sun.tools.xjc.model.CPluginCustomization;

import java.util.Optional;

/**
 * Common wrapper interface for all <em>outlines</em> that can be annotated. Implementations should contain no
 * processing logic, but provide customization metadata and annotation target accessors.
 */
public interface VisitableOutline {

  /**
   * @return The raw outline instance wrapped by this element.
   */
  Object getOutline();

  /**
   * @return The annotatable element referenced by the target, if present.
   */
  Optional<JAnnotatable> findAnnotatable(AnnotationTarget target);

  /**
   * @return The annotatable element referenced by the target or throw an exception if none can be found.
   */
  default JAnnotatable getAnnotatable(AnnotationTarget target) {
    return findAnnotatable(target)
        .orElseThrow(() -> new IllegalArgumentException("Cannot find the annotation target '" + target.getTarget()
            + "' for outline " + getDescription()));
  }

  /**
   * @return Customizations for this outline class.
   */
  Iterable<CPluginCustomization> getAllCustomizations();

  /**
   * @return Human-readable outline description for debugging purposes.
   */
  String getDescription();

  /**
   * {@link OutlineVisitor} entry point. Should contain at least the appropriate {@code visit()} call.
   * For nested outlines also the {@code accept()} calls of child outlines.
   */
  void accept(OutlineVisitor visitor);

}
