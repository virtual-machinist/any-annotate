package com.gitlab.virtualmachinist.anyannotate.outline.visitor;

import com.gitlab.virtualmachinist.anyannotate.AnnotationTarget;
import com.gitlab.virtualmachinist.anyannotate.outline.util.DomUtils;
import com.gitlab.virtualmachinist.anyannotate.outline.util.ErrorHandlerWrapper;
import com.gitlab.virtualmachinist.anyannotate.outline.visitable.*;
import com.sun.codemodel.JAnnotatable;
import com.sun.codemodel.JCodeModel;
import com.sun.tools.xjc.model.CPluginCustomization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;
import org.xml.sax.Locator;

import javax.xml.namespace.QName;
import java.util.Optional;
import java.util.function.Supplier;

abstract class AnnotationProcessingOutlineVisitor implements OutlineVisitor {

  private static final String TARGET_ATTRIBUTE_NAME = "target";

  private final Logger log = LoggerFactory.getLogger(getClass());

  final JCodeModel codeModel;
  final ErrorHandlerWrapper errorHandlerWrapper;

  private final QName customizationElementName;
  private final AnnotationTarget defaultFieldTarget;

  AnnotationProcessingOutlineVisitor(JCodeModel codeModel, ErrorHandler errorHandler,
                                     QName customizationElementName, AnnotationTarget defaultFieldTarget) {
    this.codeModel = codeModel;
    this.errorHandlerWrapper = new ErrorHandlerWrapper(errorHandler);
    this.customizationElementName = customizationElementName;
    this.defaultFieldTarget = defaultFieldTarget;
  }

  @Override
  public void visit(PackageVisitableOutline outline) {
    doVisit(outline, AnnotationTarget.PACKAGE);
  }

  @Override
  public void visit(ClassVisitableOutline outline) {
    doVisit(outline, AnnotationTarget.CLASS);
  }

  @Override
  public void visit(FieldVisitableOutline outline) {
    doVisit(outline, defaultFieldTarget);
  }

  @Override
  public void visit(ElementVisitableOutline outline) {
    doVisit(outline, AnnotationTarget.ELEMENT);
  }

  @Override
  public void visit(EnumVisitableOutline outline) {
    doVisit(outline, AnnotationTarget.ENUM);
  }

  @Override
  public void visit(EnumConstantVisitableOutline outline) {
    doVisit(outline, AnnotationTarget.ENUM_CONSTANT);
  }

  private void doVisit(VisitableOutline outline, AnnotationTarget defaultTarget) {
    log.debug("Visiting {} (default target: {})", outline, defaultTarget);
    for (CPluginCustomization customization : outline.getAllCustomizations()) {
      Element element = customization.element;
      if (isCustomizationElement(element) && !customization.isAcknowledged()) {
        try {
          customization.markAsAcknowledged();
          AnnotationTarget target = getAnnotationTarget(element).orElse(defaultTarget);
          JAnnotatable annotatable = outline.getAnnotatable(target);
          doProcess(element, customization.locator, annotatable);
        } catch (Exception e) {
          errorHandlerWrapper.error(e, customization.locator);
        }
      }
    }
  }

  private boolean isCustomizationElement(Element element) {
    return DomUtils.nameMatches(element, customizationElementName);
  }

  /**
   * Contains the actual annotation customization processing logic.
   *
   * @param customizationElement customization element, never {@code null}.
   * @param locator              customization element location.
   * @param annotatable          annotation target, never {@code null}.
   */
  abstract void doProcess(Element customizationElement, Locator locator, JAnnotatable annotatable);

  private static Optional<AnnotationTarget> getAnnotationTarget(Element element) {
    return Optional.of(element.getAttribute(TARGET_ATTRIBUTE_NAME))
        .flatMap(AnnotationTarget::fromTarget);
  }

  /**
   * @param element              element to get the text content of. Cannot be {@code null}.
   * @param errorMessageSupplier supplies the text for the thrown exception.
   * @return The trimmed text content of the given element, never {@code null}.
   * @throws IllegalArgumentException if the element has empty or blank contents.
   */
  static String requireElementContent(Element element, Supplier<String> errorMessageSupplier) {
    return Optional.of(element)
        .map(Element::getTextContent)
        .map(String::trim)
        .filter(expression -> !expression.isEmpty())
        .orElseThrow(() -> new IllegalArgumentException(errorMessageSupplier.get()));
  }

}
