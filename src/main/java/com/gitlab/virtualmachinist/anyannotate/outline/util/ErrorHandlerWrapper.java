package com.gitlab.virtualmachinist.anyannotate.outline.util;

import org.xml.sax.ErrorHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Convenience class that allows reporting errors and warnings without throwing any additional
 * {@link org.xml.sax.SAXParseException}.
 */
public class ErrorHandlerWrapper {

  private final ErrorHandler errorHandler;

  public ErrorHandlerWrapper(ErrorHandler errorHandler) {
    this.errorHandler = errorHandler;
  }

  public void warning(String message, Locator locator) {
    try {
      errorHandler.warning(new SAXParseException(message, locator));
    } catch (SAXException ignored) {
      // nothing to do here
    }
  }

  public void error(Exception exception, Locator locator) {
    try {
      errorHandler.error(new SAXParseException(exception.getMessage(), locator, exception));
    } catch (SAXException ignored) {
      // nothing to do here
    }
  }

}
