package com.gitlab.virtualmachinist.anyannotate.outline.util;

import com.sun.codemodel.JJavaName;
import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.model.CPluginCustomization;
import com.sun.tools.xjc.model.Model;
import com.sun.tools.xjc.outline.Outline;
import com.sun.tools.xjc.outline.PackageOutline;
import com.sun.tools.xjc.reader.xmlschema.bindinfo.BIDeclaration;
import com.sun.tools.xjc.reader.xmlschema.bindinfo.BISchemaBinding;
import com.sun.tools.xjc.reader.xmlschema.bindinfo.BIXPluginCustomization;
import com.sun.tools.xjc.reader.xmlschema.bindinfo.BindInfo;
import com.sun.xml.xsom.XSAnnotation;
import com.sun.xml.xsom.XSSchema;
import com.sun.xml.xsom.XSSchemaSet;
import org.w3c.dom.Element;

import javax.xml.namespace.QName;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Finds global customizations of a {@link Model} and maps them to packages. Only W3C XML schemas are supported.
 */
public class PackageCustomizationFinder {

  private final Outline outline;
  private final Model model;
  private final Options options;

  public PackageCustomizationFinder(Outline outline) {
    this.outline = outline;
    this.model = outline.getModel();
    this.options = this.model.options;
  }

  /**
   * @param elementName customization element name to search for.
   * @return A map of all package outlines and the related unacknowledged plugin customizations. Never {@code null}.
   */
  public Map<PackageOutline, List<CPluginCustomization>> findUnacknowledged(QName elementName) {
    Map<PackageOutline, List<CPluginCustomization>> result = new LinkedHashMap<>();
    Map<Element, CPluginCustomization> customizationElements = getUnacknowledgedCustomizations(elementName);
    Map<String, SchemaMetadata> metadataByPackageName = getMetadataByPackageName();
    for (PackageOutline packageOutline : outline.getAllPackageContexts()) {
      SchemaMetadata schemaMetadata = metadataByPackageName.get(packageOutline._package().name());
      if (schemaMetadata == null) {
        continue;
      }
      List<CPluginCustomization> customizationList = result.computeIfAbsent(packageOutline, p -> new ArrayList<>());
      schemaMetadata.findCustomizationElements(elementName).stream()
          .map(customizationElements::get)
          .filter(Objects::nonNull)
          .forEach(customizationList::add);
    }
    return result;
  }

  private Map<Element, CPluginCustomization> getUnacknowledgedCustomizations(QName elementName) {
    return model.getCustomizations().stream()
        .filter(customization -> !customization.isAcknowledged()
            && DomUtils.nameMatches(customization.element, elementName))
        .collect(Collectors.toMap(customization -> customization.element, Function.identity()));
  }

  private Map<String, SchemaMetadata> getMetadataByPackageName() {
    XSSchemaSet schemaComponent = model.schemaComponent;
    if (schemaComponent != null) {
      return schemaComponent.getSchemas().stream()
          .map(SchemaMetadata::new)
          .filter(schemaMetadata -> schemaMetadata.getPackageName() != null)
          .collect(Collectors.toMap(SchemaMetadata::getPackageName, Function.identity()));
    }
    return Map.of();
  }

  private class SchemaMetadata {

    private final XSSchema schema;
    private final String targetNamespace;
    private final BindInfo bindInfo;
    private final String packageName;
    private final List<BIXPluginCustomization> customizations;

    SchemaMetadata(XSSchema schema) {
      this.schema = schema;
      this.targetNamespace = schema.getTargetNamespace();
      this.bindInfo = findBindInfo();
      this.packageName = findPackageName();
      this.customizations = findPluginCustomizations();
    }

    private BindInfo findBindInfo() {
      XSAnnotation xsAnnotation = this.schema.getAnnotation(false);
      if (xsAnnotation != null) {
        Object annotation = xsAnnotation.getAnnotation();
        if (annotation instanceof BindInfo) {
          return (BindInfo) annotation;
        }
      }
      return null;
    }

    // essentially a copy of com.sun.tools.xjc.reader.xmlschema.ClassSelector#getPackage(String) logic
    // unfortunately PackageOutline#getMostUsedNamespaceURI() behavior is unreliable, so no use for matching
    private String findPackageName() {
      String name = null;
      // "-p" takes precedence over everything else
      if (PackageCustomizationFinder.this.options.defaultPackage != null) {
        name = PackageCustomizationFinder.this.options.defaultPackage;
      }
      // use the <jaxb:package> customization
      BISchemaBinding schemaBinding = this.bindInfo != null ? this.bindInfo.get(BISchemaBinding.class) : null;
      if (name == null && schemaBinding != null && schemaBinding.getPackageName() != null) {
        name = schemaBinding.getPackageName();
      }
      // the JAX-RPC option goes below the <jaxb:package>
      if (name == null && PackageCustomizationFinder.this.options.defaultPackage2 != null) {
        name = PackageCustomizationFinder.this.options.defaultPackage2;
      }
      // generate the package name from the targetNamespace
      if (name == null) {
        name = PackageCustomizationFinder.this.model.getNameConverter().toPackageName(this.targetNamespace);
      }
      // hardcode a package name because the code doesn't compile
      // if it generated into the default java package
      if (name == null) {
        name = "generated"; // the last resort
      }
      return JJavaName.isJavaPackageName(name) ? name : null;
    }

    private List<BIXPluginCustomization> findPluginCustomizations() {
      List<BIXPluginCustomization> customizations = new ArrayList<>();
      if (this.bindInfo != null) {
        for (BIDeclaration declaration : this.bindInfo) {
          if (declaration instanceof BIXPluginCustomization) {
            customizations.add((BIXPluginCustomization) declaration);
          }
        }
      }
      return customizations;
    }

    List<Element> findCustomizationElements(QName name) {
      return this.customizations.stream()
          .filter(customization -> customization.getName().equals(name))
          .map(customization -> customization.element)
          .collect(Collectors.toList());
    }

    String getPackageName() {
      return packageName;
    }

  }

}
