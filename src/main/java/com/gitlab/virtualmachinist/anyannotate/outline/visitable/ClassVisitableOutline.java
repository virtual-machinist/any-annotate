package com.gitlab.virtualmachinist.anyannotate.outline.visitable;

import com.gitlab.virtualmachinist.anyannotate.AnnotationTarget;
import com.gitlab.virtualmachinist.anyannotate.outline.visitor.OutlineVisitor;
import com.sun.codemodel.JAnnotatable;
import com.sun.tools.xjc.model.CPluginCustomization;
import com.sun.tools.xjc.outline.ClassOutline;
import com.sun.tools.xjc.outline.FieldOutline;
import org.jvnet.basicjaxb.util.CustomizationUtils;

import java.util.Optional;

public class ClassVisitableOutline extends AbstractVisitableOutline {

  private final ClassOutline outline;

  ClassVisitableOutline(ClassOutline outline) {
    this.outline = outline;
  }

  @Override
  public ClassOutline getOutline() {
    return outline;
  }

  @Override
  public Optional<JAnnotatable> findAnnotatable(AnnotationTarget target) {
    return target.getAnnotatable(outline);
  }

  @Override
  public Iterable<CPluginCustomization> getAllCustomizations() {
    return CustomizationUtils.getCustomizations(outline);
  }

  @Override
  public String getDescription() {
    return "class " + outline.implClass.fullName();
  }

  @Override
  public void accept(OutlineVisitor visitor) {
    visitor.visit(this);
    for (FieldOutline fieldOutline : outline.getDeclaredFields()) {
      new FieldVisitableOutline(fieldOutline).accept(visitor);
    }
  }

}
