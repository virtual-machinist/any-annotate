package com.gitlab.virtualmachinist.anyannotate.outline.visitable;

import com.gitlab.virtualmachinist.anyannotate.AnnotationTarget;
import com.gitlab.virtualmachinist.anyannotate.outline.visitor.OutlineVisitor;
import com.sun.codemodel.JAnnotatable;
import com.sun.tools.xjc.model.CPluginCustomization;
import com.sun.tools.xjc.outline.ElementOutline;
import org.jvnet.basicjaxb.util.CustomizationUtils;

import javax.xml.namespace.QName;
import java.util.Optional;

public class ElementVisitableOutline extends AbstractVisitableOutline {

  private final ElementOutline outline;

  public ElementVisitableOutline(ElementOutline outline) {
    this.outline = outline;
  }

  @Override
  public Optional<JAnnotatable> findAnnotatable(AnnotationTarget target) {
    return target.getAnnotatable(outline);
  }

  @Override
  public ElementOutline getOutline() {
    return outline;
  }

  @Override
  public Iterable<CPluginCustomization> getAllCustomizations() {
    return CustomizationUtils.getCustomizations(outline);
  }

  @Override
  public String getDescription() {
    StringBuilder description = new StringBuilder("element ");
    QName elementName = outline.target.getElementName();
    String prefix = elementName.getPrefix();
    if (prefix != null && !prefix.isEmpty()) {
      description.append(prefix).append(":");
    }
    description.append(elementName.getLocalPart());
    return description.toString();
  }

  @Override
  public void accept(OutlineVisitor visitor) {
    visitor.visit(this);
  }

}
