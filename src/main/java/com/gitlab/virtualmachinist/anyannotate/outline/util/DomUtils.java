package com.gitlab.virtualmachinist.anyannotate.outline.util;

import org.w3c.dom.Element;

import javax.xml.namespace.QName;
import java.util.Objects;

public final class DomUtils {

  private DomUtils() {
    // utility class
  }

  /**
   * @return {@code true} if the local name and namespace of the Element match the given QName.
   */
  public static boolean nameMatches(Element element, QName name) {
    return Objects.equals(element.getLocalName(), name.getLocalPart())
        && Objects.equals(element.getNamespaceURI(), name.getNamespaceURI());
  }

}
