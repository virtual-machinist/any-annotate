package com.gitlab.virtualmachinist.anyannotate.outline.visitor;

import com.gitlab.virtualmachinist.anyannotate.AnnotationTarget;
import com.gitlab.virtualmachinist.anyannotate.annotatable.AnnotatableFacade;
import com.sun.codemodel.JAnnotatable;
import com.sun.codemodel.JAnnotationUse;
import com.sun.codemodel.JCodeModel;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;
import org.xml.sax.Locator;

import javax.xml.namespace.QName;
import java.util.List;

public class AnnotationRemovingOutlineVisitor extends AnnotationProcessingOutlineVisitor {

  public AnnotationRemovingOutlineVisitor(JCodeModel codeModel, ErrorHandler errorHandler,
                                          QName customizationElementName, AnnotationTarget defaultFieldTarget) {
    super(codeModel, errorHandler, customizationElementName, defaultFieldTarget);
  }

  @Override
  void doProcess(Element customizationElement, Locator locator, JAnnotatable annotatable) {
    String className = requireElementContent(customizationElement,
        () -> customizationElement.getTagName() + " must contain the annotation fully qualified class name");
    List<JAnnotationUse> removed = new AnnotatableFacade(codeModel, annotatable).removeAll(className);
    if (removed.isEmpty()) {
      errorHandlerWrapper.warning("No " + className + " annotation(s) found for this target", locator);
    }
  }

}
