package com.gitlab.virtualmachinist.anyannotate.outline.visitable;

import com.gitlab.virtualmachinist.anyannotate.AnnotationTarget;
import com.gitlab.virtualmachinist.anyannotate.outline.visitor.OutlineVisitor;
import com.sun.codemodel.JAnnotatable;
import com.sun.tools.xjc.model.CPluginCustomization;
import com.sun.tools.xjc.outline.EnumConstantOutline;
import org.jvnet.basicjaxb.util.CustomizationUtils;

import java.util.Optional;

public class EnumConstantVisitableOutline extends AbstractVisitableOutline {

  private final EnumConstantOutline outline;

  EnumConstantVisitableOutline(EnumConstantOutline outline) {
    this.outline = outline;
  }

  @Override
  public EnumConstantOutline getOutline() {
    return outline;
  }

  @Override
  public Optional<JAnnotatable> findAnnotatable(AnnotationTarget target) {
    return target.getAnnotatable(outline);
  }

  @Override
  public Iterable<CPluginCustomization> getAllCustomizations() {
    return CustomizationUtils.getCustomizations(outline);
  }

  @Override
  public String getDescription() {
    return "enum constant " + outline.constRef.getName();
  }

  @Override
  public void accept(OutlineVisitor visitor) {
    visitor.visit(this);
  }

}
