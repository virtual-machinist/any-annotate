package com.gitlab.virtualmachinist.batchannotate;

import javax.xml.namespace.QName;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Constants {

  public static final String NAMESPACE_URI = "https://gitlab.com/virtual-machinist/batch-annotate";

  public static final Set<QName> ELEMENT_NAMES = Stream.of("batch-annotate", "annotate", "package", "class",
      "enum-constant", "field", "method", "method-param", "expression")
      .map(localPart -> new QName(NAMESPACE_URI, localPart))
      .collect(Collectors.toUnmodifiableSet());

  private Constants() {
    // utility class
  }

}
