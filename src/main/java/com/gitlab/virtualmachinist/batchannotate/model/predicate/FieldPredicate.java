package com.gitlab.virtualmachinist.batchannotate.model.predicate;

import com.gitlab.virtualmachinist.batchannotate.model.ModifierType;
import com.gitlab.virtualmachinist.batchannotate.model.StringMatcher;
import com.sun.codemodel.JFieldVar;

import java.util.Set;
import java.util.function.Predicate;

import static com.gitlab.virtualmachinist.batchannotate.model.ModifierType.*;

public class FieldPredicate implements Predicate<JFieldVar> {

  private static final Set<ModifierType> FIELD_MODIFIERS = Set.of(NONE, PUBLIC, PROTECTED, PRIVATE, FINAL, STATIC,
      TRANSIENT, VOLATILE);

  private final StringMatcher nameMatcher;
  private final StringMatcher typeNameMatcher;
  private final Set<ModifierType> modifiers;

  public FieldPredicate(StringMatcher nameMatcher, StringMatcher typeNameMatcher, Set<ModifierType> modifiers) {
    this.nameMatcher = nameMatcher;
    this.typeNameMatcher = typeNameMatcher;
    this.modifiers = checkModifiers(modifiers);
  }

  @Override
  public boolean test(JFieldVar jFieldVar) {
    return nameMatcher.matches(jFieldVar.name())
        && ModifierType.modifiersMatch(modifiers, jFieldVar.mods())
        && typeNameMatcher.matches(jFieldVar.type().fullName());
  }

  private static Set<ModifierType> checkModifiers(Set<ModifierType> modifiers) {
    if (modifiers != null && !modifiers.isEmpty()) {
      modifiers.stream().filter(m -> !FIELD_MODIFIERS.contains(m)).findFirst().ifPresent(m -> {
        throw new IllegalArgumentException("Invalid field modifier: " + m.getLabel());
      });
      if (modifiers.contains(NONE) && modifiers.size() > 1) {
        throw new IllegalArgumentException("Cannot use none with other field modifiers");
      }
    }
    return modifiers;
  }

}
