package com.gitlab.virtualmachinist.batchannotate.model;

import com.sun.codemodel.JMod;
import com.sun.codemodel.JMods;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum ModifierType {

  PUBLIC("public", JMod.PUBLIC),
  PROTECTED("protected", JMod.PROTECTED),
  PRIVATE("private", JMod.PRIVATE),
  FINAL("final", JMod.FINAL),
  STATIC("static", JMod.STATIC),
  ABSTRACT("abstract", JMod.ABSTRACT),
  NATIVE("native", JMod.NATIVE),
  SYNCHRONIZED("synchronized", JMod.SYNCHRONIZED),
  TRANSIENT("transient", JMod.TRANSIENT),
  VOLATILE("volatile", JMod.VOLATILE),
  NONE("none", JMod.NONE);

  private static final Map<String, ModifierType> LABEL_MAP = Stream.of(values())
      .collect(Collectors.toUnmodifiableMap(ModifierType::getLabel, Function.identity()));

  private static final Pattern WHITESPACE = Pattern.compile("\\s+");

  private final String label;
  private final int modifier;

  ModifierType(String label, int modifier) {
    this.label = label;
    this.modifier = modifier;
  }

  public String getLabel() {
    return label;
  }

  public int getModifier() {
    return modifier;
  }

  public static ModifierType fromLabel(String label) {
    ModifierType modifier = LABEL_MAP.get(label);
    if (modifier == null) {
      throw new IllegalArgumentException("Unknown modifier: " + label);
    }
    return modifier;
  }

  public static Set<ModifierType> fromLabels(String labels) {
    if (labels == null || labels.isEmpty()) {
      return Set.of();
    }
    return WHITESPACE.splitAsStream(labels)
        .filter(l -> !l.isEmpty())
        .map(ModifierType::fromLabel)
        .collect(Collectors.toUnmodifiableSet());
  }

  public static boolean modifiersMatch(Set<ModifierType> modifiers, JMods mods) {
    return modifiersMatch(modifiers, mods.getValue());
  }

  public static boolean modifiersMatch(Set<ModifierType> modifiers, int modifierValue) {
    return modifiers == null || modifiers.isEmpty() || asModifierValue(modifiers) == modifierValue;
  }

  public static int asModifierValue(Set<ModifierType> modifiers) {
    if (modifiers == null || modifiers.isEmpty()) {
      throw new IllegalArgumentException("Cannot get a modifier value for an empty modifier set");
    }
    int value = JMod.NONE;
    for (ModifierType type : modifiers) {
      value = value | type.getModifier();
    }
    return value;
  }

}
