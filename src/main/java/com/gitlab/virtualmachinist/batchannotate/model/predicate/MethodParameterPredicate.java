package com.gitlab.virtualmachinist.batchannotate.model.predicate;

import com.gitlab.virtualmachinist.batchannotate.model.MethodParameter;
import com.gitlab.virtualmachinist.batchannotate.model.StringMatcher;
import com.sun.codemodel.JType;

import java.util.function.Predicate;

public class MethodParameterPredicate implements Predicate<MethodParameter> {

  private final StringMatcher nameMatcher;
  private final Integer index;
  private final StringMatcher typeNameMatcher;

  public MethodParameterPredicate(StringMatcher nameMatcher, Integer index, StringMatcher typeNameMatcher) {
    this.nameMatcher = nameMatcher;
    this.index = index;
    this.typeNameMatcher = typeNameMatcher;
  }

  @Override
  public boolean test(MethodParameter methodParameter) {
    return nameMatcher.matches(methodParameter.getName())
        && indexMatches(methodParameter.getIndex())
        && typeMatches(methodParameter.getType());
  }

  private boolean indexMatches(int index) {
    return this.index == null || this.index == index;
  }

  private boolean typeMatches(JType type) {
    return typeNameMatcher.matches(type.fullName());
  }

}
