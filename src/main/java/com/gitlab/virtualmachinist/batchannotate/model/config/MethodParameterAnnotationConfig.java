package com.gitlab.virtualmachinist.batchannotate.model.config;

import com.gitlab.virtualmachinist.batchannotate.model.Method;
import com.gitlab.virtualmachinist.batchannotate.model.MethodParameter;
import com.sun.codemodel.JDefinedClass;

import java.util.List;
import java.util.function.Predicate;

public class MethodParameterAnnotationConfig extends MethodAnnotationConfig {

  private final Predicate<MethodParameter> methodParameterFilter;

  public MethodParameterAnnotationConfig(Predicate<JDefinedClass> classFilter,
                                         Predicate<Method> methodFilter,
                                         Predicate<MethodParameter> methodParameterFilter,
                                         List<AnnotationExpressionConfig> expressions) {
    super(classFilter, methodFilter, expressions);
    this.methodParameterFilter = methodParameterFilter;
  }

  public Predicate<MethodParameter> getMethodParameterFilter() {
    return methodParameterFilter;
  }

}
