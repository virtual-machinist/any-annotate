package com.gitlab.virtualmachinist.batchannotate.model;

import java.util.Objects;
import java.util.regex.Pattern;

@FunctionalInterface
public interface StringMatcher {

  /**
   * @param input string to match.
   * @return {@code true} if the string matches, false otherwise.
   */
  boolean matches(String input);

  /**
   * @return A matcher that always returns the inverted result of this matcher.
   */
  default StringMatcher inverted() {
    return input -> !matches(input);
  }

  /**
   * @return A matcher that returns {@code true} on any input.
   */
  static StringMatcher anyMatch() {
    return input -> true;
  }

  /**
   * @param regex a regular expression to match against. Cannot be {@code null}.
   * @return A matcher that returns {@code true} if the input matches the regular expression.
   */
  static StringMatcher ofRegex(String regex) {
    Objects.requireNonNull(regex);
    return ofPattern(Pattern.compile(regex));
  }

  /**
   * @param pattern a regular expression to match against. Cannot be {@code null}.
   * @return A matcher that returns {@code true} if the input matches the regular expression.
   */
  static StringMatcher ofPattern(Pattern pattern) {
    Objects.requireNonNull(pattern);
    return input -> pattern.matcher(input).find();
  }

}
