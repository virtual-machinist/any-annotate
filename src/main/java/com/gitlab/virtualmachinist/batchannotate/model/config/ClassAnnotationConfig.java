package com.gitlab.virtualmachinist.batchannotate.model.config;

import com.sun.codemodel.JDefinedClass;

import java.util.List;
import java.util.function.Predicate;

public class ClassAnnotationConfig extends PackageAnnotationConfig {

  private final Predicate<JDefinedClass> classFilter;

  public ClassAnnotationConfig(Predicate<JDefinedClass> classFilter, List<AnnotationExpressionConfig> expressions) {
    super(expressions);
    this.classFilter = classFilter;
  }

  public Predicate<JDefinedClass> getClassFilter() {
    return classFilter;
  }

}
