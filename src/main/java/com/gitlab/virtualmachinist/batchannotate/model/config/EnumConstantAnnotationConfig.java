package com.gitlab.virtualmachinist.batchannotate.model.config;

import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JEnumConstant;

import java.util.List;
import java.util.function.Predicate;

public class EnumConstantAnnotationConfig extends ClassAnnotationConfig {

  private final Predicate<JEnumConstant> enumConstantFilter;

  public EnumConstantAnnotationConfig(Predicate<JDefinedClass> classFilter,
                                      Predicate<JEnumConstant> enumConstantFilter,
                                      List<AnnotationExpressionConfig> expressions) {
    super(classFilter, expressions);
    this.enumConstantFilter = enumConstantFilter;
  }

  public Predicate<JEnumConstant> getEnumConstantFilter() {
    return enumConstantFilter;
  }

}
