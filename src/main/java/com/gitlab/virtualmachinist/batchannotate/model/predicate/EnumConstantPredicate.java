package com.gitlab.virtualmachinist.batchannotate.model.predicate;

import com.gitlab.virtualmachinist.batchannotate.model.StringMatcher;
import com.sun.codemodel.JEnumConstant;

import java.util.function.Predicate;

public class EnumConstantPredicate implements Predicate<JEnumConstant> {

  private final StringMatcher nameMatcher;

  public EnumConstantPredicate(StringMatcher nameMatcher) {
    this.nameMatcher = nameMatcher;
  }

  @Override
  public boolean test(JEnumConstant jEnumConstant) {
    String fullName = jEnumConstant.getName();
    String enumName = fullName.substring(fullName.lastIndexOf('.') + 1);
    return nameMatcher.matches(enumName);
  }

}
