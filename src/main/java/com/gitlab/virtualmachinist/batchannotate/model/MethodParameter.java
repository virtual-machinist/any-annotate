package com.gitlab.virtualmachinist.batchannotate.model;

import com.sun.codemodel.JType;
import com.sun.codemodel.JVar;

public class MethodParameter {

  private final int index;
  private final JVar param;

  public MethodParameter(int index, JVar param) {
    this.index = index;
    this.param = param;
  }

  public JVar getParam() {
    return param;
  }

  public String getName() {
    return param.name();
  }

  public JType getType() {
    return param.type();
  }

  public int getIndex() {
    return index;
  }

}
