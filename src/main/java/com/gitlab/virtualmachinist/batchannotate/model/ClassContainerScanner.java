package com.gitlab.virtualmachinist.batchannotate.model;

import com.sun.codemodel.ClassType;
import com.sun.codemodel.*;
import com.sun.tools.xjc.outline.EnumOutline;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Traverses a {@link JClassContainer} to find its child classes, enums, fields, methods and method arguments.
 */
public class ClassContainerScanner {

  private final Set<JDefinedClass> classes = Collections.newSetFromMap(new IdentityHashMap<>());
  private final Set<JDefinedClass> enums = Collections.newSetFromMap(new IdentityHashMap<>());
  private final Map<JDefinedClass, List<JEnumConstant>> enumConstants = new IdentityHashMap<>();
  private final Map<JDefinedClass, List<Method>> methods = new IdentityHashMap<>();
  private final Map<JDefinedClass, List<JFieldVar>> fields = new IdentityHashMap<>();
  private final Map<Method, List<MethodParameter>> methodParameters = new HashMap<>();

  /**
   * Recursively searches the given class container for child classes, enums, fields, methods and method arguments.
   *
   * @param classContainer class or package to traverse.
   */
  public void scan(JClassContainer classContainer) {
    classContainer.classes().forEachRemaining(this::scan);
  }

  /**
   * Adds all enum constants for relevant enum classes. Has no effect if no enums were found using
   * {@link #scan(JClassContainer)}. Needed since (unfortunately) a {@link JDefinedClass} has private enum constant
   * access.
   *
   * @param enumOutlines all known enum outlines.
   * @see com.sun.tools.xjc.outline.Outline#getEnums().
   */
  public void addEnumConstants(Collection<EnumOutline> enumOutlines) {
    enumOutlines.stream()
        .filter(outline -> enums.contains(outline.clazz))
        .forEach(this::addEnumConstant);
  }

  private void addEnumConstant(EnumOutline outline) {
    List<JEnumConstant> constants = outline.constants.stream()
        .map(constant -> constant.constRef).collect(Collectors.toList());
    enumConstants.put(outline.getImplClass(), constants);
  }

  private void scan(JDefinedClass klass) {
    if (klass.isAnonymous() || klass.isHidden()) {
      return;
    }
    addClass(klass);
    ClassContainerScanner scanner = new ClassContainerScanner();
    scanner.scan((JClassContainer) klass);
    gatherFound(scanner);
  }

  private void addClass(JDefinedClass klass) {
    ClassType classType = klass.getClassType();
    if (classType == ClassType.CLASS) {
      classes.add(klass);
    } else if (classType == ClassType.ENUM) {
      enums.add(klass);
    }
    addFields(klass);
    addMethodsAndParameters(klass);
  }

  private void addFields(JDefinedClass klass) {
    fields.put(klass, new ArrayList<>(klass.fields().values()));
  }

  private void addMethodsAndParameters(JDefinedClass klass) {
    List<Method> classMethods = klass.methods().stream().map(Method::new).collect(Collectors.toList());
    klass.constructors().forEachRemaining(constructor -> classMethods.add(new Method(constructor)));
    methods.put(klass, classMethods);
    classMethods.forEach(method -> methodParameters.put(method, method.getParameters()));
  }

  private void gatherFound(ClassContainerScanner scanner) {
    classes.addAll(scanner.classes);
    enums.addAll(scanner.enums);
    methods.putAll(scanner.methods);
    fields.putAll(scanner.fields);
    methodParameters.putAll(scanner.methodParameters);
  }

  private Stream<JDefinedClass> findClasses(Predicate<JDefinedClass> filter) {
    return classes.stream().filter(filter);
  }

  /**
   * @param filter enum filter.
   * @return A stream of known enum classes matching the filter.
   */
  public Stream<JDefinedClass> findEnums(Predicate<JDefinedClass> filter) {
    return enums.stream().filter(filter);
  }

  /**
   * @param enumFilter enum filter.
   * @param filter     enum constant filter.
   * @return A stream of known enum constants matching the filters.
   */
  public Stream<JEnumConstant> findEnumConstants(Predicate<JDefinedClass> enumFilter,
                                                 Predicate<JEnumConstant> filter) {
    return findEnums(enumFilter)
        .map(enumClass -> enumConstants.getOrDefault(enumClass, List.of()))
        .flatMap(List::stream)
        .filter(filter);
  }

  /**
   * @param filter class or enum filter.
   * @return A stream of known classes or enums matching the filter.
   */
  public Stream<JDefinedClass> findClassesOrEnums(Predicate<JDefinedClass> filter) {
    return Stream.concat(findClasses(filter), findEnums(filter));
  }

  /**
   * @param classOrEnumFilter class or enum filter.
   * @param filter            field filter.
   * @return A stream of known fields matching the filters.
   */
  public Stream<JFieldVar> findFields(Predicate<JDefinedClass> classOrEnumFilter,
                                      Predicate<JFieldVar> filter) {
    return findClassesOrEnums(classOrEnumFilter)
        .map(klass -> fields.getOrDefault(klass, List.of()))
        .flatMap(List::stream)
        .filter(filter);
  }

  /**
   * @param classOrEnumFilter class or enum filter.
   * @param filter            method filter.
   * @return A stream of known methods (including constructors) matching the filters.
   */
  public Stream<Method> findMethods(Predicate<JDefinedClass> classOrEnumFilter,
                                    Predicate<Method> filter) {
    return findClassesOrEnums(classOrEnumFilter)
        .map(klass -> methods.getOrDefault(klass, List.of()))
        .flatMap(List::stream)
        .filter(filter);
  }

  /**
   * @param classOrEnumFilter class or enum filter.
   * @param methodFilter      method filter.
   * @param filter            method parameter filter.
   * @return A stream of known method parameters matching the filters.
   */
  public Stream<MethodParameter> findMethodParameters(Predicate<JDefinedClass> classOrEnumFilter,
                                                      Predicate<Method> methodFilter,
                                                      Predicate<MethodParameter> filter) {
    return findMethods(classOrEnumFilter, methodFilter)
        .map(klass -> methodParameters.getOrDefault(klass, List.of()))
        .flatMap(List::stream)
        .filter(filter);
  }

}
