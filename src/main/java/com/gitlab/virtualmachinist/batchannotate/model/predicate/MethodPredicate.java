package com.gitlab.virtualmachinist.batchannotate.model.predicate;

import com.gitlab.virtualmachinist.batchannotate.model.Method;
import com.gitlab.virtualmachinist.batchannotate.model.ModifierType;
import com.gitlab.virtualmachinist.batchannotate.model.StringMatcher;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JType;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import static com.gitlab.virtualmachinist.batchannotate.model.ModifierType.*;

public class MethodPredicate implements Predicate<Method> {

  private static final Set<ModifierType> METHOD_MODIFIERS = Set.of(NONE, PUBLIC, PROTECTED, PRIVATE, FINAL, STATIC,
      ABSTRACT, NATIVE, SYNCHRONIZED);

  private final StringMatcher nameMatcher;
  private final Set<ModifierType> modifiers;
  private final List<StringMatcher> parameterTypeNameMatchers;

  public MethodPredicate(StringMatcher nameMatcher, Set<ModifierType> modifiers,
                         List<StringMatcher> parameterTypeNameMatchers) {
    this.nameMatcher = nameMatcher;
    this.modifiers = checkModifiers(modifiers);
    this.parameterTypeNameMatchers = parameterTypeNameMatchers;
  }

  @Override
  public boolean test(Method method) {
    JMethod jMethod = method.getMethod();
    return nameMatcher.matches(method.getName())
        && ModifierType.modifiersMatch(modifiers, jMethod.mods())
        && typesMatch(jMethod.listParamTypes());
  }

  private boolean typesMatch(JType[] types) {
    if (parameterTypeNameMatchers == null) {
      return true;
    } else if (parameterTypeNameMatchers.isEmpty()) {
      return types.length == 0;
    } else {
      return types.length == parameterTypeNameMatchers.size()
          && IntStream.range(0, types.length)
          .allMatch(i -> parameterTypeNameMatchers.get(i).matches(types[i].fullName()));
    }
  }

  private static Set<ModifierType> checkModifiers(Set<ModifierType> modifiers) {
    if (modifiers != null && !modifiers.isEmpty()) {
      modifiers.stream().filter(m -> !METHOD_MODIFIERS.contains(m)).findFirst().ifPresent(m -> {
        throw new IllegalArgumentException("Invalid method modifier: " + m.getLabel());
      });
      if (modifiers.contains(NONE) && modifiers.size() > 1) {
        throw new IllegalArgumentException("Cannot use none with other method modifiers");
      }
    }
    return modifiers;
  }

}
