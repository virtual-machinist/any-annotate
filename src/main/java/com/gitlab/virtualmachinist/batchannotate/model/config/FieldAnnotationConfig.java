package com.gitlab.virtualmachinist.batchannotate.model.config;

import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JFieldVar;

import java.util.List;
import java.util.function.Predicate;

public class FieldAnnotationConfig extends ClassAnnotationConfig {

  private final Predicate<JFieldVar> fieldFilter;

  public FieldAnnotationConfig(Predicate<JDefinedClass> classFilter, Predicate<JFieldVar> fieldFilter,
                               List<AnnotationExpressionConfig> expressions) {
    super(classFilter, expressions);
    this.fieldFilter = fieldFilter;
  }

  public Predicate<JFieldVar> getFieldFilter() {
    return fieldFilter;
  }

}
