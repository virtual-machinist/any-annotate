package com.gitlab.virtualmachinist.batchannotate.model.predicate;

import com.gitlab.virtualmachinist.batchannotate.model.ClassType;
import com.gitlab.virtualmachinist.batchannotate.model.StringMatcher;
import com.sun.codemodel.JDefinedClass;

import java.util.function.Predicate;

public class ClassPredicate implements Predicate<JDefinedClass> {

  private final StringMatcher nameMatcher;
  private final ClassType classType;

  public ClassPredicate(StringMatcher nameMatcher, ClassType classType) {
    this.nameMatcher = nameMatcher;
    this.classType = classType;
  }

  @Override
  public boolean test(JDefinedClass jDefinedClass) {
    return (classType == null || classType.getType().equals(jDefinedClass.getClassType()))
        && nameMatcher.matches(jDefinedClass.fullName());
  }

}
