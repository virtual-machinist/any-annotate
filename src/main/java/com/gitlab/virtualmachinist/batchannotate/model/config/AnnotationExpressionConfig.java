package com.gitlab.virtualmachinist.batchannotate.model.config;

import com.github.javaparser.ast.expr.AnnotationExpr;
import com.gitlab.virtualmachinist.anyannotate.annotatable.JavaParserUtils;

public class AnnotationExpressionConfig {

  private final AnnotationExpr expression;
  private final boolean replaced;

  public AnnotationExpressionConfig(String expressionString, boolean replaced) {
    this.expression = JavaParserUtils.parseAnnotation(expressionString);
    this.replaced = replaced;
  }

  public AnnotationExpr getExpression() {
    return expression;
  }

  public boolean isReplaced() {
    return replaced;
  }

}
