package com.gitlab.virtualmachinist.batchannotate.model;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;

/**
 * Encapsulates {@link com.sun.codemodel.ClassType} as a typesafe enumeration. Used directly for config XML binding.
 */
@XmlType(name = "ClassType")
@XmlEnum
public enum ClassType {

  @XmlEnumValue("class")
  CLASS("class", com.sun.codemodel.ClassType.CLASS),
  @XmlEnumValue("enum")
  ENUM("enum", com.sun.codemodel.ClassType.ENUM),
  @XmlEnumValue("interface")
  INTERFACE("interface", com.sun.codemodel.ClassType.INTERFACE),
  @XmlEnumValue("annotation")
  ANNOTATION("annotation", com.sun.codemodel.ClassType.ANNOTATION_TYPE_DECL);

  private static final ClassType[] VALUES = values();

  private final String value;
  private final com.sun.codemodel.ClassType type;

  ClassType(String value, com.sun.codemodel.ClassType type) {
    this.value = value;
    this.type = type;
  }

  public String value() {
    return value;
  }

  public com.sun.codemodel.ClassType getType() {
    return type;
  }

  public static ClassType fromValue(String v) {
    for (ClassType c : VALUES) {
      if (c.value.equals(v)) {
        return c;
      }
    }
    throw new IllegalArgumentException(v);
  }

}
