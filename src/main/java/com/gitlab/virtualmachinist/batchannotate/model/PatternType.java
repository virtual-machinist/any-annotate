package com.gitlab.virtualmachinist.batchannotate.model;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;

import java.util.function.Function;

/**
 * An enumeration of supported pattern types. Used for direct creation of {@link StringMatcher} instances. Used directly
 * for config XML binding.
 */
@XmlType(name = "PatternType")
@XmlEnum
public enum PatternType implements Function<String, StringMatcher> {

  @XmlEnumValue("regex")
  REGEX("regex") {
    @Override
    StringMatcher getMatcher(String pattern) {
      return StringMatcher.ofRegex(pattern);
    }
  },

  @XmlEnumValue("wildcard")
  WILDCARD("wildcard") {
    @Override
    StringMatcher getMatcher(String pattern) {
      if (pattern.startsWith("!")) {
        return StringMatcher.ofRegex(toRegex(pattern.substring(1))).inverted();
      } else {
        return StringMatcher.ofRegex(toRegex(pattern));
      }
    }

    private String toRegex(String wildcard) {
      StringBuilder regex = new StringBuilder(wildcard.length())
          .append('^');
      wildcard.chars().forEach(character -> {
        switch (character) {
          case '*':
            regex.append(".*");
            break;
          case '?':
            regex.append('.');
            break;
          // special characters
          case '(':
          case ')':
          case '[':
          case ']':
          case '$':
          case '^':
          case '.':
          case '{':
          case '}':
          case '|':
          case '\\':
            regex.append('\\')
                .append((char) character);
            break;
          default:
            regex.append((char) character);
            break;
        }
      });
      return regex.append('$').toString();
    }
  };

  private static final PatternType[] VALUES = values();

  private final String value;

  PatternType(String value) {
    this.value = value;
  }

  abstract StringMatcher getMatcher(String pattern);

  /**
   * @param pattern a supported pattern. If {@code null} an instance of {@link StringMatcher#anyMatch()} is returned.
   * @return A {@code StringMatcher} returned by the pattern.
   */
  @Override
  public StringMatcher apply(String pattern) {
    return pattern == null ? StringMatcher.anyMatch() : getMatcher(pattern);
  }

  public static PatternType fromValue(String v) {
    for (PatternType c : VALUES) {
      if (c.value.equals(v)) {
        return c;
      }
    }
    throw new IllegalArgumentException(v);
  }

}
