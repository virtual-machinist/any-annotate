package com.gitlab.virtualmachinist.batchannotate.model.config;

import java.util.Collections;
import java.util.List;

public class PackageAnnotationConfig {

  private final List<AnnotationExpressionConfig> expressions;

  public PackageAnnotationConfig(List<AnnotationExpressionConfig> expressions) {
    this.expressions = Collections.unmodifiableList(expressions);
  }

  public List<AnnotationExpressionConfig> getExpressions() {
    return expressions;
  }

}
