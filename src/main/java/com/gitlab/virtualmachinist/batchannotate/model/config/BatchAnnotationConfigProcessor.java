package com.gitlab.virtualmachinist.batchannotate.model.config;

import com.gitlab.virtualmachinist.batchannotate.model.*;
import com.gitlab.virtualmachinist.batchannotate.model.predicate.*;
import com.gitlab.virtualmachinist.batchannotate.model.spec.*;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JEnumConstant;
import com.sun.codemodel.JFieldVar;
import jakarta.xml.bind.JAXB;
import org.w3c.dom.Element;

import javax.xml.transform.dom.DOMSource;
import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Processes XML annotation specification of a {@code batch-annotate} DOM element producing a set of annotation
 * configurations (filter + annotation expressions).
 */
public class BatchAnnotationConfigProcessor {

  private static final Pattern SEMICOLON = Pattern.compile(";");

  private final PatternType patternType;
  private PackageAnnotationConfig packageAnnotation;
  private final List<ClassAnnotationConfig> classAnnotations = new ArrayList<>();
  private final List<EnumConstantAnnotationConfig> enumConstantAnnotations = new ArrayList<>();
  private final List<FieldAnnotationConfig> fieldAnnotations = new ArrayList<>();
  private final List<MethodAnnotationConfig> methodAnnotations = new ArrayList<>();
  private final List<MethodParameterAnnotationConfig> methodParameterAnnotations = new ArrayList<>();

  public BatchAnnotationConfigProcessor(Element specElement) {
    this(JAXB.unmarshal(new DOMSource(specElement), BatchAnnotate.class));
  }

  private BatchAnnotationConfigProcessor(BatchAnnotate spec) {
    PatternType patternType = spec.getPatternType();
    this.patternType = patternType != null ? patternType : PatternType.WILDCARD;
    spec.getAnnotations().forEach(this::processAnnotationSpec);
  }

  public Optional<PackageAnnotationConfig> getPackageAnnotation() {
    return Optional.ofNullable(packageAnnotation);
  }

  public List<ClassAnnotationConfig> getClassAnnotations() {
    return Collections.unmodifiableList(classAnnotations);
  }

  public List<EnumConstantAnnotationConfig> getEnumConstantAnnotations() {
    return Collections.unmodifiableList(enumConstantAnnotations);
  }

  public List<FieldAnnotationConfig> getFieldAnnotations() {
    return Collections.unmodifiableList(fieldAnnotations);
  }

  public List<MethodAnnotationConfig> getMethodAnnotations() {
    return Collections.unmodifiableList(methodAnnotations);
  }

  public List<MethodParameterAnnotationConfig> getMethodParameterAnnotations() {
    return Collections.unmodifiableList(methodParameterAnnotations);
  }

  private void processAnnotationSpec(AnnotationSpecification spec) {
    List<AnnotationExpressionConfig> expressions = processExpressionsSpec(spec.getExpressions());
    if (spec.getPackage() != null) {
      packageAnnotation = new PackageAnnotationConfig(expressions);
    } else if (spec.getClazz() != null) {
      processClassSpec(spec.getClazz(), expressions);
    } else if (spec.getEnumConstant() != null) {
      processEnumConstantSpec(spec.getEnumConstant(), expressions);
    } else if (spec.getField() != null) {
      processFieldSpec(spec.getField(), expressions);
    } else if (spec.getMethod() != null) {
      processMethodSpec(spec.getMethod(), expressions);
    } else if (spec.getMethodParam() != null) {
      processMethodParamSpec(spec.getMethodParam(), expressions);
    } else {
      throw new IllegalArgumentException("Invalid AnnotationSpecification - no target present");
    }
  }

  private void processMethodParamSpec(MethodParamFilterSpecification spec,
                                      List<AnnotationExpressionConfig> expressions) {
    Predicate<JDefinedClass> classPredicate = new ClassPredicate(matcher(spec.getClassName()), spec.getClassType());
    Set<ModifierType> methodModifiers = ModifierType.fromLabels(spec.getMethodModifiers());
    List<StringMatcher> methodParamTypeMatchers = paramTypeMatchers(spec.getMethodParamTypes());
    Predicate<Method> methodPredicate = new MethodPredicate(matcher(spec.getMethodName()), methodModifiers,
        methodParamTypeMatchers);
    Predicate<MethodParameter> methodParameterPredicate = new MethodParameterPredicate(matcher(spec.getName()),
        spec.getIndex(), matcher(spec.getType()));
    methodParameterAnnotations.add(new MethodParameterAnnotationConfig(classPredicate, methodPredicate,
        methodParameterPredicate, expressions));
  }

  private void processMethodSpec(MethodFilterSpecification spec, List<AnnotationExpressionConfig> expressions) {
    Predicate<JDefinedClass> classPredicate = new ClassPredicate(matcher(spec.getClassName()), spec.getClassType());
    Set<ModifierType> modifiers = ModifierType.fromLabels(spec.getModifiers());
    List<StringMatcher> paramTypeMatchers = paramTypeMatchers(spec.getParamTypes());
    Predicate<Method> methodPredicate = new MethodPredicate(matcher(spec.getName()), modifiers, paramTypeMatchers);
    methodAnnotations.add(new MethodAnnotationConfig(classPredicate, methodPredicate, expressions));
  }

  private void processFieldSpec(FieldFilterSpecification spec, List<AnnotationExpressionConfig> expressions) {
    Predicate<JDefinedClass> classPredicate = new ClassPredicate(matcher(spec.getClassName()), spec.getClassType());
    Set<ModifierType> modifiers = ModifierType.fromLabels(spec.getModifiers());
    Predicate<JFieldVar> fieldPredicate = new FieldPredicate(matcher(spec.getName()),
        matcher(spec.getType()), modifiers);
    fieldAnnotations.add(new FieldAnnotationConfig(classPredicate, fieldPredicate, expressions));
  }

  private void processEnumConstantSpec(EnumConstantFilterSpecification spec,
                                       List<AnnotationExpressionConfig> expressions) {
    Predicate<JDefinedClass> enumPredicate = new ClassPredicate(matcher(spec.getClassName()), ClassType.ENUM);
    Predicate<JEnumConstant> constantPredicate = new EnumConstantPredicate(matcher(spec.getName()));
    enumConstantAnnotations.add(new EnumConstantAnnotationConfig(enumPredicate, constantPredicate, expressions));
  }

  private void processClassSpec(ClassFilterSpecification spec, List<AnnotationExpressionConfig> expressions) {
    Predicate<JDefinedClass> predicate = new ClassPredicate(matcher(spec.getName()), spec.getType());
    classAnnotations.add(new ClassAnnotationConfig(predicate, expressions));
  }

  private List<StringMatcher> paramTypeMatchers(String paramTypes) {
    if (paramTypes == null) {
      return null;
    } else if (paramTypes.trim().isEmpty()) {
      return List.of();
    }
    if (patternType != PatternType.WILDCARD) {
      throw new IllegalArgumentException("Parameter type name matching supported only for wildcard patterns");
    }
    return SEMICOLON.splitAsStream(paramTypes)
        .map(pattern -> matcher(pattern.isEmpty() ? null : pattern)) // empty wildcards coerced to anyMatcher() here
        .collect(Collectors.toList());
  }

  private StringMatcher matcher(String pattern) {
    return patternType.apply(pattern);
  }

  private static List<AnnotationExpressionConfig> processExpressionsSpec(List<AnnotationExpressionSpecification> specs) {
    return specs.stream()
        .map(spec -> new AnnotationExpressionConfig(spec.getValue(), spec.getReplace()))
        .collect(Collectors.toList());
  }

}
