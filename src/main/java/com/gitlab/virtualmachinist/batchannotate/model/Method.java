package com.gitlab.virtualmachinist.batchannotate.model;

import com.sun.codemodel.JMethod;
import com.sun.codemodel.JVar;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Wrapper for {@link JMethod} XJC model to unify constructors and regular methods.
 */
public class Method {

  public static final String CONSTRUCTOR_NAME = "<init>";

  private final JMethod method;
  private final String name;

  public Method(JMethod method) {
    this.method = method;
    this.name = method.type() != null ? method.name() : CONSTRUCTOR_NAME;
  }

  public JMethod getMethod() {
    return method;
  }

  public String getName() {
    return name;
  }

  public List<MethodParameter> getParameters() {
    JVar[] params = method.listParams();
    return IntStream.range(0, params.length)
        .mapToObj(index -> new MethodParameter(index, params[index]))
        .collect(Collectors.toList());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Method method1 = (Method) o;
    return method.equals(method1.method);
  }

  @Override
  public int hashCode() {
    return Objects.hash(method);
  }

  @Override
  public String toString() {
    return method.toString();
  }

}
