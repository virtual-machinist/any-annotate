package com.gitlab.virtualmachinist.batchannotate.model.config;

import com.gitlab.virtualmachinist.batchannotate.model.Method;
import com.sun.codemodel.JDefinedClass;

import java.util.List;
import java.util.function.Predicate;

public class MethodAnnotationConfig extends ClassAnnotationConfig {

  private final Predicate<Method> methodFilter;
  public MethodAnnotationConfig(Predicate<JDefinedClass> classFilter, Predicate<Method> methodFilter,
                                List<AnnotationExpressionConfig> expressions) {
    super(classFilter, expressions);
    this.methodFilter = methodFilter;
  }

  public Predicate<Method> getMethodFilter() {
    return methodFilter;
  }

}
