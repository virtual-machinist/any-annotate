package com.gitlab.virtualmachinist.batchannotate;

import com.github.javaparser.ast.expr.AnnotationExpr;
import com.gitlab.virtualmachinist.anyannotate.annotatable.AnnotatableFacade;
import com.gitlab.virtualmachinist.anyannotate.outline.util.PackageCustomizationFinder;
import com.gitlab.virtualmachinist.batchannotate.model.ClassContainerScanner;
import com.gitlab.virtualmachinist.batchannotate.model.config.*;
import com.sun.codemodel.JAnnotatable;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JPackage;
import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.model.CPluginCustomization;
import com.sun.tools.xjc.outline.EnumOutline;
import com.sun.tools.xjc.outline.Outline;
import com.sun.tools.xjc.outline.PackageOutline;
import org.jvnet.basicjaxb.plugin.AbstractParameterizablePlugin;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;

import javax.xml.namespace.QName;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class BatchAnnotatePlugin extends AbstractParameterizablePlugin {

  private static final QName CUSTOMIZATION_ELEMENT_NAME = new QName(Constants.NAMESPACE_URI, "batch-annotate");

  private final String optionName;
  private final Set<QName> customizationElementNames;

  public BatchAnnotatePlugin() {
    this.customizationElementNames = Constants.ELEMENT_NAMES;
    this.optionName = "Xbatch-annotate";
  }

  @Override
  public boolean run(Outline outline, Options options, ErrorHandler errorHandler) {
    Map<PackageOutline, List<CPluginCustomization>> packageCustomizations
        = new PackageCustomizationFinder(outline).findUnacknowledged(CUSTOMIZATION_ELEMENT_NAME);
    Collection<EnumOutline> enums = outline.getEnums();
    for (Entry<PackageOutline, List<CPluginCustomization>> entry : packageCustomizations.entrySet()) {
      JPackage jPackage = entry.getKey()._package();
      ClassContainerScanner scanner = new ClassContainerScanner();
      scanner.scan(jPackage);
      scanner.addEnumConstants(enums);
      processCustomizations(jPackage, scanner, entry.getValue());
    }
    return true;
  }

  private void processCustomizations(JPackage jPackage, ClassContainerScanner scanner,
                                     List<CPluginCustomization> customizations) {
    for (CPluginCustomization customization : customizations) {
      Element element = customization.element;
      JCodeModel owner = jPackage.owner();
      BatchAnnotationConfigProcessor processor = new BatchAnnotationConfigProcessor(element);
      processor.getPackageAnnotation().ifPresent(config -> annotate(owner, jPackage, config.getExpressions()));
      processor.getClassAnnotations().forEach(config -> annotate(config, scanner, owner));
      processor.getEnumConstantAnnotations().forEach(config -> annotate(config, scanner, owner));
      processor.getFieldAnnotations().forEach(config -> annotate(config, scanner, owner));
      processor.getMethodAnnotations().forEach(config -> annotate(config, scanner, owner));
      processor.getMethodParameterAnnotations().forEach(config -> annotate(config, scanner, owner));
      customization.markAsAcknowledged();
    }
  }

  private void annotate(MethodParameterAnnotationConfig config, ClassContainerScanner scanner, JCodeModel owner) {
    scanner.findMethodParameters(config.getClassFilter(), config.getMethodFilter(), config.getMethodParameterFilter())
        .forEach(methodParameter -> annotate(owner, methodParameter.getParam(), config.getExpressions()));
  }

  private void annotate(MethodAnnotationConfig config, ClassContainerScanner scanner, JCodeModel owner) {
    scanner.findMethods(config.getClassFilter(), config.getMethodFilter())
        .forEach(method -> annotate(owner, method.getMethod(), config.getExpressions()));
  }

  private void annotate(FieldAnnotationConfig config, ClassContainerScanner scanner, JCodeModel owner) {
    scanner.findFields(config.getClassFilter(), config.getFieldFilter())
        .forEach(field -> annotate(owner, field, config.getExpressions()));
  }

  private void annotate(EnumConstantAnnotationConfig config, ClassContainerScanner scanner, JCodeModel owner) {
    scanner.findEnumConstants(config.getClassFilter(), config.getEnumConstantFilter())
        .forEach(constant -> annotate(owner, constant, config.getExpressions()));
  }

  private void annotate(ClassAnnotationConfig config, ClassContainerScanner scanner, JCodeModel owner) {
    scanner.findClassesOrEnums(config.getClassFilter())
        .forEach(klass -> annotate(owner, klass, config.getExpressions()));
  }

  private void annotate(JCodeModel owner, JAnnotatable annotatable, List<AnnotationExpressionConfig> expressions) {
    AnnotatableFacade facade = new AnnotatableFacade(owner, annotatable);
    expressions.forEach(config -> {
      AnnotationExpr expression = config.getExpression();
      if (config.isReplaced()) {
        facade.replaceAll(expression);
      } else {
        facade.add(expression);
      }
    });
  }

  @Override
  public Collection<QName> getCustomizationElementNames() {
    return customizationElementNames;
  }

  @Override
  public String getOptionName() {
    return optionName;
  }

  @Override
  public String getUsage() {
    return "  -" + optionName + ":  enables plugin for adding arbitrary annotations in batch";
  }

}

