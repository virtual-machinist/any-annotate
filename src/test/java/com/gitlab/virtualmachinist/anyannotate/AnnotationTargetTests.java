package com.gitlab.virtualmachinist.anyannotate;

import com.gitlab.virtualmachinist.anyannotate.outline.util.PackageCustomizationFinder;
import com.gitlab.virtualmachinist.anyannotate.outline.visitable.*;
import com.gitlab.virtualmachinist.anyannotate.outline.visitor.OutlineVisitor;
import com.gitlab.virtualmachinist.anyannotate.testsupport.OptionsBuilder;
import com.gitlab.virtualmachinist.anyannotate.testsupport.XjcLauncher;
import com.sun.codemodel.*;
import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.model.CElementInfo;
import com.sun.tools.xjc.model.CPluginCustomization;
import com.sun.tools.xjc.model.Model;
import com.sun.tools.xjc.outline.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.jvnet.basicjaxb.util.OutlineUtils;

import javax.xml.namespace.QName;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("AssertBetweenInconvertibleTypes") // false positive noise in IntelliJ
class AnnotationTargetTests {

  private Model model;
  private Outline outline;
  private CollectingVisitor visitor;

  @BeforeEach
  void setUp() throws Exception {
    Options options = new OptionsBuilder()
        .withBindFile("any-annotate/simple-schema.xjb")
        .withSchemaFiles("simple-schema.xsd", "other-schema.xsd")
        .withArguments("-Xany-annotate")
        .build();
    XjcLauncher launcher = new XjcLauncher(options).createOutline();
    this.model = launcher.getModel();
    this.outline = launcher.getOutline();
    assertThat(this.outline).isNotNull();

    this.visitor = new CollectingVisitor();
    visitOutline();
  }

  @Test
  void packageWorks() {
    PackageOutline packageOutline = visitor.packages.get("foo.bar.something");
    assertThat(packageOutline).isNotNull();
    JPackage jPackage = packageOutline._package();
    assertThat(AnnotationTarget.PACKAGE.getAnnotatable(packageOutline)).get().isSameAs(jPackage);

    ClassOutline classOutline = visitor.classes.get("foo.bar.something.Car");
    assertThat(classOutline).isNotNull();
    assertThat(AnnotationTarget.PACKAGE.getAnnotatable(classOutline)).get().isSameAs(jPackage);

    FieldOutline fieldOutline = visitor.fields.get("foo.bar.something.Car.Door");
    assertThat(fieldOutline).isNotNull();
    assertThat(AnnotationTarget.PACKAGE.getAnnotatable(fieldOutline)).get().isSameAs(jPackage);

    EnumOutline enumOutline = visitor.enums.get("foo.bar.something.Side");
    assertThat(enumOutline).isNotNull();
    assertThat(AnnotationTarget.PACKAGE.getAnnotatable(enumOutline)).get().isSameAs(jPackage);

    EnumConstantOutline enumConstantOutline = visitor.enumConstants.get("foo.bar.something.Side.LEFT_FRONT");
    assertThat(enumConstantOutline).isNotNull();
    assertThat(AnnotationTarget.PACKAGE.getAnnotatable(enumConstantOutline)).get().isSameAs(jPackage);

    ElementOutline elementOutline = visitor.elements.get(new QName("carelem"));
    assertThat(elementOutline).isNotNull();
    assertThat(AnnotationTarget.PACKAGE.getAnnotatable(elementOutline)).get().isSameAs(jPackage);
  }

  @Test
  void classWorks() {
    ClassOutline classOutline = visitor.classes.get("foo.bar.something.Car");
    assertThat(classOutline).isNotNull();
    JDefinedClass ref = classOutline.ref;
    assertThat(AnnotationTarget.CLASS.getAnnotatable(classOutline)).get().isSameAs(ref);

    FieldOutline fieldOutline = visitor.fields.get("foo.bar.something.Car.Door");
    assertThat(fieldOutline).isNotNull();
    assertThat(AnnotationTarget.CLASS.getAnnotatable(fieldOutline)).get().isSameAs(ref);
  }

  @Test
  void propertyGetterWorks() {
    FieldOutline fieldOutline = visitor.fields.get("foo.bar.something.Car.Door");
    assertThat(fieldOutline).isNotNull();
    JMethod getter = fieldOutline.parent().implClass.getMethod("getDoor", new JType[0]);
    assertThat(AnnotationTarget.PROPERTY_GETTER.getAnnotatable(fieldOutline)).get().isSameAs(getter);
  }

  @Test
  void propertySetterWorks() {
    FieldOutline fieldOutline = visitor.fields.get("foo.bar.something.Door.Side");
    assertThat(fieldOutline).isNotNull();
    JMethod setter = fieldOutline.parent().implClass.getMethod("setSide", new JType[] {fieldOutline.getRawType()});
    assertThat(AnnotationTarget.PROPERTY_SETTER.getAnnotatable(fieldOutline)).get().isSameAs(setter);
  }

  @Test
  void propertyFieldWorks() {
    FieldOutline fieldOutline = visitor.fields.get("foo.bar.something.Door.Side");
    assertThat(fieldOutline).isNotNull();
    JFieldVar field = fieldOutline.parent().implClass.fields().get("side");
    assertThat(AnnotationTarget.PROPERTY_FIELD.getAnnotatable(fieldOutline)).get().isSameAs(field);
  }

  @Test
  void propertySetterParameterWorks() {
    FieldOutline fieldOutline = visitor.fields.get("foo.bar.something.Door.Side");
    assertThat(fieldOutline).isNotNull();
    JMethod setter = fieldOutline.parent().implClass.getMethod("setSide", new JType[] {fieldOutline.getRawType()});
    assertThat(setter).isNotNull();
    JVar parameter = setter.params().get(0);
    assertThat(AnnotationTarget.PROPERTY_SETTER_PARAMETER.getAnnotatable(fieldOutline)).get().isSameAs(parameter);
  }

  @Test
  void enumWorks() {
    EnumOutline enumOutline = visitor.enums.get("foo.bar.something.Side");
    assertThat(enumOutline).isNotNull();
    JDefinedClass clazz = enumOutline.clazz;
    assertThat(AnnotationTarget.ENUM.getAnnotatable(enumOutline)).get().isSameAs(clazz);

    EnumConstantOutline enumConstantOutline = visitor.enumConstants.get("foo.bar.something.Side.LEFT_FRONT");
    assertThat(enumConstantOutline).isNotNull();
    assertThat(AnnotationTarget.ENUM.getAnnotatable(enumConstantOutline)).get().isSameAs(clazz);
  }

  @Test
  void enumConstantWorks() {
    EnumConstantOutline enumConstantOutline = visitor.enumConstants.get("foo.bar.something.Side.LEFT_FRONT");
    assertThat(enumConstantOutline).isNotNull();
    JEnumConstant constRef = enumConstantOutline.constRef;
    assertThat(AnnotationTarget.ENUM_CONSTANT.getAnnotatable(enumConstantOutline)).get().isSameAs(constRef);
  }

  @Test
  void enumValueMethodWorks() {
    EnumOutline enumOutline = visitor.enums.get("foo.bar.something.Side");
    assertThat(enumOutline).isNotNull();
    JDefinedClass clazz = enumOutline.clazz;
    assertThat(clazz).isNotNull();

    JMethod method = clazz.getMethod("value", new JType[0]);
    assertThat(AnnotationTarget.ENUM_VALUE_METHOD.getAnnotatable(enumOutline)).get().isSameAs(method);
  }

  @Test
  void enumFromValueMethodWorks() {
    EnumOutline enumOutline = visitor.enums.get("foo.bar.something.Side");
    assertThat(enumOutline).isNotNull();
    JDefinedClass clazz = enumOutline.clazz;
    assertThat(clazz).isNotNull();

    JMethod method = clazz.getMethod("fromValue", new JType[]{model.codeModel.ref(String.class)});
    assertThat(AnnotationTarget.ENUM_FROM_VALUE_METHOD.getAnnotatable(enumOutline)).get().isSameAs(method);
  }

  @Test
  void elementWorks() {
    ElementOutline elementOutline = visitor.elements.get(new QName("carelem"));
    assertThat(elementOutline).isNotNull();
    JDefinedClass implClass = elementOutline.implClass;
    assertThat(AnnotationTarget.ELEMENT.getAnnotatable(elementOutline)).get().isSameAs(implClass);
  }

  private void visitOutline() {
    for (CElementInfo elementInfo : outline.getModel().getAllElements()) {
      ElementOutline elementOutline = outline.getElement(elementInfo);
      if (elementOutline != null) {
        new ElementVisitableOutline(elementOutline).accept(visitor);
      }
    }

    QName elementName = new QName(Constants.NAMESPACE_URI, "any-annotate");
    Map<PackageOutline, List<CPluginCustomization>> packageCustomizations
        = new PackageCustomizationFinder(outline).findUnacknowledged(elementName);
    for (PackageOutline packageOutline : outline.getAllPackageContexts()) {
      new PackageVisitableOutline(packageOutline, packageCustomizations.get(packageOutline)).accept(visitor);
    }

    for (EnumOutline enumOutline : outline.getEnums()) {
      new EnumVisitableOutline(enumOutline).accept(visitor);
    }
  }

  static class CollectingVisitor implements OutlineVisitor {

    final Map<String, PackageOutline> packages = new LinkedHashMap<>();
    final Map<String, ClassOutline> classes = new LinkedHashMap<>();
    final Map<String, FieldOutline> fields = new LinkedHashMap<>();
    final Map<QName, ElementOutline> elements = new LinkedHashMap<>();
    final Map<String, EnumOutline> enums = new LinkedHashMap<>();
    final Map<String, EnumConstantOutline> enumConstants = new LinkedHashMap<>();

    @Override
    public void visit(PackageVisitableOutline outline) {
      PackageOutline packageOutline = outline.getOutline();
      packages.put(packageOutline._package().name(), packageOutline);
    }

    @Override
    public void visit(ClassVisitableOutline outline) {
      ClassOutline classOutline = outline.getOutline();
      classes.put(classOutline.ref.fullName(), classOutline);
    }

    @Override
    public void visit(FieldVisitableOutline outline) {
      FieldOutline fieldOutline = outline.getOutline();
      fields.put(OutlineUtils.getFieldName(fieldOutline), fieldOutline);
    }

    @Override
    public void visit(ElementVisitableOutline outline) {
      ElementOutline elementOutline = outline.getOutline();
      elements.put(elementOutline.target.getElementName(), elementOutline);
    }

    @Override
    public void visit(EnumVisitableOutline outline) {
      EnumOutline enumOutline = outline.getOutline();
      enums.put(enumOutline.clazz.fullName(), enumOutline);
    }

    @Override
    public void visit(EnumConstantVisitableOutline outline) {
      EnumConstantOutline enumConstantOutline = outline.getOutline();
      enumConstants.put(enumConstantOutline.constRef.getName(), enumConstantOutline);
    }

  }

}
