package com.gitlab.virtualmachinist.anyannotate;

import com.github.javaparser.ast.CompilationUnit;
import com.gitlab.virtualmachinist.anyannotate.testsupport.asserts.CompilationUnitAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;
import java.util.Map;

import static com.gitlab.virtualmachinist.anyannotate.testsupport.conditions.AnnotatedWithCondition.withOnly;
import static com.gitlab.virtualmachinist.anyannotate.testsupport.conditions.AnnotatedWithCondition.withSomeOf;
import static org.assertj.core.api.Assertions.assertThat;

abstract class AnyAnnotatePluginSimpleSchemaTests extends FileParsingPluginTests {

  @TempDir
  Path output;

  Map<String, CompilationUnit> parsed = Map.of();

  @Test
  void annotatesPackagesFromDifferentSchemasSeparately() {
    assertThat(parsed).containsKeys("foo.bar.something.package-info", "foo.bar.other.package-info");
    assertThat(parsed.get("foo.bar.something.package-info").getPackageDeclaration())
        .isNotEmpty().get()
        .satisfies(declaration -> assertThat(declaration.getName().asString()).isEqualTo("foo.bar.something"))
        .satisfies(withOnly("@foo.bar.PackageMarker"));
    assertThat(parsed.get("foo.bar.other.package-info").getPackageDeclaration())
        .isNotEmpty().get()
        .satisfies(declaration -> assertThat(declaration.getName().asString()).isEqualTo("foo.bar.other"))
        .satisfies(withOnly("@jakarta.xml.bind.annotation.XmlSchema(namespace = \"urn:other:schema\")",
            "@bar.bar.PackageMarker"));
  }

  @Test
  void annotatesElements() {
    assertThat(parsed).containsKey("foo.bar.something.Carelem");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Carelem"))
        .hasPackageName("foo.bar.something")
        .hasNonStaticImports("bar.baz.Deprecated")
        .hasPrimaryType("Carelem")
        .getType("Carelem")
        .isClass()
        .satisfies(withSomeOf("@Deprecated"));
  }

  @Test
  void annotatesClass() {
    assertThat(parsed).containsKey("foo.bar.something.Car");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Car"))
        .hasPackageName("foo.bar.something")
        .hasNonStaticImports("foo.bar.ClassMarker")
        .hasPrimaryType("Car")
        .getType("Car")
        .isClass()
        .satisfies(withSomeOf("@ClassMarker"));
  }

  @Test
  void annotatesPropertyField() {
    assertThat(parsed).containsKey("foo.bar.something.Car");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Car"))
        .hasNonStaticImports("foo.bar.FieldMarker")
        .getType("Car")
        .getFieldByName("door")
        .isPresent()
        .get()
        .satisfies(withSomeOf("@FieldMarker"));
  }

  @Test
  void annotatesPropertySetter() {
    assertThat(parsed).containsKey("foo.bar.something.Car");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Car"))
        .hasNonStaticImports("foo.bar.SetterMarker")
        .getType("Car")
        .getMethodsBySignature("setMake", "String")
        .hasSize(1)
        .first()
        .satisfies(withOnly("@SetterMarker"));
  }

  @Test
  void annotatesPropertyGetter() {
    assertThat(parsed).containsKey("foo.bar.something.Car");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Car"))
        .hasNonStaticImports("foo.bar.GetterMarker")
        .getType("Car")
        .getMethodsBySignature("getModel")
        .hasSize(1)
        .first()
        .satisfies(withOnly("@GetterMarker"));
  }

  @Test
  void annotatesPropertySetterParameter() {
    assertThat(parsed).containsKey("foo.bar.something.Door");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Door"))
        .hasNonStaticImports("foo.bar.SetterParameterMarker")
        .hasPrimaryType("Door")
        .getType("Door")
        .getMethodsBySignature("setSide", "Side")
        .hasSize(1)
        .first()
        .extracting(declaration -> declaration.getParameter(0)) // TODO very ugly return type from AssertJ
        .satisfies(parameter -> assertThat(parameter).satisfies(withOnly("@SetterParameterMarker")));
  }

  @Test
  void annotatesEnum() {
    assertThat(parsed).containsKey("foo.bar.something.Side");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Side"))
        .hasNonStaticImports("foo.bar.EnumMarker")
        .hasPrimaryType("Side")
        .getType("Side")
        .isEnum()
        .satisfies(withSomeOf("@EnumMarker"))
        .getEnumConstants()
        .hasSize(5);
  }

  @Test
  void annotatesEnumValueMethod() {
    assertThat(parsed).containsKey("foo.bar.something.Side");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Side"))
        .hasNonStaticImports("foo.bar.EnumValueMethodMarker")
        .getType("Side")
        .isEnum()
        .getMethodsBySignature("value")
        .hasSize(1)
        .first()
        .satisfies(withSomeOf("@EnumValueMethodMarker"));
  }

  @Test
  void annotatesEnumFromValueMethod() {
    assertThat(parsed).containsKey("foo.bar.something.Side");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Side"))
        .hasNonStaticImports("foo.bar.EnumFromValueMethod")
        .getType("Side")
        .isEnum()
        .getMethodsBySignature("fromValue", "String")
        .hasSize(1)
        .first()
        .satisfies(withSomeOf("@EnumFromValueMethod"));
  }

  @Test
  void annotatesEnumMember() {
    assertThat(parsed).containsKey("foo.bar.something.Side");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Side"))
        .hasNonStaticImports("foo.bar.EnumMemberMarker")
        .getType("Side")
        .isEnum()
        .getEnumConstant("LEFT_FRONT")
        .satisfies(withSomeOf("@EnumMemberMarker"));
  }

  @Test
  void replacesExistingAnnotation() {
    assertThat(parsed).containsKey("foo.bar.other.Bar");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.other.Bar"))
        .hasNonStaticImports("jakarta.xml.bind.annotation.XmlRootElement",
            "jakarta.xml.bind.annotation.XmlAccessorType",
            "jakarta.xml.bind.annotation.XmlType")
        .hasPrimaryType("Bar")
        .getType("Bar")
        .isClass()
        .satisfies(withOnly("@XmlAccessorType", "@XmlType", "@XmlRootElement"));
  }

}
