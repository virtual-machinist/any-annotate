package com.gitlab.virtualmachinist.anyannotate;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseResult;
import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import com.gitlab.virtualmachinist.anyannotate.testsupport.XjcLauncher;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class FileParsingPluginTests {

  protected Map<String, CompilationUnit> parseFiles(XjcLauncher launcher) {
    Path targetDirectory = launcher.getTargetDirectory();
    CombinedTypeSolver typeSolver = new CombinedTypeSolver();
    typeSolver.add(new ReflectionTypeSolver());
    typeSolver.add(new JavaParserTypeSolver(targetDirectory));
    JavaSymbolSolver symbolSolver = new JavaSymbolSolver(typeSolver);
    ParserConfiguration parserConfiguration = new ParserConfiguration().setSymbolResolver(symbolSolver);
    JavaParser parser = new JavaParser(parserConfiguration);
    return launcher.streamJavaFiles()
        .collect(Collectors.toMap(path -> convertPath(targetDirectory, path), path -> parse(parser, path)));
  }

  private static CompilationUnit parse(JavaParser parser, Path path) {
    try {
      ParseResult<CompilationUnit> result = parser.parse(path);
      assertThat(result).matches(ParseResult::isSuccessful, "parsing is successful");
      assertThat(result.getResult()).isNotEmpty();
      return result.getResult().get();
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  private String convertPath(Path root, Path path) {
    return root.relativize(path)
        .toString()
        .replaceAll(Pattern.quote(File.separator), ".")
        .replaceAll("\\.java", "");
  }

}
