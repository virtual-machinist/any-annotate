package com.gitlab.virtualmachinist.anyannotate;

import com.gitlab.virtualmachinist.anyannotate.testsupport.OptionsBuilder;
import com.gitlab.virtualmachinist.anyannotate.testsupport.XjcLauncher;
import com.sun.tools.xjc.BadCommandLineException;
import com.sun.tools.xjc.ConsoleErrorReporter;
import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.outline.Outline;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class AnyDeannotatePluginTests {

  @SuppressWarnings("WeakerAccess")
  @TempDir
  Path path;

  @Test
  void generatesCode() throws Exception {
    Options options = new OptionsBuilder()
        .withTargetDirectory(path)
        .withBindFile("any-deannotate/simple-schema.xjb")
        .withSchemaFiles("simple-schema.xsd", "other-schema.xsd")
        .withArguments("-mark-generated", "-Xany-deannotate")
        .build();
    try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
      ConsoleErrorReporter errorReporter = new ConsoleErrorReporter(out);
      XjcLauncher launcher = new XjcLauncher(options, errorReporter).generateCode();
      int artifactCount = launcher.getModel().codeModel.countArtifacts();
      long count = launcher.writeFiles().streamJavaFiles().count();
      assertThat(count).isEqualTo(artifactCount);
      assertThat(out.toString(StandardCharsets.UTF_8)).doesNotContain("WARNING", "ERROR");
    }
  }

  @Test
  void reportsErrorOnInvalidTarget() throws Exception {
    Options options = new OptionsBuilder()
        .withBindFile("any-deannotate/incorrect-target.xjb")
        .withSchemaFiles("simple-schema.xsd")
        .withArguments("-Xany-deannotate")
        .build();
    try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
      ConsoleErrorReporter errorReporter = new ConsoleErrorReporter(out);
      Outline outline = new XjcLauncher(options, errorReporter).createOutline().getOutline();
      assertThat(outline).isNull();
      String errorText = out.toString(StandardCharsets.UTF_8);
      assertThat(errorText).contains("ERROR", "Annotation target 'field' cannot be applied to a class");
    }
  }

  @Test
  void reportsErrorOnBadClassName() throws Exception {
    Options options = new OptionsBuilder()
        .withBindFile("any-deannotate/bad-class-name.xjb")
        .withSchemaFiles("simple-schema.xsd")
        .withArguments("-Xany-deannotate")
        .build();
    try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
      ConsoleErrorReporter errorReporter = new ConsoleErrorReporter(out);
      Outline outline = new XjcLauncher(options, errorReporter).createOutline().getOutline();
      assertThat(outline).isNull();
      String errorText = out.toString(StandardCharsets.UTF_8);
      assertThat(errorText).contains("ERROR", "Cannot parse 'null' as a class type");
    }
  }

  @Test
  void reportsErrorOnEmptyClassName() throws Exception {
    Options options = new OptionsBuilder()
        .withBindFile("any-deannotate/empty-class-name.xjb")
        .withSchemaFiles("simple-schema.xsd")
        .withArguments("-Xany-deannotate")
        .build();
    try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
      ConsoleErrorReporter errorReporter = new ConsoleErrorReporter(out);
      Outline outline = new XjcLauncher(options, errorReporter).createOutline().getOutline();
      assertThat(outline).isNull();
      String errorText = out.toString(StandardCharsets.UTF_8);
      assertThat(errorText).contains("ERROR", "deannotate must contain the annotation fully qualified class name");
    }
  }

  @Test
  void reportsWarningOnMissingAnnotation() throws Exception {
    Options options = new OptionsBuilder()
        .withBindFile("any-deannotate/missing-annotation.xjb")
        .withSchemaFiles("simple-schema.xsd")
        .withArguments("-Xany-deannotate")
        .build();
    try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
      ConsoleErrorReporter errorReporter = new ConsoleErrorReporter(out);
      Outline outline = new XjcLauncher(options, errorReporter).createOutline().getOutline();
      assertThat(outline).isNotNull();
      String warnText = out.toString(StandardCharsets.UTF_8);
      assertThat(warnText).contains("WARNING", "No foo.bar.Deprecated annotation(s) found for this target");
    }
  }

  @ParameterizedTest
  @ValueSource(strings = {"getter", "setter", "setter-parameter", "field"})
  void acceptsDefaultFieldValueChange(String value) throws Exception {
    Options options = new OptionsBuilder()
        .withBindFile("any-deannotate/simple-schema.xjb")
        .withSchemaFiles("simple-schema.xsd", "other-schema.xsd")
        .withArguments("-Xany-deannotate", "-Xany-deannotate-defaultFieldTarget=" + value)
        .build();
    assertThat(options.activePlugins).filteredOn(AnyDeannotatePlugin.class::isInstance).isNotEmpty()
        .first().matches(plugin -> ((AnyDeannotatePlugin) plugin).getDefaultFieldTarget().equals(value),
        "defaultFieldTarget is " + value);
  }

  @ParameterizedTest
  @ValueSource(strings = {"package", "class", "enum", "enum-constant", "enum-value-method", "enum-fromValue-method",
      "element"})
  void failsOnForbiddenDefaultFieldValueChange(String value) {
    assertThat(new AnyDeannotatePlugin().getDefaultFieldTarget()).isNotEqualTo(value);
    OptionsBuilder builder = new OptionsBuilder()
        .withBindFile("any-deannotate/simple-schema.xjb")
        .withSchemaFiles("simple-schema.xsd", "other-schema.xsd")
        .withArguments("-Xany-deannotate", "-Xany-deannotate-defaultFieldTarget=" + value);
    assertThatThrownBy(builder::build)
        .isInstanceOf(BadCommandLineException.class)
        .hasMessage("Error setting property [defaultFieldTarget], value [%s].", value);
  }

}
