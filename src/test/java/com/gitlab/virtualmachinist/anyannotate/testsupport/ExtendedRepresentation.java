package com.gitlab.virtualmachinist.anyannotate.testsupport;

import com.sun.codemodel.JGenerable;
import org.assertj.core.presentation.StandardRepresentation;

import static com.gitlab.virtualmachinist.anyannotate.testsupport.conditions.GenerableExpressionValueCondition.getExpressionValueAsText;

/**
 * StandardRepresentation doesn't do checks for subtypes when registering formatters.
 */
public class ExtendedRepresentation extends StandardRepresentation {

  @Override
  protected String fallbackToStringOf(Object object) {
    if (object instanceof JGenerable) {
      return getExpressionValueAsText((JGenerable) object);
    }
    return super.fallbackToStringOf(object);
  }

}
