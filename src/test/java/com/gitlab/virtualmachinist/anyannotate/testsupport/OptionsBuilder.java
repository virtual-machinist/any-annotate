package com.gitlab.virtualmachinist.anyannotate.testsupport;

import com.sun.tools.xjc.BadCommandLineException;
import com.sun.tools.xjc.Options;
import org.xml.sax.InputSource;

import java.net.URL;
import java.nio.file.Path;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Simplifies the creation of {@link Options} for tests.
 */
public class OptionsBuilder {

  private Path targetDirectory;
  private String bindFile;
  private List<String> schemaFiles;
  private String[] arguments;

  public OptionsBuilder withTargetDirectory(Path targetDirectory) {
    this.targetDirectory = targetDirectory;
    return this;
  }

  public OptionsBuilder withBindFile(String bindFile) {
    this.bindFile = bindFile;
    return this;
  }

  public OptionsBuilder withSchemaFiles(String... schemaFiles) {
    assertThat(schemaFiles).doesNotContainNull();
    this.schemaFiles = List.of(schemaFiles);
    return this;
  }

  public OptionsBuilder withArguments(String... arguments) {
    assertThat(arguments).doesNotContainNull();
    this.arguments = arguments;
    return this;
  }

  public Options build() throws BadCommandLineException {
    assertThat(schemaFiles).isNotEmpty();

    Options options = new Options();
    options.compatibilityMode = Options.EXTENSION;
    options.encoding = "UTF-8";
    options.noFileHeader = true;
    options.targetDir = targetDirectory != null ? targetDirectory.toFile() : null;
    options.verbose = true;
    if (bindFile != null) {
      options.addBindFile(getSource(bindFile));
    }
    schemaFiles.forEach(file -> options.addGrammar(getSource(file)));
    options.parseArguments(arguments);
    return options;
  }

  private InputSource getSource(String location) {
    URL resource = this.getClass().getClassLoader().getResource(location);
    assertThat(resource).isNotNull();
    return new InputSource(resource.toExternalForm());
  }

}
