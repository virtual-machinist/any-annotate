package com.gitlab.virtualmachinist.anyannotate.testsupport.asserts;

import com.github.javaparser.ast.body.*;
import org.assertj.core.api.*;

public class TypeDeclarationAssert extends AbstractAssert<TypeDeclarationAssert, TypeDeclaration<?>> {

  public TypeDeclarationAssert(TypeDeclaration<?> actual) {
    super(actual, TypeDeclarationAssert.class);
  }

  public TypeDeclarationAssert isEnum() {
    isNotNull();
    isInstanceOf(EnumDeclaration.class);
    return myself;
  }

  public TypeDeclarationAssert isClass() {
    isNotNull();
    satisfies(new Condition<>(declaration -> declaration.isClassOrInterfaceDeclaration()
        && !declaration.asClassOrInterfaceDeclaration().isInterface(), "is a class"));
    return myself;
  }

  public TypeDeclarationAssert isInterface() {
    isNotNull();
    satisfies(anInterface());
    return myself;
  }

  public ListAssert<ConstructorDeclaration> getConstructors() {
    isNotNull();
    return AssertionsForInterfaceTypes.assertThat(actual.getConstructors());
  }

  public ListAssert<MethodDeclaration> getMethods() {
    isNotNull();
    return AssertionsForInterfaceTypes.assertThat(actual.getMethods());
  }

  public ListAssert<MethodDeclaration> getMethodsBySignature(String name, String... paramTypes) {
    isNotNull();
    return AssertionsForInterfaceTypes.assertThat(actual.getMethodsBySignature(name, paramTypes));
  }

  public ListAssert<FieldDeclaration> getFields() {
    isNotNull();
    isInstanceOfAny(EnumDeclaration.class, ClassOrInterfaceDeclaration.class);
    isNot(anInterface());
    return AssertionsForInterfaceTypes.assertThat(actual.getFields());
  }

  public OptionalAssert<FieldDeclaration> getFieldByName(String name) {
    isNotNull();
    isInstanceOfAny(EnumDeclaration.class, ClassOrInterfaceDeclaration.class);
    isNot(anInterface());
    return AssertionsForClassTypes.assertThat(actual.getFieldByName(name));
  }

  public ListAssert<EnumConstantDeclaration> getEnumConstants() {
    isEnum();
    return AssertionsForInterfaceTypes.assertThat(actual.asEnumDeclaration().getEntries());
  }

  public ObjectAssert<EnumConstantDeclaration> getEnumConstant(String name) {
    return getEnumConstants()
        .filteredOn(declaration -> declaration.getName().asString().equals(name))
        .hasSize(1)
        .first();
  }

  private static Condition<TypeDeclaration<?>> anInterface() {
    return new Condition<>(declaration -> declaration.isClassOrInterfaceDeclaration()
        && declaration.asClassOrInterfaceDeclaration().isInterface(), "is an interface");
  }

}
