package com.gitlab.virtualmachinist.anyannotate.testsupport.conditions;

import com.sun.codemodel.JFormatter;
import com.sun.codemodel.JGenerable;
import org.assertj.core.api.Condition;

import java.io.StringWriter;

/**
 * Condition for matching against the actual expression value of a JGenerable.
 */
public class GenerableExpressionValueCondition extends Condition<JGenerable> {

  private final String expectedValue;

  private GenerableExpressionValueCondition(String expectedValue) {
    super(expectedValue);
    this.expectedValue = expectedValue;
    as(expectedValue);
  }

  @Override
  public boolean matches(JGenerable value) {
    return value != null && getExpressionValueAsText(value).equals(expectedValue);
  }

  public static GenerableExpressionValueCondition expressionValueEqualTo(String expectedValue, Object... params) {
    return expressionValueEqualTo(String.format(expectedValue, params));
  }

  public static GenerableExpressionValueCondition expressionValueEqualTo(String expectedValue) {
    return new GenerableExpressionValueCondition(expectedValue);
  }

  public static String getExpressionValueAsText(JGenerable generable) {
    StringWriter writer = new StringWriter();
    JFormatter formatter = new JFormatter(writer);
    generable.generate(formatter);
    return writer.toString();
  }

}
