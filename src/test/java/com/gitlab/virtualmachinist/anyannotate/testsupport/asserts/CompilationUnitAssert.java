package com.gitlab.virtualmachinist.anyannotate.testsupport.asserts;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.expr.Name;
import org.assertj.core.api.AbstractAssert;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.util.Sets.newLinkedHashSet;

public class CompilationUnitAssert extends AbstractAssert<CompilationUnitAssert, CompilationUnit> {

  public CompilationUnitAssert(CompilationUnit actual) {
    super(actual, CompilationUnitAssert.class);
  }

  public CompilationUnitAssert hasPackageName(String packageName) {
    isNotNull();
    Optional<String> expected = actual.getPackageDeclaration()
        .map(PackageDeclaration::getName)
        .map(Name::asString)
        .filter(packageName::equals);
    if (expected.isEmpty()) {
      failWithMessage("Expecting class to have package %s", packageName);
    }
    return myself;
  }

  public CompilationUnitAssert hasNonStaticImports(String... importedClasses) {
    isNotNull();
    Set<String> expectedImports = importedClasses != null ? newLinkedHashSet(importedClasses) : Set.of();
    Set<String> actualImports = getNonStaticImports();
    if (expectedImports.isEmpty()) {
      if (!actualImports.isEmpty()) {
        failWithMessage("Expecting no imports, got %s instead", actualImports);
      }
    } else if (!actualImports.containsAll(expectedImports)) {
      expectedImports.removeAll(actualImports);
      failWithMessage("Expecting imports: %s", expectedImports);
    }
    return myself;
  }

  public CompilationUnitAssert hasPrimaryType(String name) {
    isNotNull();
    Optional<String> primaryTypeName = actual.getPrimaryTypeName();
    if (primaryTypeName.isEmpty()) {
      failWithMessage("No primary type present");
    } else {
      String actualName = primaryTypeName.get();
      if (!actualName.equals(name)) {
        failWithMessage("Expecting %s primary name, got %s", name, actualName);
      }
    }
    return myself;
  }

  public TypeDeclarationAssert getPrimaryType() {
    isNotNull();
    Optional<TypeDeclaration<?>> primaryType = actual.getPrimaryType();
    if (primaryType.isEmpty()) {
      failWithMessage("No primary type present");
      return null;
    }
    return new TypeDeclarationAssert(primaryType.get());
  }

  public TypeDeclarationAssert getType(String name) {
    isNotNull();
    if (name == null || name.isEmpty()) {
      failWithMessage("Name argument cannot be empty");
      return null; // never happens
    }
    Optional<TypeDeclaration<?>> type = findType(name);
    if (type.isEmpty()) {
      failWithMessage("Type '%s' not found", name);
      return null;
    }
    return new TypeDeclarationAssert(type.get());
  }

  private Optional<TypeDeclaration<?>> findType(String name) {
    return actual.getTypes().stream()
        .filter(declaration -> declaration.getName().asString().equals(name))
        .findFirst();
  }

  private Set<String> getNonStaticImports() {
    return actual.getImports().stream()
        .filter(importDeclaration -> !importDeclaration.isStatic())
        .map(ImportDeclaration::getName)
        .map(Name::asString)
        .collect(Collectors.toCollection(LinkedHashSet::new));
  }

  public static CompilationUnitAssert assertThat(CompilationUnit compilationUnit) {
    return new CompilationUnitAssert(compilationUnit);
  }

}
