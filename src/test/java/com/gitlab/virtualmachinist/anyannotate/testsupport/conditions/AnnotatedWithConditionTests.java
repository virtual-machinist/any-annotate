package com.gitlab.virtualmachinist.anyannotate.testsupport.conditions;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AnnotatedWithConditionTests {

  @Test
  void withSomeOfMatchesSingleAnnotation() {
    ClassOrInterfaceDeclaration declaration
        = parseClassDeclaration("@foo.bar.Annotation1 @foo.bar.Annotation2 class Foo {}");
    AnnotatedWithCondition condition = AnnotatedWithCondition.withSomeOf("@foo.bar.Annotation1", "@foo.SomeAnnotation");
    assertThat(condition.matches(declaration)).isTrue();
  }

  @Test
  void withSomeOfDoesntMatchIfNoneFromExpected() {
    ClassOrInterfaceDeclaration declaration
        = parseClassDeclaration("@foo.bar.Annotation1 @foo.bar.Annotation2 class Foo {}");
    AnnotatedWithCondition condition = AnnotatedWithCondition.withSomeOf("@foo.Annotation1", "@foo.Annotation2");
    assertThat(condition.matches(declaration)).isFalse();
  }

  @Test
  void withAllOfMatchesTwoAnnotations() {
    ClassOrInterfaceDeclaration declaration =
        parseClassDeclaration("@foo.Annotation1 @foo.Annotation2 @foo.Annotation3 class Foo {}");
    AnnotatedWithCondition condition = AnnotatedWithCondition.withAllOf("@foo.Annotation1", "@foo.Annotation2");
    assertThat(condition.matches(declaration)).isTrue();
  }

  @Test
  void withAllOfDoesntMatchIfOneMissing() {
    ClassOrInterfaceDeclaration declaration = parseClassDeclaration("@foo.Annotation1 @foo.Annotation2 class Foo {}");
    AnnotatedWithCondition condition = AnnotatedWithCondition.withAllOf("@foo.Annotation1", "@foo.Annotation3");
    assertThat(condition.matches(declaration)).isFalse();
  }

  @Test
  void withOnlyMatchesSingleAnnotation() {
    ClassOrInterfaceDeclaration declaration = parseClassDeclaration("@foo.Annotation1 class Foo {}");
    AnnotatedWithCondition condition = AnnotatedWithCondition.withOnly("@foo.Annotation1");
    assertThat(condition.matches(declaration)).isTrue();
  }

  @Test
  void withOnlyIgnoresImportQualifier() {
    ClassOrInterfaceDeclaration declaration = parseClassDeclaration("import foo.Annotation1; @Annotation1 class Foo {}");
    AnnotatedWithCondition condition = AnnotatedWithCondition.withOnly("@Annotation1");
    assertThat(condition.matches(declaration)).isTrue();
  }

  @Test
  void withOnlyDoesntMatchWithMoreAnnotations() {
    ClassOrInterfaceDeclaration declaration = parseClassDeclaration("@foo.Annotation1 @foo.Annotation2 class Foo {}");
    AnnotatedWithCondition condition = AnnotatedWithCondition.withOnly("@foo.Annotation1");
    assertThat(condition.matches(declaration)).isFalse();
  }

  @Test
  void withoutAnyMatchesIfNoAnnotations() {
    ClassOrInterfaceDeclaration declaration = parseClassDeclaration("class Foo {}");
    AnnotatedWithCondition condition = AnnotatedWithCondition.withoutAny();
    assertThat(condition.matches(declaration)).isTrue();
  }

  @Test
  void withoutAnyDoesntMatchIfAnnotated() {
    ClassOrInterfaceDeclaration declaration = parseClassDeclaration("@foo.Annotation1 class Foo {}");
    AnnotatedWithCondition condition = AnnotatedWithCondition.withoutAny();
    assertThat(condition.matches(declaration)).isFalse();
  }

  @Test
  void withNoneOfClassesDoesntMatchIfAnnotatedWithDifferentParams() {
    ClassOrInterfaceDeclaration declaration = parseClassDeclaration("@foo.Annotation1(param=true) @foo.Annotation3 class Foo {}");
    AnnotatedWithCondition condition = AnnotatedWithCondition.withNoneOfClasses("@foo.Annotation1(param=false)");
    assertThat(condition.matches(declaration)).isFalse();
  }

  @Test
  void withSomeOfClassesMatchesIfAnnotatedWithDifferentParams() {
    ClassOrInterfaceDeclaration declaration = parseClassDeclaration("@foo.Annotation1(param=true) @foo.Annotation3 class Foo {}");
    AnnotatedWithCondition condition = AnnotatedWithCondition.withSomeOfClasses("@foo.Annotation1(param=false)");
    assertThat(condition.matches(declaration)).isTrue();
  }

  private static ClassOrInterfaceDeclaration parseClassDeclaration(String code) {
    CompilationUnit unit = StaticJavaParser.parse(code);
    return unit.getType(0).asClassOrInterfaceDeclaration();
  }

}
