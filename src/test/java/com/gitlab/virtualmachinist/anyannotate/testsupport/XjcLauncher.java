package com.gitlab.virtualmachinist.anyannotate.testsupport;

import com.sun.codemodel.JCodeModel;
import com.sun.tools.xjc.ConsoleErrorReporter;
import com.sun.tools.xjc.ModelLoader;
import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.model.Model;
import com.sun.tools.xjc.outline.Outline;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Simplifies launching XJC in tests. Is typically single-shot.
 */
public class XjcLauncher {

  private final Options options;
  private final ConsoleErrorReporter reporter;

  private Model model;
  private Outline outline;

  public XjcLauncher(Options options) {
    this(options, new ConsoleErrorReporter());
  }

  public XjcLauncher(Options options, ConsoleErrorReporter reporter) {
    this.options = options;
    this.reporter = reporter;
  }

  public XjcLauncher createOutline() {
    if (model == null) {
      Model model = ModelLoader.load(options, new JCodeModel(), reporter);
      assertThat(model).isNotNull();
      this.model = model;
    }
    this.outline = model.generateCode(options, reporter);
    return this;
  }

  public XjcLauncher generateCode() {
    createOutline();
    assertThat(outline).isNotNull();
    return this;
  }

  public XjcLauncher writeFiles() {
    if (model == null || outline == null) {
      generateCode();
    }
    assertThat(options.targetDir).isDirectory();
    try {
      model.codeModel.build(options.createCodeWriter());
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
    return this;
  }

  public Stream<Path> streamJavaFiles() {
    try (Stream<Path> closeableStream = Files.walk(getTargetDirectory())
          .filter(Files::isRegularFile)
          .filter(file -> file.getFileName().toString().endsWith(".java"))) {
      // Files.walk() needs to be closed, but we do want a stream
      return closeableStream.collect(Collectors.toList()).stream();
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  public Model getModel() {
    return model;
  }

  public Outline getOutline() {
    return outline;
  }

  public Path getTargetDirectory() {
    assertThat(options.targetDir).isDirectory();
    return options.targetDir.toPath();
  }

}
