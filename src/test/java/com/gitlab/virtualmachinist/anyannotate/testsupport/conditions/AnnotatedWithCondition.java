package com.gitlab.virtualmachinist.anyannotate.testsupport.conditions;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.Name;
import com.github.javaparser.ast.nodeTypes.NodeWithAnnotations;
import org.assertj.core.api.Condition;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Condition for matching annotated members using the JavaParser model.
 * Be aware that the condition matches the annotation expressions same way they are in the code (i.e. either using
 * non-qualified _or_ fully qualified class name) - class name resolution is not supported. Expressions must be valid
 * annotation expressions.
 */
public class AnnotatedWithCondition extends Condition<NodeWithAnnotations<?>> {

  private final List<AnnotationExpr> expectedAnnotations;
  private final BiFunction<List<AnnotationExpr>, List<AnnotationExpr>, Boolean> check;

  private AnnotatedWithCondition(String[] stringExpressions,
                                 BiFunction<List<AnnotationExpr>, List<AnnotationExpr>, Boolean> check,
                                 String description) {
    this.expectedAnnotations = stringExpressions != null ? parseExpressions(stringExpressions) : List.of();
    this.check = check;
    as(description, stringExpressions != null ? Arrays.toString(stringExpressions) : "[]");
  }

  private static List<AnnotationExpr> parseExpressions(String[] expressions) {
    return expressions != null
        ? Stream.of(expressions).map(StaticJavaParser::parseAnnotation).collect(Collectors.toList())
        : List.of();
  }

  @Override
  public boolean matches(NodeWithAnnotations<?> value) {
    NodeList<AnnotationExpr> actualAnnotations = value.getAnnotations();
    return check.apply(actualAnnotations, expectedAnnotations);
  }

  /**
   * Condition that matches if at least one of the annotations is present on the tested node.
   */
  public static AnnotatedWithCondition withSomeOf(String... expressions) {
    return new AnnotatedWithCondition(expressions, (actual, expected) -> expected.stream().anyMatch(actual::contains),
        "with some of %s");
  }

  /**
   * Condition that matches if at least one of the annotation classes (i.e. disregarding parameters) is present on the
   * tested node.
   */
  public static AnnotatedWithCondition withSomeOfClasses(String... expressions) {
    return new AnnotatedWithCondition(expressions, (actual, expected) -> {
      List<Name> actualNames = getClassNames(actual);
      return expected.stream().map(AnnotationExpr::getName).anyMatch(actualNames::contains);
    },
        "with some of %s classes");
  }

  /**
   * Condition that matches if none of the annotation classes (i.e. disregarding parameters) is present on the
   * tested node.
   */
  public static AnnotatedWithCondition withNoneOfClasses(String... expressions) {
    return new AnnotatedWithCondition(expressions,
        (actual, expected) -> {
          List<Name> actualNames = getClassNames(actual);
          return expected.stream().map(AnnotationExpr::getName).noneMatch(actualNames::contains);
        },
        "with none of %s classes");
  }

  private static List<Name> getClassNames(List<AnnotationExpr> actual) {
    return actual.stream().map(AnnotationExpr::getName).collect(Collectors.toList());
  }

  /**
   * Condition that matches if all the annotations are present on the tested node.
   */
  public static AnnotatedWithCondition withAllOf(String... expressions) {
    return new AnnotatedWithCondition(expressions, List::containsAll, "with all of %s");
  }

  /**
   * Condition that matches if all and no other annotations are present on the tested node.
   */
  public static AnnotatedWithCondition withOnly(String... expressions) {
    if (expressions == null || expressions.length == 0) {
      return withoutAny();
    } else {
      return new AnnotatedWithCondition(expressions, (actual, expected) -> actual.size() == expected.size()
          && actual.containsAll(expected), "with only %s");
    }
  }

  /**
   * Condition that matches if no annotations are present on the tested node.
   */
  public static AnnotatedWithCondition withoutAny() {
    return new AnnotatedWithCondition(null, (actual, expected) -> actual.isEmpty(), "without any");
  }

}
