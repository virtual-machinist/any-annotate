package com.gitlab.virtualmachinist.anyannotate;

import com.github.javaparser.ast.CompilationUnit;
import com.gitlab.virtualmachinist.anyannotate.testsupport.asserts.CompilationUnitAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;
import java.util.Map;

import static com.gitlab.virtualmachinist.anyannotate.testsupport.conditions.AnnotatedWithCondition.withNoneOfClasses;
import static com.gitlab.virtualmachinist.anyannotate.testsupport.conditions.AnnotatedWithCondition.withSomeOfClasses;
import static org.assertj.core.api.Assertions.assertThat;

abstract class AnyDeannotatePluginSimpleSchemaTests extends FileParsingPluginTests {

  @TempDir
  Path output;

  Map<String, CompilationUnit> parsed = Map.of();

  @Test
  void deannotatesPackage() {
    // empty package-info not written in jaxb-ri since 800e2043f4dd8a82d158e6a4ea816f123d27b388
    assertThat(parsed).doesNotContainKey("foo.bar.other.package-info");
  }

  @Test
  void deannotatesOneClass() {
    assertThat(parsed).containsKey("foo.bar.something.Car");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Car"))
        .hasNonStaticImports("jakarta.annotation.Generated")
        .hasPackageName("foo.bar.something")
        .hasPrimaryType("Car")
        .getType("Car")
        .isClass()
        .satisfies(withNoneOfClasses("@Generated"));
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Door"))
        .hasNonStaticImports("jakarta.annotation.Generated")
        .hasPackageName("foo.bar.something")
        .hasPrimaryType("Door")
        .getType("Door")
        .isClass()
        .satisfies(withSomeOfClasses("@Generated"));
  }

  @Test
  void deannotatesOnePropertyField() {
    assertThat(parsed).containsKey("foo.bar.something.Car");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Car"))
        .hasNonStaticImports("jakarta.annotation.Generated")
        .getType("Car")
        .getFieldByName("make")
        .isPresent()
        .get()
        .satisfies(withSomeOfClasses("@Generated"));
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Car"))
        .getType("Car")
        .getFieldByName("door")
        .isPresent()
        .get()
        .satisfies(withNoneOfClasses("@Generated"));
  }

  @Test
  void deannotatesOnePropertySetter() {
    assertThat(parsed).containsKey("foo.bar.something.Car");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Car"))
        .hasNonStaticImports("jakarta.annotation.Generated")
        .getType("Car")
        .getMethodsBySignature("setModel", "String")
        .hasSize(1)
        .first()
        .satisfies(withNoneOfClasses("@Generated"));
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Car"))
        .getType("Car")
        .getMethodsBySignature("setMake", "String")
        .hasSize(1)
        .first()
        .satisfies(withSomeOfClasses("@Generated"));
  }

  @Test
  void deannotatesOnePropertyGetter() {
    assertThat(parsed).containsKey("foo.bar.something.Car");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Car"))
        .hasNonStaticImports("jakarta.annotation.Generated")
        .getType("Car")
        .getMethodsBySignature("getMake")
        .hasSize(1)
        .first()
        .satisfies(withNoneOfClasses("@Generated"));
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Car"))
        .getType("Car")
        .getMethodsBySignature("getModel")
        .hasSize(1)
        .first()
        .satisfies(withSomeOfClasses("@Generated"));
  }

  @Test
  void deannotatesEnum() {
    assertThat(parsed).containsKey("foo.bar.something.Side");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Side"))
        .hasNonStaticImports("jakarta.xml.bind.annotation.XmlEnum")
        .hasPrimaryType("Side")
        .getType("Side")
        .isEnum()
        .satisfies(withNoneOfClasses("@Generated"))
        .satisfies(withSomeOfClasses("@XmlEnum"))
        .getEnumConstants()
        .hasSize(5);
  }

}
