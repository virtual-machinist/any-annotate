package com.gitlab.virtualmachinist.anyannotate;

import com.gitlab.virtualmachinist.anyannotate.testsupport.OptionsBuilder;
import com.gitlab.virtualmachinist.anyannotate.testsupport.XjcLauncher;
import com.sun.tools.xjc.Options;
import org.junit.jupiter.api.BeforeEach;

class AnyAnnotatePluginEmbeddedSchemaTests extends AnyAnnotatePluginSimpleSchemaTests {

  @BeforeEach
  void setUp() throws Exception {
    Options options = new OptionsBuilder()
        .withTargetDirectory(output)
        .withSchemaFiles("any-annotate/simple-schema-embedded.xsd", "any-annotate/other-schema-embedded.xsd")
        .withArguments("-Xany-annotate")
        .build();
    parsed = parseFiles(new XjcLauncher(options).writeFiles());
  }

}
