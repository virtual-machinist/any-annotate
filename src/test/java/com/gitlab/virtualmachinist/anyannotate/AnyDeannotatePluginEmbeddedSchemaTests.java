package com.gitlab.virtualmachinist.anyannotate;

import com.gitlab.virtualmachinist.anyannotate.testsupport.OptionsBuilder;
import com.gitlab.virtualmachinist.anyannotate.testsupport.XjcLauncher;
import com.sun.tools.xjc.Options;
import org.junit.jupiter.api.BeforeEach;

class AnyDeannotatePluginEmbeddedSchemaTests extends AnyDeannotatePluginSimpleSchemaTests {

  @BeforeEach
  void setUp() throws Exception {
    Options options = new OptionsBuilder()
        .withTargetDirectory(output)
        .withSchemaFiles("any-deannotate/simple-schema-embedded.xsd", "any-deannotate/other-schema-embedded.xsd")
        .withArguments("-mark-generated", "-Xany-deannotate")
        .build();
    parsed = parseFiles(new XjcLauncher(options).writeFiles());
  }

}
