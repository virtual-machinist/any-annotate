package com.gitlab.virtualmachinist.anyannotate;

import com.gitlab.virtualmachinist.anyannotate.testsupport.OptionsBuilder;
import com.gitlab.virtualmachinist.anyannotate.testsupport.XjcLauncher;
import com.sun.tools.xjc.Options;
import org.junit.jupiter.api.BeforeEach;

class AnyDeannotatePluginBindingFileTests extends AnyDeannotatePluginSimpleSchemaTests {

  @BeforeEach
  void setUp() throws Exception {
    Options options = new OptionsBuilder()
        .withTargetDirectory(output)
        .withBindFile("any-deannotate/simple-schema.xjb")
        .withSchemaFiles("simple-schema.xsd", "other-schema.xsd")
        .withArguments("-mark-generated", "-Xany-deannotate")
        .build();
    parsed = parseFiles(new XjcLauncher(options).writeFiles());
  }

}
