package com.gitlab.virtualmachinist.anyannotate.annotatable;

import com.github.javaparser.ParseProblemException;
import com.gitlab.virtualmachinist.anyannotate.testsupport.ExtendedRepresentation;
import com.sun.codemodel.*;
import jakarta.xml.bind.annotation.XmlRootElement;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.gitlab.virtualmachinist.anyannotate.testsupport.conditions.GenerableExpressionValueCondition.expressionValueEqualTo;
import static com.gitlab.virtualmachinist.anyannotate.testsupport.conditions.GenerableExpressionValueCondition.getExpressionValueAsText;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class AnnotatableFacadeTests {

  private JDefinedClass annotatable;
  private AnnotatableFacade facade;

  @BeforeAll
  static void initAssertions() {
    Assertions.useRepresentation(new ExtendedRepresentation());
  }

  @BeforeEach
  void setUp() {
    JCodeModel codeModel = new JCodeModel();
    this.annotatable = codeModel.anonymousClass(codeModel.ref("com.foo.SomeClass"));
    this.facade = new AnnotatableFacade(codeModel, annotatable);
  }

  @Test
  void addsMarkerAnnotation() {
    JAnnotationUse annotation = facade.add("@com.foo.bar.SomeAnnotation");
    assertThat(annotation.getAnnotationMembers()).isEmpty();
    assertThat(annotation)
        .has(expressionValueEqualTo("@com.foo.bar.SomeAnnotation"));
  }

  @ParameterizedTest
  @ValueSource(strings = {"123", "false", "45.45d", "123L", "java.lang.String.class", "int.class", "long[][][][].class",
      "\"something\"", "java.lang.Integer.MAX_VALUE", "'c'", "(1 + 1)"})
  void addsSingleMemberAnnotationWithSimpleParameter(String param) {
    JAnnotationUse annotation = facade.add("@com.foo.bar.SomeAnnotation(" + param + ")");
    assertThat(annotation.getAnnotationMembers())
        .hasSize(1)
        .containsKey("value")
        .hasValueSatisfying(expressionValueEqualTo(param));
    assertThat(annotation)
        .has(expressionValueEqualTo("@com.foo.bar.SomeAnnotation(%s)", param));
  }

  @ParameterizedTest
  @ValueSource(strings = {"@com.foo.bar.SomeOtherAnnotation(1)", "@com.foo.bar.SomeOtherAnnotation"})
  void addsSingleMemberAnnotationWithAnnotationParameter(String param) {
    JAnnotationUse annotation = facade.add("@com.foo.bar.SomeAnnotation(" + param + ")");
    assertThat(annotation.getAnnotationMembers())
        .hasSize(1)
        .containsKey("value")
        .hasValueSatisfying(expressionValueEqualTo(param));
    assertThat(annotation)
        .has(expressionValueEqualTo("@com.foo.bar.SomeAnnotation(%s)", param));
  }

  @ParameterizedTest
  @ValueSource(strings = {"123", "false", "45.45d", "123L", "java.lang.String.class", "\"something\"",
      "java.lang.Integer.MAX_VALUE", "'c'", "(1 + 1)"})
  void addsSingleMemberAnnotationWithSimpleValueArray(String param) {
    JAnnotationUse annotation = facade.add("@com.foo.bar.SomeAnnotation({" + param + "})");
    assertThat(annotation.getAnnotationMembers())
        .hasSize(1)
        .containsKey("value");

    List<JAnnotationValue> elements = getArrayElements(annotation.getAnnotationMembers().get("value"));
    assertThat(elements)
        .hasSize(1)
        .first()
        .has(expressionValueEqualTo(param));

    assertThat(annotation)
        .has(expressionValueEqualTo("@com.foo.bar.SomeAnnotation({%n    %s%n})", param));
  }

  @ParameterizedTest
  @ValueSource(strings = {"@com.foo.bar.SomeOtherAnnotation(1)", "@com.foo.bar.SomeOtherAnnotation"})
  void addsSingleMemberAnnotationWithAnnotationArray(String param) {
    JAnnotationUse annotation = facade.add("@com.foo.bar.SomeAnnotation({" + param + "})");
    assertThat(annotation.getAnnotationMembers())
        .hasSize(1)
        .containsKey("value");

    List<JAnnotationValue> elements = getArrayElements(annotation.getAnnotationMembers().get("value"));
    assertThat(elements)
        .hasSize(1)
        .first()
        .isInstanceOf(JAnnotationUse.class)
        .has(expressionValueEqualTo(param));

    assertThat(annotation)
        .has(expressionValueEqualTo("@com.foo.bar.SomeAnnotation({%n    %s%n})", param));
  }

  @Test
  void addsNormalAnnotationWithTwoParameters() {
    JAnnotationUse annotation = facade.add("@com.foo.bar.SomeAnnotation(value = 1, name = \"something\")");
    assertThat(annotation.getAnnotationMembers())
        .hasSize(2)
        .containsKeys("value", "name");
    assertThat(annotation.getAnnotationMembers())
        .extractingFromEntries(entry -> getExpressionValueAsText(entry.getValue()))
        .contains("1", "\"something\"");

    assertThat(annotation)
        .has(expressionValueEqualTo("@com.foo.bar.SomeAnnotation(value = 1, name = \"something\")"));
  }

  @Test
  void addThrowsExceptionOnParsingProblems() {
    assertThatThrownBy(() -> facade.add("foo"))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot parse 'foo' as an annotation")
        .hasCauseInstanceOf(ParseProblemException.class);
  }

  @Test
  void removeThrowsExceptionOnBadClassName() {
    assertThatThrownBy(() -> facade.removeAll("void"))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot parse 'void' as a class type")
        .hasCauseInstanceOf(ParseProblemException.class);
  }

  @Test
  void removesRepeatedAnnotations() {
    JAnnotationUse annotation1 = facade.add("@com.foo.bar.SomeAnnotation(value = 1, name = \"something\")");
    JAnnotationUse annotation2 = annotatable.annotate(XmlRootElement.class);
    JAnnotationUse annotation3 = facade.add("@com.foo.bar.SomeAnnotation(value = 2, name = \"something\")");
    assertThat(annotatable.annotations())
        .hasSize(3)
        .contains(annotation1, annotation2, annotation3);

    List<JAnnotationUse> removed = facade.removeAll("com.foo.bar.SomeAnnotation");
    assertThat(removed)
        .hasSize(2)
        .contains(annotation1, annotation3);
    assertThat(annotatable.annotations())
        .hasSize(1)
        .contains(annotation2);
  }

  @Test
  void replacesAnnotation() {
    JAnnotationUse annotation1 = facade.add("@com.foo.bar.SomeAnnotation(value = 1, name = \"something\")");
    JAnnotationUse annotation2 = facade.add("@com.foo.bar.SomeAnnotation(value = 2, name = \"something\")");
    assertThat(annotatable.annotations())
        .hasSize(2)
        .contains(annotation1, annotation2);
    JAnnotationUse replacement = facade.replaceAll("@com.foo.bar.SomeAnnotation");
    assertThat(replacement).has(expressionValueEqualTo("@com.foo.bar.SomeAnnotation"));
    assertThat(annotatable.annotations())
        .hasSize(1)
        .contains(replacement);
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  private static List<JAnnotationValue> getArrayElements(JAnnotationValue arrayValue) {
    assertThat(arrayValue)
        .isInstanceOf(JAnnotationArrayMember.class);
    // a bit ugly, since JAnnotationArrayMember doesn't expose its true values, but should work regardless
    Collection<JAnnotationValue> values = (Collection) ((JAnnotationArrayMember) arrayValue).annotations();
    return new ArrayList<>(values);
  }

}
