package com.gitlab.virtualmachinist.anyannotate;

import com.gitlab.virtualmachinist.anyannotate.testsupport.OptionsBuilder;
import com.gitlab.virtualmachinist.anyannotate.testsupport.XjcLauncher;
import com.gitlab.virtualmachinist.anyannotate.testsupport.asserts.CompilationUnitAssert;
import com.sun.tools.xjc.Options;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static com.gitlab.virtualmachinist.anyannotate.testsupport.conditions.AnnotatedWithCondition.withSomeOf;
import static org.assertj.core.api.Assertions.assertThat;

class AnyAnnotatePluginBindingFileTests extends AnyAnnotatePluginSimpleSchemaTests {

  @BeforeEach
  void setUp() throws Exception {
    Options options = new OptionsBuilder()
        .withTargetDirectory(output)
        .withBindFile("any-annotate/simple-schema.xjb")
        .withSchemaFiles("simple-schema.xsd", "other-schema.xsd")
        .withArguments("-Xany-annotate")
        .build();
    parsed = parseFiles(new XjcLauncher(options).writeFiles());
  }

  @Test
  void annotatesMultipleClasses() throws Exception {
    Path multipleClassesOutput = Files.createDirectory(output.resolve("multi"));
    Options options = new OptionsBuilder()
        .withTargetDirectory(multipleClassesOutput)
        .withBindFile("any-annotate/multiple-classes.xjb")
        .withSchemaFiles("simple-schema.xsd", "other-schema.xsd")
        .withArguments("-Xany-annotate")
        .build();
    parsed = parseFiles(new XjcLauncher(options).writeFiles());
    List<String> complexTypes = List.of("foo.bar.something.Foo", "foo.bar.something.Door",
        "foo.bar.something.Car", "foo.bar.other.Bar");
    assertThat(complexTypes).allSatisfy(complexType ->
        CompilationUnitAssert.assertThat(parsed.get(complexType))
            .hasNonStaticImports("foo.bar.ClassMarker")
            .getPrimaryType()
            .satisfies(withSomeOf("@ClassMarker"))
    );
  }

}
