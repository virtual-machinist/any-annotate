package com.gitlab.virtualmachinist.batchannotate;

import com.github.javaparser.ast.CompilationUnit;
import com.gitlab.virtualmachinist.anyannotate.FileParsingPluginTests;
import com.gitlab.virtualmachinist.anyannotate.testsupport.OptionsBuilder;
import com.gitlab.virtualmachinist.anyannotate.testsupport.XjcLauncher;
import com.sun.tools.xjc.Options;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.nio.file.Path;
import java.util.Map;

import static com.gitlab.virtualmachinist.anyannotate.testsupport.conditions.AnnotatedWithCondition.withOnly;
import static com.gitlab.virtualmachinist.anyannotate.testsupport.conditions.AnnotatedWithCondition.withSomeOf;
import static org.assertj.core.api.Assertions.assertThat;

class BatchAnnotatePluginCompatibilityTests extends FileParsingPluginTests {

  @TempDir
  Path output;

  @CsvSource({"-Xbatch-annotate,-Xany-annotate", "-Xany-annotate,-Xbatch-annotate"})
  @ParameterizedTest
  void annotatesPackagesWithSchemaBindings(String firstPlugin, String secondPlugin) throws Exception {
    Options options = new OptionsBuilder()
        .withTargetDirectory(output)
        .withBindFile("batch-annotate/simple-schema-other-plugins.xjb")
        .withSchemaFiles("simple-schema.xsd", "other-schema.xsd")
        .withArguments(firstPlugin, secondPlugin)
        .build();

    Map<String, CompilationUnit> parsed = parseFiles(new XjcLauncher(options).writeFiles());

    assertThat(parsed).containsKeys("foo.bar.other.package-info", "foo.bar.something.package-info");
    assertThat(parsed.get("foo.bar.other.package-info").getPackageDeclaration())
        .isNotEmpty().get()
        .satisfies(declaration -> assertThat(declaration.getName().asString()).isEqualTo("foo.bar.other"))
        .satisfies(withSomeOf("@com.foo.PackageAnnotation(value = \"foo.bar.other\")",
            "@javax.xml.bind.annotation.XmlSchema(namespace = \"urn:replaced\")"));
    assertThat(parsed.get("foo.bar.something.package-info").getPackageDeclaration())
        .isNotEmpty().get()
        .satisfies(declaration -> assertThat(declaration.getName().asString()).isEqualTo("foo.bar.something"))
        .satisfies(withOnly("@com.foo.PackageAnnotation(\"foo.bar.something\")"));
  }

  @CsvSource({"-Xbatch-annotate,-Xany-annotate", "-Xany-annotate,-Xbatch-annotate"})
  @ParameterizedTest
  void annotatesPackagesWithoutSchemaBindings(String firstPlugin, String secondPlugin) throws Exception {
    Options options = new OptionsBuilder()
        .withTargetDirectory(output)
        .withBindFile("batch-annotate/simple-schema-other-plugins-no-schemabindings.xjb")
        .withSchemaFiles("simple-schema.xsd", "other-schema.xsd")
        .withArguments(firstPlugin, secondPlugin)
        .build();

    Map<String, CompilationUnit> parsed = parseFiles(new XjcLauncher(options).writeFiles());

    assertThat(parsed).containsKeys("other.schema.package-info", "generated.package-info");
    assertThat(parsed.get("other.schema.package-info").getPackageDeclaration())
        .isNotEmpty().get()
        .satisfies(declaration -> assertThat(declaration.getName().asString()).isEqualTo("other.schema"))
        .satisfies(withSomeOf("@com.foo.PackageAnnotation(value = \"foo.bar.other\")",
            "@javax.xml.bind.annotation.XmlSchema(namespace = \"urn:replaced\")"));
    assertThat(parsed.get("generated.package-info").getPackageDeclaration())
        .isNotEmpty().get()
        .satisfies(declaration -> assertThat(declaration.getName().asString()).isEqualTo("generated"))
        .satisfies(withOnly("@com.foo.PackageAnnotation(\"foo.bar.something\")"));
  }

}
