package com.gitlab.virtualmachinist.batchannotate;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.gitlab.virtualmachinist.anyannotate.FileParsingPluginTests;
import com.gitlab.virtualmachinist.anyannotate.testsupport.OptionsBuilder;
import com.gitlab.virtualmachinist.anyannotate.testsupport.XjcLauncher;
import com.gitlab.virtualmachinist.anyannotate.testsupport.asserts.CompilationUnitAssert;
import com.sun.tools.xjc.Options;
import org.assertj.core.api.InstanceOfAssertFactories;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;
import java.util.Map;

import static com.gitlab.virtualmachinist.anyannotate.testsupport.conditions.AnnotatedWithCondition.withOnly;
import static com.gitlab.virtualmachinist.anyannotate.testsupport.conditions.AnnotatedWithCondition.withSomeOf;
import static org.assertj.core.api.Assertions.assertThat;

class BatchAnnotatePluginTests extends FileParsingPluginTests {

  @TempDir
  Path output;

  Map<String, CompilationUnit> parsed = Map.of();

  @BeforeEach
  void setUp() throws Exception {
    Options options = new OptionsBuilder()
        .withTargetDirectory(output)
        .withBindFile("batch-annotate/simple-schema.xjb")
        .withSchemaFiles("simple-schema.xsd", "other-schema.xsd")
        .withArguments("-Xbatch-annotate")
        .build();
    parsed = parseFiles(new XjcLauncher(options).writeFiles());
  }

  @Test
  void annotatesField() {
    parsed.keySet().removeIf(key -> !key.endsWith("Car"));
    assertThat(parsed)
        .hasSize(1)
        .containsKey("foo.bar.something.Car");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Car"))
        .hasPackageName("foo.bar.something")
        .hasNonStaticImports("com.foo.FieldAnnotation")
        .hasPrimaryType("Car")
        .getType("Car")
        .isClass()
        .getFieldByName("make")
        .isPresent()
        .get()
        .satisfies(withSomeOf("@FieldAnnotation"));
  }

  @Test
  void annotatesEnumConstant() {
    assertThat(parsed).containsKey("foo.bar.something.Side");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Side"))
        .hasPrimaryType("Side")
        .getType("Side")
        .isEnum()
        .getEnumConstants()
        .hasSize(5)
        .filteredOn(constant -> constant.getNameAsString().endsWith("FRONT"))
        .hasSize(2)
        .allSatisfy(constant -> assertThat(constant).satisfies(withOnly("@com.foo.EnumConstantAnnotation")));
  }

  @Test
  void annotatesMethodParameter() {
    assertThat(parsed.values())
        .filteredOnAssertions(unit -> CompilationUnitAssert.assertThat(unit).hasPackageName("foo.bar.something"))
        .isNotEmpty()
        .allSatisfy(unit -> CompilationUnitAssert.assertThat(unit)
            .getPrimaryType()
            .getMethods()
            .filteredOn(method -> method.getNameAsString().startsWith("set"))
            .flatExtracting(MethodDeclaration::getParameters)
            .asInstanceOf(InstanceOfAssertFactories.list(Parameter.class))
            .filteredOn(parameter -> parameter.getType().asString().equals("java.lang.String"))
            .allSatisfy(parameter -> assertThat(parameter).satisfies(withSomeOf("@com.foo.StringParamAnnotation"))));
  }

  @Test
  void annotatesMethod() {
    assertThat(parsed).containsKey("foo.bar.something.Car");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.something.Car"))
        .hasPackageName("foo.bar.something")
        .hasNonStaticImports("com.foo.MethodAnnotation")
        .hasPrimaryType("Car")
        .getType("Car")
        .isClass()
        .getMethodsBySignature("getDoor")
        .hasSize(1)
        .first()
        .satisfies(withSomeOf("@MethodAnnotation"));
  }

  @Test
  void annotatesPackage() {
    assertThat(parsed).doesNotContainKey("foo.bar.something.package-info");
    assertThat(parsed).containsKey("foo.bar.other.package-info");
    assertThat(parsed.get("foo.bar.other.package-info").getPackageDeclaration())
        .isNotEmpty().get()
        .satisfies(declaration -> assertThat(declaration.getName().asString()).isEqualTo("foo.bar.other"))
        .satisfies(withSomeOf("@com.foo.PackageAnnotation", "@javax.xml.bind.annotation.XmlSchema(namespace = \"urn:replaced\")"));
  }

  @Test
  void annotatesClass() {
    parsed.keySet().removeIf(key -> key.endsWith("package-info") || key.startsWith("foo.bar.something"));
    assertThat(parsed.values())
        .allSatisfy(unit -> CompilationUnitAssert.assertThat(unit)
            .hasPackageName("foo.bar.other")
            .hasNonStaticImports("com.foo.ClassAnnotation")
            .getPrimaryType()
            .satisfies(withSomeOf("@ClassAnnotation")));
  }

  @Test
  void annotatesConstructor() {
    assertThat(parsed).containsKey("foo.bar.other.ObjectFactory");
    CompilationUnitAssert.assertThat(parsed.get("foo.bar.other.ObjectFactory"))
        .hasPackageName("foo.bar.other")
        .hasNonStaticImports("com.foo.bar.ConstructorAnnotation")
        .hasPrimaryType("ObjectFactory")
        .getType("ObjectFactory")
        .isClass()
        .getConstructors()
        .hasSize(1)
        .first()
        .satisfies(withOnly("@ConstructorAnnotation"));
  }

}
