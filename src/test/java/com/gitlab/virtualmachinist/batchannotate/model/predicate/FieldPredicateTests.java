package com.gitlab.virtualmachinist.batchannotate.model.predicate;

import com.gitlab.virtualmachinist.batchannotate.model.ModifierType;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JMod;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Set;

import static com.gitlab.virtualmachinist.batchannotate.model.StringMatcher.anyMatch;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class FieldPredicateTests {

  private JCodeModel model;
  private JDefinedClass klass;

  @BeforeEach
  void setUp() throws Exception {
    model = new JCodeModel();
    klass = model._class("foo.bar.Test");
  }

  @CsvSource({
      "name,name,true",
      "fff,name,false"
  })
  @ParameterizedTest
  void fieldNameMatches(String name, String expectedName, boolean result) {
    FieldPredicate predicate = new FieldPredicate(expectedName::equals, "int"::equals, null);
    assertThat(predicate.test(klass.field(JMod.NONE, model.INT, name))).isEqualTo(result);
  }

  @CsvSource({
      "java.lang.Object,java.lang.Object,true",
      "java.lang.Object,int,false",
      "int,java.lang.Object,false",
      "int,int,true"
  })
  @ParameterizedTest
  void typesMatch(Class<?> type, String expectedTypeName, boolean result) {
    JFieldVar field = klass.field(JMod.PRIVATE, type, "name");
    assertThat(new FieldPredicate(anyMatch(), expectedTypeName::equals, null).test(field)).isEqualTo(result);
  }

  @CsvSource({
      "private final,private final,true",
      "private static,private,false",
      "none,none,true",
      "none,final,false"
  })
  @ParameterizedTest
  void modifiersMatch(String modifierLabels, String expectedLabels, boolean result) {
    Set<ModifierType> modifiers = ModifierType.fromLabels(modifierLabels);
    Set<ModifierType> expected = ModifierType.fromLabels(expectedLabels);
    JFieldVar field = klass.field(ModifierType.asModifierValue(modifiers), int.class, "name");
    assertThat(new FieldPredicate(anyMatch(), anyMatch(), expected).test(field)).isEqualTo(result);
  }

  @Test
  void throwsExceptionOnInvalidFieldModifier() {
    assertThatThrownBy(() -> new FieldPredicate(anyMatch(), anyMatch(), Set.of(ModifierType.ABSTRACT)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Invalid field modifier: abstract");
  }

  @Test
  void throwsExceptionOnNoneWithAnotherModifier() {
    assertThatThrownBy(() -> new FieldPredicate(anyMatch(), anyMatch(),
        Set.of(ModifierType.PUBLIC, ModifierType.NONE)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot use none with other field modifiers");
  }

}
