package com.gitlab.virtualmachinist.batchannotate.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

class PatternTypeTests {

  @CsvSource({
      ",,true",
      ".*,,true",
      ".+,,false",
      "(foo|bar),foo,true",
      "(foo|bar),foo,true",
      "(foo|bar),baz,false",
      "hello,hello world,true",
      "^hello$,hello world,false",
  })
  @ParameterizedTest
  void regexMatches(String pattern, String test, boolean matches) {
    assertThat(PatternType.REGEX.apply(pattern == null ? "" : pattern).matches(test == null ? "" : test))
        .isEqualTo(matches);
  }

  @CsvSource({
      ",,true",
      ",fff,false", // an empty wildcard = no match
      "*,,true",
      "*,fff,true",
      "?,fff,false",
      "?,f,true",
      "ca?,cat,true",
      "ca?,camel,false",
      "ca*,camel,true",
      "!foo,foo,false",
      "!,foo,true", // a negated empty wildcard = any match
      "!,,false",
      "[foo],[foo],true"
  })
  @ParameterizedTest
  void wildcardMatches(String pattern, String test, boolean matches) {
    assertThat(PatternType.WILDCARD.apply(pattern == null ? "" : pattern).matches(test == null ? "" : test))
        .isEqualTo(matches);
  }

  @Test
  void anyPatternsYieldsAnyMatchOnNullInput() {
    assertThat(PatternType.values())
        .allSatisfy(type -> {
          assertThat(type.apply(null).matches("foobar")).isTrue();
          assertThat(type.apply(null).matches(null)).isTrue();
          assertThat(type.apply(null).matches("")).isTrue();
        });
  }

}
