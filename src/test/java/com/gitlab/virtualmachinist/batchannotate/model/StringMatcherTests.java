package com.gitlab.virtualmachinist.batchannotate.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

class StringMatcherTests {

  @CsvSource({
      "foobarTEST,TEST,true",
      "TEST,.*,true",
      "TEST,test,false",
      "TEST,^TEST$,true"
  })
  @ParameterizedTest
  void ofRegexProducesRegexMatcher(String input, String pattern, boolean matches) {
    assertThat(StringMatcher.ofRegex(pattern).matches(input)).isEqualTo(matches);
  }

  @Test
  void anyMatchReturnsTrue() {
    assertThat(StringMatcher.anyMatch().matches(null)).isTrue();
    assertThat(StringMatcher.anyMatch().matches("")).isTrue();
    assertThat(StringMatcher.anyMatch().matches("true")).isTrue();
  }

  @Test
  void invertedInvertsResult() {
    assertThat(StringMatcher.anyMatch().inverted().matches(null)).isFalse();
    assertThat(StringMatcher.anyMatch().inverted().matches("")).isFalse();
    assertThat(StringMatcher.anyMatch().inverted().matches("true")).isFalse();
  }

}
