package com.gitlab.virtualmachinist.batchannotate.model.predicate;


import com.gitlab.virtualmachinist.batchannotate.model.ClassType;
import com.sun.codemodel.JCodeModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static com.gitlab.virtualmachinist.batchannotate.model.StringMatcher.anyMatch;
import static org.assertj.core.api.Assertions.assertThat;

class ClassPredicateTests {

  private JCodeModel model;

  @BeforeEach
  void setUp() {
    model = new JCodeModel();
  }

  @CsvSource({
      "foo.bar.Test,foo.bar.Test,true",
      "foo.bar.Test,Test,false",
  })
  @ParameterizedTest
  void classFullNameMatches(String className, String expectedName, boolean result) throws Exception {
    assertThat(new ClassPredicate(expectedName::equals, null).test(model._class(className)))
        .isEqualTo(result);
  }

  @CsvSource({
      "foo.bar.Test,CLASS,CLASS,true",
      "foo.bar.Test,CLASS,ENUM,false",
      "foo.bar.Test,CLASS,INTERFACE,false",
      "foo.bar.Test,CLASS,ANNOTATION,false",
      "foo.bar.Test,ENUM,ENUM,true",
      "foo.bar.Test,ENUM,CLASS,false",
      "foo.bar.Test,ENUM,INTERFACE,false",
      "foo.bar.Test,ENUM,ANNOTATION,false",
      "foo.bar.Test,INTERFACE,INTERFACE,true",
      "foo.bar.Test,INTERFACE,CLASS,false",
      "foo.bar.Test,INTERFACE,ENUM,false",
      "foo.bar.Test,INTERFACE,ANNOTATION,false",
      "foo.bar.Test,ANNOTATION,ANNOTATION,true",
      "foo.bar.Test,ANNOTATION,CLASS,false",
      "foo.bar.Test,ANNOTATION,INTERFACE,false",
      "foo.bar.Test,ANNOTATION,ENUM,false",
  })
  @ParameterizedTest
  void classTypeMatches(String className, ClassType classType, ClassType expectedType, boolean result) throws Exception {
    assertThat(new ClassPredicate(anyMatch(), expectedType).test(model._class(className, classType.getType())))
        .isEqualTo(result);
  }

}
