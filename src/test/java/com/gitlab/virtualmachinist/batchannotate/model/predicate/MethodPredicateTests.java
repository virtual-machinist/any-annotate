package com.gitlab.virtualmachinist.batchannotate.model.predicate;

import com.gitlab.virtualmachinist.batchannotate.model.Method;
import com.gitlab.virtualmachinist.batchannotate.model.ModifierType;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.gitlab.virtualmachinist.batchannotate.model.StringMatcher.anyMatch;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class MethodPredicateTests {

  private JCodeModel model;
  private JDefinedClass klass;

  @BeforeEach
  void setUp() throws Exception {
    model = new JCodeModel();
    klass = model._class("foo.bar.Test");
  }

  @Test
  void methodNameMatchesForConstructor() {
    JMethod constructor = klass.constructor(JMod.PRIVATE);
    assertThat(new MethodPredicate("<init>"::equals, null, null).test(new Method(constructor))).isTrue();
    assertThat(new MethodPredicate("test"::equals, null, null).test(new Method(constructor))).isFalse();
  }

  @Test
  void methodNameMatchesForRegularMethod() {
    JMethod method = klass.method(JMod.PUBLIC, model.INT, "test");
    assertThat(new MethodPredicate("<init>"::equals, null, null).test(new Method(method))).isFalse();
    assertThat(new MethodPredicate("test"::equals, null, null).test(new Method(method))).isTrue();
  }

  @Test
  void modifiersMatchWithSingleModifier() {
    JMethod method = klass.method(JMod.PUBLIC, model.INT, "test");
    assertThat(new MethodPredicate(anyMatch(), Set.of(ModifierType.PUBLIC), null).test(new Method(method)))
        .isTrue();
    assertThat(new MethodPredicate(anyMatch(), Set.of(ModifierType.PUBLIC, ModifierType.FINAL), null)
        .test(new Method(method)))
        .isFalse();
    assertThat(new MethodPredicate(anyMatch(), Set.of(ModifierType.PRIVATE), null).test(new Method(method)))
        .isFalse();
  }

  @Test
  void modifiersMatchWithMultipleModifiers() {
    JMethod method = klass.method(JMod.PUBLIC | JMod.FINAL, model.INT, "test");
    assertThat(new MethodPredicate(anyMatch(), Set.of(ModifierType.PUBLIC, ModifierType.FINAL), null)
        .test(new Method(method)))
        .isTrue();
    assertThat(new MethodPredicate(anyMatch(), Set.of(ModifierType.PUBLIC), null).test(new Method(method)))
        .isFalse();
    assertThat(new MethodPredicate(anyMatch(), Set.of(ModifierType.NATIVE), null).test(new Method(method)))
        .isFalse();
  }

  @Test
  void parameterTypesMatchOnNumberOfParams() {
    JMethod method0 = klass.method(JMod.NONE, model.INT, "test0");
    JMethod method1 = klass.method(JMod.NONE, model.INT, "test1");
    method1.param(model.LONG, "param1");
    JMethod method2 = klass.method(JMod.NONE, model.INT, "test2");
    method2.param(model.LONG, "param1");
    method2.param(model.LONG, "param2");

    assertThat(new MethodPredicate(anyMatch(), null, List.of()).test(new Method(method0))).isTrue();
    assertThat(new MethodPredicate(anyMatch(), null, List.of()).test(new Method(method1))).isFalse();
    assertThat(new MethodPredicate(anyMatch(), null, List.of()).test(new Method(method2))).isFalse();

    assertThat(new MethodPredicate(anyMatch(), null, List.of(anyMatch())).test(new Method(method0))).isFalse();
    assertThat(new MethodPredicate(anyMatch(), null, List.of(anyMatch())).test(new Method(method1))).isTrue();
    assertThat(new MethodPredicate(anyMatch(), null, List.of(anyMatch())).test(new Method(method2))).isFalse();

    assertThat(new MethodPredicate(anyMatch(), null, List.of(anyMatch(), anyMatch())).test(new Method(method0)))
        .isFalse();
    assertThat(new MethodPredicate(anyMatch(), null, List.of(anyMatch(), anyMatch())).test(new Method(method1)))
        .isFalse();
    assertThat(new MethodPredicate(anyMatch(), null, List.of(anyMatch(), anyMatch())).test(new Method(method2)))
        .isTrue();
  }

  @Test
  void parameterTypesMatchOnSingleParamType() {
    JMethod method = klass.method(JMod.NONE, model.INT, "test");
    method.param(model.ref(Object.class).array(), "param1");

    assertThat(new MethodPredicate(anyMatch(), null, List.of("java.lang.Object[]"::equals))
        .test(new Method(method)))
        .isTrue();
    assertThat(new MethodPredicate(anyMatch(), null, List.of("char"::equals)).test(new Method(method)))
        .isFalse();
  }

  @Test
  void parameterTypesMatchOnMultipleParamTypes() {
    JMethod method = klass.method(JMod.NONE, model.INT, "test");
    method.param(model.CHAR, "param1");
    method.param(model.ref(List.class).narrow(String.class), "param2");

    assertThat(new MethodPredicate(anyMatch(), null, List.of("char"::equals,
        "java.util.List<java.lang.String>"::equals)).test(new Method(method)))
        .isTrue();
    assertThat(new MethodPredicate(anyMatch(), null, List.of("java.util.List<java.lang.String>"::equals,
        "char"::equals)).test(new Method(method)))
        .isFalse();
    assertThat(new MethodPredicate(anyMatch(), null, List.of("char"::equals, "char"::equals))
        .test(new Method(method)))
        .isFalse();
    assertThat(new MethodPredicate(anyMatch(), null, List.of("java.util.List<java.lang.String>"::equals,
        "java.util.List<java.lang.String>"::equals)).test(new Method(method)))
        .isFalse();
  }

  @Test
  void parameterTypesMatchOnMultipleGenericParameters() {
    JMethod method = klass.method(JMod.NONE, model.INT, "test");
    method.param(model.ref(Map.class).narrow(String.class, Integer.class), "param");
    assertThat(new MethodPredicate(anyMatch(), null, List.of("java.util.Map<java.lang.String,java.lang.Integer>"::equals))
        .test(new Method(method)))
        .isTrue();
    assertThat(new MethodPredicate(anyMatch(), null, List.of("char"::equals)).test(new Method(method)))
        .isFalse();
    assertThat(new MethodPredicate(anyMatch(), null, List.of("java.util.Map"::equals)).test(new Method(method)))
        .isFalse();
  }

  @Test
  void throwsExceptionOnInvalidMethodModifier() {
    assertThatThrownBy(() -> new MethodPredicate(anyMatch(), Set.of(ModifierType.TRANSIENT), null))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Invalid method modifier: transient");
  }

  @Test
  void throwsExceptionOnNoneWithAnotherModifier() {
    assertThatThrownBy(() -> new MethodPredicate(anyMatch(), Set.of(ModifierType.PUBLIC, ModifierType.NONE), null))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot use none with other method modifiers");
  }

}
