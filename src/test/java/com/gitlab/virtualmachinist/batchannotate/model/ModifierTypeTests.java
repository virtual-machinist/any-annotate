package com.gitlab.virtualmachinist.batchannotate.model;

import com.sun.codemodel.JMod;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.EnumSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ModifierTypeTests {

  @CsvSource({
      "public,PUBLIC",
      "protected,PROTECTED",
      "private,PRIVATE",
      "final,FINAL",
      "static,STATIC",
      "abstract,ABSTRACT",
      "native,NATIVE",
      "synchronized,SYNCHRONIZED",
      "transient,TRANSIENT",
      "volatile,VOLATILE",
      "none,NONE"
  })
  @ParameterizedTest
  void fromLabelReturnsLabel(String label, ModifierType modifier) {
    assertThat(ModifierType.fromLabel(label)).isEqualTo(modifier);
  }

  @Test
  void fromLabelThrowsExceptionOnBadInput() {
    assertThatThrownBy(() -> ModifierType.fromLabel("foobar"))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Unknown modifier: foobar");
  }

  @Test
  void fromLabelsReturnsLabels() {
    String labels = "public protected private final static abstract native synchronized transient volatile none";
    assertThat(ModifierType.fromLabels(labels)).isEqualTo(EnumSet.allOf(ModifierType.class));
  }

  @Test
  void fromLabelsReturnsEmptyOnNullOrEmpty() {
    assertThat(ModifierType.fromLabels("")).isEmpty();
    assertThat(ModifierType.fromLabels(null)).isEmpty();
  }

  @Test
  void asModifierValueThrowsExceptionOnNullOrEmpty() {
    //noinspection DataFlowIssue
    assertThatThrownBy(() -> ModifierType.asModifierValue(null))
        .isInstanceOf(IllegalArgumentException.class);
    assertThatThrownBy(() -> ModifierType.asModifierValue(Set.of()))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void modifiersMatchReturnsTrueIfAllMatch() {
    assertThat(ModifierType.modifiersMatch(Set.of(ModifierType.PRIVATE, ModifierType.STATIC),
        JMod.PRIVATE | JMod.STATIC)).isTrue();
    assertThat(ModifierType.modifiersMatch(Set.of(ModifierType.PRIVATE, ModifierType.STATIC),
        JMod.PRIVATE)).isFalse();
    assertThat(ModifierType.modifiersMatch(Set.of(ModifierType.NATIVE), JMod.NATIVE)).isTrue();
  }

  @Test
  void modifiersMatchReturnsTrueOnEmptyNone() {
    assertThat(ModifierType.modifiersMatch(Set.of(), JMod.NONE)).isTrue();
  }

  @Test
  void modifiersMatchReturnsTrueOnNullForAnyModifier() {
    assertThat(ModifierType.values())
        .allSatisfy(modifier -> {
          assertThat(ModifierType.modifiersMatch(null, modifier.getModifier())).isTrue();
          assertThat(ModifierType.modifiersMatch(Set.of(), modifier.getModifier())).isTrue();
        });
  }

}
