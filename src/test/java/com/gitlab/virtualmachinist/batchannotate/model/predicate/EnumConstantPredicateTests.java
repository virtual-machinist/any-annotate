package com.gitlab.virtualmachinist.batchannotate.model.predicate;

import com.sun.codemodel.ClassType;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JEnumConstant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

class EnumConstantPredicateTests {

  private JCodeModel model;

  @BeforeEach
  void setUp() {
    model = new JCodeModel();
  }

  @CsvSource({
      "FOO,FOO,true",
      "BAR_BAR,FOO,false"
  })
  @ParameterizedTest
  void enumConstantMatches(String name, String expectedName, boolean result) throws Exception {
    JDefinedClass klass = model._class("foo.bar.MyEnum", ClassType.ENUM);
    JEnumConstant enumConstant = klass.enumConstant(name);
    assertThat(new EnumConstantPredicate(expectedName::equals).test(enumConstant)).isEqualTo(result);
  }

}
