package com.gitlab.virtualmachinist.batchannotate.model.config;

import com.gitlab.virtualmachinist.batchannotate.model.StringMatcher;
import com.gitlab.virtualmachinist.batchannotate.model.predicate.*;
import org.assertj.core.api.InstanceOfAssertFactories;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.ReflectionUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class BatchAnnotationConfigProcessorTests {

  private DocumentBuilder builder;

  @BeforeEach
  void setUp() throws Exception {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
    factory.setNamespaceAware(true);
    builder = factory.newDocumentBuilder();
  }

  @Test
  void noTargetThrowsException() throws Exception {
    Element element = loadXml("batch-annotate/no-target.xml");
    assertThatThrownBy(() -> new BatchAnnotationConfigProcessor(element))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Invalid AnnotationSpecification - no target present");
  }

  @Test
  void processesPackageSpec() throws Exception {
    Element element = loadXml("batch-annotate/package-annotate.xml");
    BatchAnnotationConfigProcessor processor = new BatchAnnotationConfigProcessor(element);
    assertThat(processor.getPackageAnnotation())
        .get()
        .extracting(PackageAnnotationConfig::getExpressions)
        .asInstanceOf(InstanceOfAssertFactories.list(AnnotationExpressionConfig.class))
        .hasSize(1)
        .first()
        .satisfies(expression -> {
          assertThat(expression.getExpression()).isNotNull();
          assertThat(expression.isReplaced()).isTrue();
        });
  }

  @Test
  void processesClassSpec() throws Exception {
    Element element = loadXml("batch-annotate/class-annotate.xml");
    BatchAnnotationConfigProcessor processor = new BatchAnnotationConfigProcessor(element);
    assertThat(processor.getClassAnnotations())
        .hasSize(1)
        .first()
        .satisfies(config -> {
          assertThat(config.getClassFilter()).isInstanceOf(ClassPredicate.class);
          assertThat(config.getExpressions())
              .hasSize(1)
              .first()
              .satisfies(expression -> {
                assertThat(expression.getExpression()).isNotNull();
                assertThat(expression.isReplaced()).isFalse();
              });
        });
  }

  @Test
  void processesEnumConstantSpec() throws Exception {
    Element element = loadXml("batch-annotate/enum-constant-annotate.xml");
    BatchAnnotationConfigProcessor processor = new BatchAnnotationConfigProcessor(element);
    assertThat(processor.getEnumConstantAnnotations())
        .hasSize(1)
        .first()
        .satisfies(config -> {
          assertThat(config.getClassFilter()).isInstanceOf(ClassPredicate.class);
          assertThat(config.getEnumConstantFilter()).isInstanceOf(EnumConstantPredicate.class);
          assertThat(config.getExpressions())
              .hasSize(1)
              .first()
              .satisfies(expression -> {
                assertThat(expression.getExpression()).isNotNull();
                assertThat(expression.isReplaced()).isFalse();
              });
        });
  }

  @Test
  void processesFieldFilterSpec() throws Exception {
    Element element = loadXml("batch-annotate/field-annotate.xml");
    BatchAnnotationConfigProcessor processor = new BatchAnnotationConfigProcessor(element);
    assertThat(processor.getFieldAnnotations())
        .hasSize(1)
        .first()
        .satisfies(config -> {
          assertThat(config.getClassFilter()).isInstanceOf(ClassPredicate.class);
          assertThat(config.getFieldFilter()).isInstanceOf(FieldPredicate.class);
          assertThat(config.getExpressions())
              .hasSize(1)
              .first()
              .satisfies(expression -> {
                assertThat(expression.getExpression()).isNotNull();
                assertThat(expression.isReplaced()).isFalse();
              });
        });
  }

  @Test
  void processesMethodThrowsExceptionOnRegexPatternWithParameterTypes() throws Exception {
    Element element = loadXml("batch-annotate/method-annotate-regex-params.xml");
    assertThatThrownBy(() -> new BatchAnnotationConfigProcessor(element))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Parameter type name matching supported only for wildcard patterns");
  }

  @Test
  void processesMethodFilterSpec() throws Exception {
    Element element = loadXml("batch-annotate/method-annotate.xml");
    BatchAnnotationConfigProcessor processor = new BatchAnnotationConfigProcessor(element);
    assertThat(processor.getMethodAnnotations())
        .hasSize(1)
        .first()
        .satisfies(config -> {
          assertThat(config.getClassFilter()).isInstanceOf(ClassPredicate.class);
          assertThat(config.getMethodFilter()).isInstanceOf(MethodPredicate.class);
          assertThat(config.getExpressions())
              .hasSize(1)
              .first()
              .satisfies(expression -> {
                assertThat(expression.getExpression()).isNotNull();
                assertThat(expression.isReplaced()).isFalse();
              });
        });
  }

  @Test
  void processesMethodFilterSpecWithMultipleGenericParameters() throws Exception {
    Element element = loadXml("batch-annotate/method-annotate-generic-params.xml");
    BatchAnnotationConfigProcessor processor = new BatchAnnotationConfigProcessor(element);
    assertThat(processor.getMethodAnnotations())
        .hasSize(1)
        .first()
        .satisfies(config -> {
          assertThat(config.getClassFilter()).isInstanceOf(ClassPredicate.class);
          assertThat(config.getMethodFilter())
              .asInstanceOf(InstanceOfAssertFactories.type(MethodPredicate.class))
              .satisfies(predicate -> {
                // a bit of a dirty hack, but it's simply for testing
                Optional<Object> matchers = ReflectionUtils.tryToReadFieldValue(MethodPredicate.class,
                    "parameterTypeNameMatchers", predicate).toOptional();
                assertThat(matchers).get(InstanceOfAssertFactories.list(StringMatcher.class)).hasSize(2);
              });
          assertThat(config.getExpressions())
              .hasSize(1)
              .first()
              .satisfies(expression -> {
                assertThat(expression.getExpression()).isNotNull();
                assertThat(expression.isReplaced()).isFalse();
              });
        });
  }

  @Test
  void processesMethodFilterAllowsNoTypeSpec() throws Exception {
    Element element = loadXml("batch-annotate/method-annotate-no-types.xml");
    BatchAnnotationConfigProcessor processor = new BatchAnnotationConfigProcessor(element);
    assertThat(processor.getMethodAnnotations())
        .hasSize(1)
        .first()
        .satisfies(config -> {
          assertThat(config.getClassFilter()).isInstanceOf(ClassPredicate.class);
          assertThat(config.getMethodFilter()).isInstanceOf(MethodPredicate.class);
          assertThat(config.getExpressions())
              .hasSize(1)
              .first()
              .satisfies(expression -> {
                assertThat(expression.getExpression()).isNotNull();
                assertThat(expression.isReplaced()).isFalse();
              });
        });
  }

  @Test
  void processesMethodParameterFilterSpec() throws Exception {
    Element element = loadXml("batch-annotate/method-param-annotate.xml");
    BatchAnnotationConfigProcessor processor = new BatchAnnotationConfigProcessor(element);
    assertThat(processor.getMethodParameterAnnotations())
        .hasSize(1)
        .first()
        .satisfies(config -> {
          assertThat(config.getClassFilter()).isInstanceOf(ClassPredicate.class);
          assertThat(config.getMethodFilter()).isInstanceOf(MethodPredicate.class);
          assertThat(config.getMethodParameterFilter()).isInstanceOf(MethodParameterPredicate.class);
          assertThat(config.getExpressions())
              .hasSize(1)
              .first()
              .satisfies(expression -> {
                assertThat(expression.getExpression()).isNotNull();
                assertThat(expression.isReplaced()).isFalse();
              });
        });
  }

  private Element loadXml(String filename) throws Exception {
    try (InputStream stream = getClass().getClassLoader().getResourceAsStream(filename)) {
      Document document = builder.parse(stream);
      return document.getDocumentElement();
    }
  }

}
