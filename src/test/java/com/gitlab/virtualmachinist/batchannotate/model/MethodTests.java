package com.gitlab.virtualmachinist.batchannotate.model;

import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class MethodTests {

  private JCodeModel model;
  private JDefinedClass klass;

  @BeforeEach
  void setUp() throws Exception {
    model = new JCodeModel();
    klass = model._class("foo.bar.Test");
  }

  @Test
  void constructorNameIsInit() {
    Method method = new Method(klass.constructor(JMod.PUBLIC));
    assertThat(method.getName()).isEqualTo("<init>");
  }

  @Test
  void regularMethodNameIsUnchanged() {
    Method method = new Method(klass.method(JMod.PROTECTED, model.VOID, "test"));
    assertThat(method.getName()).isEqualTo("test");
  }

  @Test
  void getParametersReturnsEmptyListOnNoParameters() {
    assertThat(new Method(klass.constructor(JMod.PUBLIC)).getParameters())
        .isNotNull()
        .isEmpty();
    assertThat(new Method(klass.method(JMod.PROTECTED, model.VOID, "test")).getParameters())
        .isNotNull()
        .isEmpty();
  }

  @Test
  void getParametersReturnsConstructorParameters() {
    JMethod constructor = klass.constructor(JMod.PUBLIC);
    constructor.param(model.INT, "intParam");
    constructor.param(model.ref("foo.bar.Test2"), "test2Param");
    List<MethodParameter> parameters = new Method(constructor).getParameters();
    assertThat(parameters).hasSize(2);
    assertThat(parameters.get(0)).satisfies(parameter -> {
      assertThat(parameter.getIndex()).isEqualTo(0);
      assertThat(parameter.getName()).isEqualTo("intParam");
      assertThat(parameter.getType().fullName()).isEqualTo("int");
    });
    assertThat(parameters.get(1)).satisfies(parameter -> {
      assertThat(parameter.getIndex()).isEqualTo(1);
      assertThat(parameter.getName()).isEqualTo("test2Param");
      assertThat(parameter.getType().fullName()).isEqualTo("foo.bar.Test2");
    });
  }

  @Test
  void getParametersReturnsRegularMethodParameters() {
    JMethod method = klass.method(JMod.PUBLIC, model.VOID, "setName");
    method.param(model.ref(String.class), "name");
    List<MethodParameter> parameters = new Method(method).getParameters();
    assertThat(parameters)
        .hasSize(1)
        .first().satisfies(parameter -> {
      assertThat(parameter.getIndex()).isEqualTo(0);
      assertThat(parameter.getName()).isEqualTo("name");
      assertThat(parameter.getType().fullName()).isEqualTo("java.lang.String");
    });
  }

  @Test
  void equalsHashCodeToString() {
    JMethod method = klass.method(JMod.PROTECTED, model.INT, "test");
    Method m1 = new Method(method);
    Method m2 = new Method(method);
    assertThat(m1)
        .isEqualTo(m2)
        .hasSameHashCodeAs(m2)
        .hasToString(m2.toString());
  }

}
