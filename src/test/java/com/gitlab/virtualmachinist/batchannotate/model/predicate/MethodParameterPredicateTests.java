package com.gitlab.virtualmachinist.batchannotate.model.predicate;

import com.gitlab.virtualmachinist.batchannotate.model.Method;
import com.gitlab.virtualmachinist.batchannotate.model.MethodParameter;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static com.gitlab.virtualmachinist.batchannotate.model.StringMatcher.anyMatch;
import static org.assertj.core.api.Assertions.assertThat;

class MethodParameterPredicateTests {

  private JCodeModel model;
  private JMethod method;

  @BeforeEach
  void setUp() throws Exception {
    model = new JCodeModel();
    JDefinedClass klass = model._class("foo.bar.Test");
    method = klass.method(JMod.PUBLIC, model.VOID, "test");
  }

  @Test
  void parameterNameMatches() {
    method.param(model.BOOLEAN, "param");
    MethodParameter parameter = new Method(method).getParameters().get(0);
    assertThat(new MethodParameterPredicate("param"::equals, null, anyMatch()).test(parameter)).isTrue();
    assertThat(new MethodParameterPredicate("foo"::equals, null, anyMatch()).test(parameter)).isFalse();
  }

  @Test
  void parameterIndexMatches() {
    method.param(model.BOOLEAN, "param1");
    method.param(model.SHORT, "param2");
    MethodParameter parameter1 = new Method(method).getParameters().get(0);
    MethodParameter parameter2 = new Method(method).getParameters().get(1);

    assertThat(new MethodParameterPredicate(anyMatch(), 0, anyMatch()).test(parameter1)).isTrue();
    assertThat(new MethodParameterPredicate(anyMatch(), 1, anyMatch()).test(parameter1)).isFalse();
    assertThat(new MethodParameterPredicate(anyMatch(), 0, anyMatch()).test(parameter2)).isFalse();
    assertThat(new MethodParameterPredicate(anyMatch(), 1, anyMatch()).test(parameter2)).isTrue();
  }

  @Test
  void parameterTypeMatches() {
    method.param(model.ref(Map.class).narrow(model.ref(String.class), model.wildcard()), "param");
    MethodParameter parameter = new Method(method).getParameters().get(0);
    assertThat(new MethodParameterPredicate(anyMatch(), null, "float"::equals).test(parameter)).isFalse();
    assertThat(new MethodParameterPredicate(anyMatch(), null,
        "java.util.Map<java.lang.String,? extends java.lang.Object>"::equals).test(parameter)).isTrue();
  }

}
