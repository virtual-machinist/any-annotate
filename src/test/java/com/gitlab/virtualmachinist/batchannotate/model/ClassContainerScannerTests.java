package com.gitlab.virtualmachinist.batchannotate.model;

import com.gitlab.virtualmachinist.anyannotate.testsupport.OptionsBuilder;
import com.gitlab.virtualmachinist.anyannotate.testsupport.XjcLauncher;
import com.sun.codemodel.ClassType;
import com.sun.codemodel.*;
import com.sun.tools.xjc.ConsoleErrorReporter;
import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.outline.Outline;
import com.sun.tools.xjc.outline.PackageOutline;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class ClassContainerScannerTests {

  @SuppressWarnings("WeakerAccess")
  @TempDir
  Path path;

  private Map<String, ClassContainerScanner> scanners;

  @BeforeEach
  void setUp() throws Exception {
    scanners = new HashMap<>();
    Options options = new OptionsBuilder()
        .withTargetDirectory(path)
        .withBindFile("batch-annotate/simple-schema-only-packages.xjb")
        .withSchemaFiles("simple-schema.xsd", "other-schema.xsd")
        .withArguments("-mark-generated")
        .build();
    try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
      ConsoleErrorReporter errorReporter = new ConsoleErrorReporter(out);
      XjcLauncher launcher = new XjcLauncher(options, errorReporter).generateCode();
      assertThat(out.toString(StandardCharsets.UTF_8)).doesNotContain("WARNING", "ERROR");
      Outline outline = launcher.getOutline();
      for (PackageOutline packageContext : outline.getAllPackageContexts()) {
        JPackage jPackage = packageContext._package();
        ClassContainerScanner scanner = new ClassContainerScanner();
        scanner.scan(jPackage);
        scanner.addEnumConstants(outline.getEnums());
        scanners.put(jPackage.name(), scanner);
      }
    }
  }

  @Test
  void findsPackageClasses() {
    assertThat(scanners.get("foo.bar.something")
        .findClassesOrEnums(klass -> klass.getClassType() == ClassType.CLASS)
        .map(JDefinedClass::fullName))
        .hasSize(4)
        .contains("foo.bar.something.ObjectFactory", "foo.bar.something.Foo", "foo.bar.something.Door",
            "foo.bar.something.Car");
    assertThat(scanners.get("foo.bar.other")
        .findClassesOrEnums(klass -> klass.getClassType() == ClassType.CLASS)
        .map(JDefinedClass::fullName))
        .hasSize(2)
        .contains("foo.bar.other.Bar", "foo.bar.other.ObjectFactory");
  }

  @Test
  void findsPackageEnums() {
    assertThat(scanners.get("foo.bar.something")
        .findClassesOrEnums(klass -> klass.getClassType() == ClassType.ENUM)
        .map(JDefinedClass::fullName))
        .hasSize(1)
        .contains("foo.bar.something.Side");
    assertThat(scanners.get("foo.bar.other")
        .findClassesOrEnums(klass -> klass.getClassType() == ClassType.ENUM)
        .map(JDefinedClass::fullName))
        .isEmpty();
  }

  @Test
  void findsEnumConstants() {
    assertThat(scanners.get("foo.bar.something")
        .findEnumConstants(klass -> klass.fullName().equals("foo.bar.something.Side"), __ -> true)
        .map(JEnumConstant::getName))
        .hasSize(5)
        .contains("foo.bar.something.Side.LEFT_FRONT", "foo.bar.something.Side.RIGHT_FRONT",
            "foo.bar.something.Side.LEFT_BACK", "foo.bar.something.Side.RIGHT_BACK", "foo.bar.something.Side.BACK");
  }

  @Test
  void findsFields() {
    assertThat(scanners.get("foo.bar.something")
        .findFields(klass -> klass.fullName().equals("foo.bar.something.Car"), __ -> true)
        .map(JFieldVar::name))
        .hasSize(3)
        .contains("door", "make", "model");
  }

  @Test
  void findsClassMethods() {
    assertThat(scanners.get("foo.bar.something")
        .findMethods(klass -> klass.fullName().equals("foo.bar.something.Car"), __ -> true)
        .map(method -> method.getMethod().name()))
        .hasSize(5)
        .contains("getDoor", "getMake", "setMake", "getModel", "setModel");
  }

  @Test
  void findsEnumMethods() {
    assertThat(scanners.get("foo.bar.something")
        .findMethods(klass -> klass.fullName().equals("foo.bar.something.Side"), __ -> true)
        .map(method -> method.getMethod().name()))
        .hasSize(2)
        .contains("value", "fromValue");
  }

  @Test
  void findsClassMethodParameters() {
    assertThat(scanners.get("foo.bar.something")
        .findMethodParameters(klass -> klass.fullName().equals("foo.bar.something.Car"),
            method -> method.getName().equals("setMake"), __ -> true)
        .map(MethodParameter::getName))
        .hasSize(1)
        .contains("value");
  }

  @Test
  void findsEnumMethodParameters() {
    assertThat(scanners.get("foo.bar.something")
        .findMethodParameters(klass -> klass.fullName().equals("foo.bar.something.Side"),
            method -> method.getName().equals("fromValue"), __ -> true)
        .map(MethodParameter::getName))
        .hasSize(1)
        .contains("v");
  }

}
