v2.1.0 (2024-04-23)

- Upgrade dependencies, Gradle wrapper and build plugins

v2.0.0 (2023-08-13)

- Migrate to JAXB 4, HiSrc BasicJAXB to 2.1.1, JavaParser to 3.25.4 (#9)
- Upgrade Gradle Wrapper to 8.2.1, make use of Gradle XJC Plugin fork by `dlmiles`
- Use platform BOMs for Glassfish JAXB RI, JUnit and AssertJ

v1.0.0 (2023-02-02)

- Migrate to JAXB 3 / Java 11, make use of `org.patrodyne.jvnet:hisrc-basicjaxb-tools`
  instead of `org.jvnet.jaxb2_commons:jaxb2-basics-tools` (#5)
- Upgrade Gradle Wrapper to 7.4.2, JavaParser to 3.25.0
- Add manifest versioning and OWASP scanning to build

v0.5.1 (2021-02-07)

- Change artifact group to `com.gitlab-virtual-machinist`, publish to Maven Central (#8)
- Upgrade Gradle Wrapper to 6.8.2

v0.5.0 (2020-12-28)

- Support filtering interface and annotation classes in batch-annotate (#6)

v0.4.1 (2020-12-27)

- Rewrite package customization search to work around inconsistent
  `com.sun.tools.xjc.outline.PackageOutline#getMostUsedNamespaceURI()` behavior  (#7)

v0.4.0 (2020-12-20)

- Add batch annotation support (#2)
- Upgrade JAXB to 2.3.3, Gradle Wrapper to 6.7.1, JavaParser to 3.18.0
- Remove deprecated `com.gitlab.virtualmachinist.anyannotate.annotator.ParsingAnnotator` class

v0.3.0 (2019-09-16)

- Add support to replace existing annotations (#4)
- `com.gitlab.virtualmachinist.anyannotate.annotator.ParsingAnnotator` is deprecated and will be removed from the public
API in future releases. See `com.gitlab.virtualmachinist.anyannotate.annotatable.AnnotatableFacade` for replacement.

v0.2.0 (2019-09-09)

- Add schema file for basic IDE autocomplete/validation (#1)
- Add annotation removal feature (#3)

v0.1.0 (2019-09-06)

- Initial public release
