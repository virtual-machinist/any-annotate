any-annotate
============

Allows adding and removing arbitrary annotation expressions to XJC/JAXB generated sources.

Important notice
----------------

Starting from version 0.5.1 the artifact is published to Maven Central under `com.gitlab.virtual-machinist` group.  
Please update your build files.

Rationale
---------

This is not the first plugin to provide the feature of adding arbitrary annotations to sources generated in XJC.
To my knowledge there is at least **jaxb2-annotate-plugin** that does the same thing (please check
[it](https://github.com/highsource/jaxb2-annotate-plugin) or its [fork](https://github.com/patrodyne/hisrc-hyperjaxb-annox)
out at GitHub).

However, that particular plugin has one major restriction - the annotation classes need to be in the classpath of the
plugin in order to be used. This is not a big deal if you use only standard annotations like JAXB or Validation API,
but is _kind of_ a problem if you choose to use project-specific annotation classes (or reference any classes being
generated, say enums). To deal with that you have to extract them into a separate module, compile and add them to the
classpath of XJC, etc.

When I started looking into this problem I realized that XJC API being ~~ugly~~ asymmetric and heterogeneous as it is
does not actually _forbid_ referencing classes (including annotation classes) that are not in the classpath and/or part
of the generated model.

I found out that the restriction is due to using **Annox** project that loads the annotation classes to parse the
expressions. That gives better type safety and a more flexible DSL, but adds the classpath requirement.

Due to Annox being a core component of jaxb2-annotate-plugin it wasn't easy to replace it with something else, so I
decided to write something myself. Thus, this project was conceived.

Features
--------

This project consists of three separate plugins:

* `any-annotate` for adding and replacing annotations using the standard XJC `Outline`-based customization.
* `any-deannotate` for removing any annotations by class, also using standard `Outline`-based customization.
* `batch-annotate` for adding and replacing annotations _en masse_ for any class, field, enum constant, method,
constructor or method parameter known to XJC, including `ObjectFactory` classes and code generated with other plugins.


Description and Usage
---------------------

#### Enabling the Plugin
The plugin with all its dependencies has to be in the classpath of XJC to be enabled. There are many ways to do this
depending on how you launch your code generation.
I prefer using Gradle with [a fork of the original Gradle XJC Plugin](https://dlmiles.github.io/gradle-xjc-plugin/).
With this configuration you need to add the following to your `build.gradle`:
```
plugins {
  // see https://github.com/dlmiles/gradle-xjc-plugin#quick-start on how to get this version
  id 'org.unbroken-dome.xjc' version '2.3.0-SNAPSHOT'
}

repositories {
  mavenCentral()
}

dependencies {
...
  xjcClasspath 'com.gitlab.virtual-machinist:any-annotate:2.1.0'
...
}
```

The plugin is enabled by adding `-Xany-annotate`, `-Xany-deannotate` or `-Xbatch-annotate` to XJC command line options:
```
sourceSets {
  main {
    ...
    xjcExtraArgs.addAll '-Xany-annotate', '-Xany-deannotate', '-Xbatch-annotate'
  }
}
```

For further usage details please refer to [USING.md](USING.md).

#### Requirements
* JDK 11+
* JAXB 4.0.x


Contributing
------------

Technical contributions and bug reports are welcome after raising an issue.


Credits
-------

1. Late Dr Alexey Valikov ([@highsource](https://github.com/highsource)) and other authors of **jaxb2-annotate-plugin**.
Their plugin was the primary inspiration for this project.

2. The [authors](https://github.com/javaparser/javaparser/graphs/contributors) of **JavaParser** library.
This project wouldn't have existed without you!

3. Manuel Siggen ([@Siggen](https://github.com/Siggen)) and other authors of **jaxb2-namespace-prefix** plugin.
Your code gave me ideas on how to match packages with namespaces.

4. Rick O'Sullivan ([@patrodyne](https://github.com/patrodyne)) who took over the orphaned
jaxb2-annotate-plugin, releasing it under **hisrc-hyperjaxb-annox** name, together with other JAXB2 Basics Plugins.

5. Darryl Miles ([@dlmiles](https://github.com/dlmiles)) for maintaining a fork of the original
**Gradle XJC Plugin** that supports XJC 4.x.